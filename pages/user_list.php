<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

include_once "../include/header.php";
?>

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Users List
                </h2>
                <!--                <ul class="header-dropdown m-r--5">-->
                <!--                    <li class="dropdown">-->
                <!--                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">-->
                <!--                            <i class="material-icons">more_vert</i>-->
                <!--                        </a>-->
                <!--                        <ul class="dropdown-menu pull-right">-->
                <!--                            <li><a href="javascript:void(0);">Action</a></li>-->
                <!--                            <li><a href="javascript:void(0);">Another action</a></li>-->
                <!--                            <li><a href="javascript:void(0);">Something else here</a></li>-->
                <!--                        </ul>-->
                <!--                    </li>-->
                <!--                </ul>-->
            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="mytable" class="table table-bordered table-striped table-hover dataTable js-exportable"> -->
                    <table id="mytable" style="display:none">

                        <thead>
                            <tr>
                                <th>S. no</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Balance</th>
                                <th>Messages Consumed</th>
                                <th>User Type</th>
                                <th>Contact</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Company</th>
                                <th>Nature of Business</th>
                                <th>Date</th>
                                <th>Package Type</th>
                                <th>Address</th>
                                <th>Ads Watched Count</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>S. no</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Balance</th>
                                <th>Messags Consumed</th>
                                <th>User Type</th>
                                <th>Contact</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Company</th>
                                <th>Nature of Business</th>
                                <th>Date</th>
                                <th>Package Type</th>
                                <th>Address</th>
                                <th>Ads Watched Count</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <!-- <?php echo $user->get_all_user();?> -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
<!--                        <tr>-->
                            <!--                            <td>Tiger Nixon</td>-->
                            <!--                            <td>System Architect</td>-->
                            <!--                            <td>Edinburgh</td>-->
                            <!--                            <td>61</td>-->
                            <!--                            <td>2011/04/25</td>-->
                            <!--                            <td><span class="label label-success">Active</span></td>-->
                            <!--                            <td>-->
                            <!--                                <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float">-->
                            <!--                                    <i class="material-icons">edit</i>-->
                            <!--                                </button>-->
                            <!--                            </td>-->
                            <!--                        </tr>-->

<!-- <div id="view_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-2"><label for="">Name</label></div>
                    <div class="col-md-10">
                        <p id="u_name"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Email</label></div>
                    <div class="col-md-10">
                        <p id="u_email"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Balance</label></div>
                    <div class="col-md-10">
                        <p id="u_total_msg"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Messages Consumed</label></div>
                    <div class="col-md-10">
                        <p id="u_msg_consume"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">User Type</label></div>
                    <div class="col-md-10">
                        <p id="u_is_paid"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Contact</label></div>
                    <div class="col-md-10">
                        <p id="u_contact"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Country</label></div>
                    <div class="col-md-10">
                        <p id="u_country"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">City</label></div>
                    <div class="col-md-10">
                        <p id="u_city"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Company</label></div>
                    <div class="col-md-10">
                        <p id="u_company"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Nature of Business</label></div>
                    <div class="col-md-10">
                        <p id="u_nature_bussniess"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"><label for="">Date</label></div>
                    <div class="col-md-10">
                        <p id="u_created_at"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Package Type</label></div>
                    <div class="col-md-10">
                        <p id="u_pkg_type"></p>
                    </div>
                </div>
                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->

<div id="view_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- <?php echo $user->get_all_user();?> -->
<?php
include_once "../include/footer.php"
?>

<script>

    // $(".btn_view_record").click(function (e) { 
    //     e.preventDefault();
        
    //     $("#u_name").               text($(this).closest('tr').find('.u_name').text());
    //     $("#u_email").              text($(this).closest('tr').find('.u_email').text())
    //     $("#u_total_msg").          text($(this).closest('tr').find('.u_total_msg').text())
    //     $("#u_msg_consume").        text($(this).closest('tr').find('.u_msg_consume').text())
    //     $("#u_is_paid").            text($(this).closest('tr').find('.u_is_paid').text())
    //     $("#u_contact").            text($(this).closest('tr').find('.u_contact').text())
    //     $("#u_country").            text($(this).closest('tr').find('.u_country').text())
    //     $("#u_city").               text($(this).closest('tr').find('.u_city').text())
    //     $("#u_company").            text($(this).closest('tr').find('.u_company').text())
    //     $("#u_nature_bussniess").   text($(this).closest('tr').find('.u_nature_bussniess').data('nature'))
    //     $("#u_created_at").         text($(this).closest('tr').find('.u_created_at').text())
    //     $("#u_pkg_type").           text($(this).closest('tr').find('.u_pkg_type').text())

    //     $("#view_modal").modal("show");

    // });

    $(document).ready(function () {
        var temp = "1";
        if(temp == 1)
        {
            $.ajax({
                type: "POST",
                data: { invoiceno: temp },
                url: "get_all_users_list.php",
                dataType: "html",
                async: true,
                success: function(data) {
                    let result = data;
                    // console.log("bilal",result);
                    $("tbody").html(result);

                    $('#mytable').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $('#mytable').addClass("table table-bordered table-striped table-hover");
                    // table table-bordered table-striped table-hover dataTable js-exportable
                    $('#mytable').show();
                    temp++;
                }
            });    
        }
    });

    // $(".btn_view_record").click(,function (e) { 
    $(document).on("click",".btn_view_record",function (e) { 

        e.preventDefault();

        console.log("bilal");

        var arr = [
            ".u_name",
            ".u_email",
            ".u_total_msg",
            ".u_msg_consume",
            ".u_is_paid",
            ".u_contact",
            ".u_country",
            ".u_city",
            ".u_company",
            ".u_nature_bussniess",
            ".u_created_at",
            ".u_pkg_type",
            ".u_comp_address"
        ];

        var label = [
            "Name",
            "Email",
            "Balance",
            "Messags Consumed",
            "User Type",
            "Contact",
            "Country",
            "City",
            "Company",
            "Nature of Business",
            "Date",
            "Package Type",
            "Address"
        ];

        var i;
        var text = '';
        var val = '';
        for (i = 0; i < label.length; i++) 
        {
            if (arr[i] == '.u_nature_bussniess' || arr[i] == '.u_comp_address' ) 
            {
                val = $(this).closest('tr').find(arr[i]).data(arr[i].replace('.',''));
                let temp = (val != null)? val:"";
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+temp+`</p>
                    </div>
                    </div>`;
            }
            else{
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+$(this).closest('tr').find(arr[i]).text()+`</p>
                    </div>
                    </div>`;
            }
            
        }

        $(".modal-body").html(text);
        $("#view_modal").modal("show");

    });

</script>