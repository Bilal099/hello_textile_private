<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 12/24/2018
 * Time: 2:13 PM
 */

class Notifications
{
    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    function add_notifications($title, $description, $include_countries)
    {
        global $push, $user;
        // get_all_users_tokens($exclude_id = null, $exclude_country = null, $cid = null, $sub_cid = null, $include_countries = null)
        $tokens = $user->get_all_users_tokens(null, null, null, null, $include_countries);

        $date = date('m/d/Y h:i:s a', time());
        
        $inc_countries = strlen($include_countries) > 0 ? $include_countries : null;
        $stmt = $this->conn->prepare("INSERT INTO notification(n_title, n_msg, n_date, n_countries) VALUES (?,?,?,?)");
        $stmt->bind_param("ssss", $title, $description, $date, $inc_countries);
        if ($stmt->execute())
        {
            $msg = '<script>swal("Good Job!", "Notification add successfully.", "success");</script>';
            $chunks = array_chunk($tokens, 999);
            for ($x = 0; $x < count($chunks); $x++) {
                $push->send_push_to_noti_panel($chunks[$x], $title,"Let's Check What's new Notification you have.","");
            }
        }
        else
            $msg = '<script>swal("Oops!", "Unable to add Notification.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function get_all_notifications()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT n_id, n_title, n_msg, n_date FROM notification ");
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['title'],$param['desc'],$param['n_date']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<tr>
                            <td>'.$param['title'].'</td>
                            <td>'.$param['desc'].'</td>
                            <td>'.$param['n_date'].'</td>
                            <td>
                                <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_notification?id='.$param['id'].'"><i class="material-icons">edit</i></a>
                            </td>
                        </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Notification Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    function get_Notification_by_id($n_id)
    {
        $stmt = $this->conn->prepare("SELECT n_title, n_msg FROM notification WHERE n_id = ?");
        $stmt->bind_param("i",$n_id);
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['title'],$cat_arr['desc']);
        $stmt->store_result();
        $stmt->fetch();
        return $cat_arr;
        $stmt->close();
    }

    function delete_Notification($n_id)
    {
        $stmt = $this->conn->prepare("DELETE FROM notification WHERE n_id = ?");
        $stmt->bind_param("i",$n_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Notification Delete successfully.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to Delete Notification.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function update_Notification($n_id,$name,$desc)
    {
        $stmt = $this->conn->prepare("UPDATE notification SET n_title = ?, n_msg = ? WHERE  n_id = ?");
        $stmt->bind_param("ssi",$name,$desc,$n_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Notification update successfully.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to update Notification.", "error");</script>';
        $stmt->close();
        return $msg;
    }
}