<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 10/2/2018
 * Time: 12:38 PM
 */
include_once "../include/header.php";
$sub_admin_id = $_GET['id'];
if(isset($_POST['subBtn']))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    echo $sub_admin->update_sub_admin($sub_admin_id,$name,$email,$password);
}
if(isset($_POST['deleteBtn']))
{
    echo $sub_admin->delete_sub_admin($sub_admin_id);
}
$cat_arr  = $sub_admin->get_sub_admin_by_id($sub_admin_id);
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit Sub Admin</h2>
            </div>
            <div class="body">
                <form id="form_validation" enctype="multipart/form-data" method="POST" action="">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="<? echo $cat_arr['name']?>" required>
                            <label class="form-label">Name</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="email" value="<? echo $cat_arr['email']?>" required>
                            <label class="form-label">Email</label>
                        </div>
                    </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" value="<? echo $cat_arr['password']?>" required>
                                <label class="form-label">Password</label>
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT<i class="material-icons">send</i></button> <br><br>
                             <button class="btn btn-danger waves-effect" name="deleteBtn" type="submit"><i class="material-icons">delete</i>DELETE</button>
                        </div>
                    </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

<?php
include_once "../include/footer.php";
?>
<!-- #END# Basic Validation -->