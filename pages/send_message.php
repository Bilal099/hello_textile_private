<?php
include_once "../include/header.php";

if(isset($_POST['submit']))
{
    if(empty($_POST['subCategory']))
        $sub_cat = 0;
    else
        $sub_cat = $_POST['subCategory'];

    $param = array(
        'category_id'   =>  $_POST['category'],
        'sub_cat_id'    =>  $sub_cat,
        'user_id'       =>  $_SESSION["admin_id"],
        'message'       =>  $_POST['message']
    );
    echo $chat->add_admin_msg_in_cat($param);
}
?>

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Send Message to Category</h2>
                </div>
                <div class="body">
                    <form id="form_validation" enctype="multipart/form-data" method="POST" action="">

                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <p>
                                        <b>Category</b>
                                    </p>
                                    <select id="category" name="category" class="form-control show-tick">
                                        <? echo $category_obj->select_category(); ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <p>
                                        <b>Sub Category</b>
                                    </p>
                                    <select id="subCategory" name="subCategory" class="form-control show-tick">

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
<!--                                    <input type="text" class="form-control" name="message" required>-->
                                    <textarea name="message" cols="30" rows="5" class="form-control no-resize" required></textarea>
                                    <label class="form-label">Type here your Message..</label>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary waves-effect" type="submit" name="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "../include/footer.php";
?>

<script>

    $(document).ready(function () {
        load_subcategories();
    });

    $("#category").change(function () {
        load_subcategories();
    });

    function load_subcategories() {
        var id = $("#category").val();
        $.ajax({
            type: "POST",
            data: { request:'load_sub_category_for_send_message', catt_id: id },
            url: "ajaxcall.php",
            success: function(data)
            {
                $('#subCategory').html(data);
                $('#subCategory').selectpicker('refresh');
            }
        });
    }
</script>
