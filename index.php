<?php
ob_start();
include_once "include/globals.php";
if(common::logged_in())
{
    if (isset($_SESSION['adv_email'])) {
        header("location:pages/new_ad");
    }
    else {
        if (!isset($_GET['redir'])) {
            header("location:" . $_GET['redir']);
        } 
        else {
            header("location:pages/home");
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en" >

<head>

  <meta charset="UTF-8">
  <title>HelloTextiles Admin</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'>
  <link rel="stylesheet" href="css/style_login.css">
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />

</head>

<body>
<form class="login" method="post" action="">
  <fieldset>
  	<legend class="legend">Login to HelloTextiles</legend>

          <div class="input">
              <input name="email" type="email" placeholder="Email" required />
              <span><i class="fa fa-envelope-o"></i></span>
          </div>

          <div class="input">
              <input name="pass" type="password" placeholder="Password" required />
              <span><i class="fa fa-lock"></i></span>
          </div>

          <button name="loginbtn" type="submit" class="submit"><i class="fa fa-long-arrow-right"></i></button>
    
  </fieldset>
</form>



  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <!-- SweetAlert Plugin Js -->
    <script src="plugins/sweetalert/sweetalert.min.js"></script>

<!--  <script  src="js/index.js"></script>-->
<?php
if(isset($_POST['loginbtn']))
{
    $params = array(
            'email' =>  $_POST['email'],
            'pass'  =>  $_POST['pass']
    );
//    $email = $_POST['email'];
//    $pass = $_POST['pass'];
//    include_once dirname(__FILE__) . '/config.php';
//    $link = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
//    $stmt = mysqli_prepare($link,"SELECT u_id, u_email, u_pass FROM users WHERE u_email = ? AND u_pass = ? AND u_status = 1");
//    mysqli_stmt_bind_param($stmt,"ss",$email,$pass);
//    mysqli_stmt_execute($stmt);
//    mysqli_stmt_bind_result($email,$pass);
//    mysqli_stmt_store_result($stmt);
//    mysqli_stmt_fetch($stmt);
//    if(mysqli_stmt_num_rows($stmt)==1)
//        echo '<script>swal("Good Job!", "Login successful.", "success");</script>';
//    mysqli_stmt_close($stmt);
//    mysqli_close($link);
    echo $user->login($params);
}
?>
</body>
</html>
