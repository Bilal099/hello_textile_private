<?php
include_once "../include/header.php";
$conn = $link->connect();
$a_id = $_GET['id'];
if(isset($_POST['subbtn']))
{
    $param = array(
        'id'    =>  $a_id,
        'name'  =>  $_POST['name'],
        'email' =>  $_POST['email'],
        'pass'  => $_POST['pass']
    );
    echo $user->update_admin_profile($param);
}
$a_arr = array();
$a_arr = $user->get_admin_detail($a_id);
//$stmt = $conn->prepare("SELECT a_name, a_email, a_pass FROM admin_info WHERE a_id = ? ");
//$stmt->bind_param("s",$a_id);
//$stmt->execute();
//$stmt->bind_result($name,$email,$pass);
//$stmt->store_result();
//$stmt->fetch();
//$stmt->close();
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Update Profile</h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <form id="form_validation" action="" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="<? echo $a_arr['name']?>" required>
                            <label class="form-label">Name</label>
                        </div>
                    </div>
					   <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" value="<? echo $a_arr['email']?>" name="email" required>
                            <label class="form-label">Email</label>
                        </div>
                    </div>
					   <div class="form-group form-float">
                        <div class="form-line">
                            <input type="password" class="form-control" value="<? echo $a_arr['pass']?>" name="pass" required>
                            <label class="form-label">Passward</label>
                        </div>
                    </div>
                 

                    <button class="btn btn-primary waves-effect" name="subbtn" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>