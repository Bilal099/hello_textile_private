<?php
session_start();

if (!isset($_SESSION['admin_email'])) {
    exit();
}

include "../include/config.php";

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

$video_length = $_POST['video_length'];
$num_views = $_POST['num_views'];
$duration_num = $_POST['duration_num'];
$duration_type = $_POST['duration_type'];
$ad_link = $_POST['ad_link'];
$ad_link = $ad_link == 'null' ? NULL : "$ad_link";
$advertiser_id = $_POST['advertiser_id'];
$advertiser_pwd = $_POST['advertiser_pwd'];
$view_countries = $_POST['view_countries'];
$view_countries = $view_countries == 'null' ? NULL : "$view_countries";
$is_live = $_POST['is_live'];
$ad_file = $_POST['ad_file'];

$result = mysqli_query($con, 
                "INSERT INTO video_ads(video_length,num_views,views_left,duration_num,duration_type,ad_link,advertiser_id,advertiser_pwd,view_countries,is_live,ad_file) 
                             VALUES($video_length,$num_views,$num_views,$duration_num,'$duration_type','$ad_link','$advertiser_id','$advertiser_pwd','$view_countries',$is_live,'$ad_file')");
if (!$result) {
    $error = mysqli_error($con);
    if (substr($error,0,9) == "Duplicate") {
        $error = "Advertiser id already taken. Please use a different one.";
    }
    
    http_response_code(400);
    echo $error;
}

?>