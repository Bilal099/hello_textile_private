<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 10/2/2018
 * Time: 12:38 PM
 */
include_once "../include/header.php";
$sc_id = $_GET['id'];
if(isset($_POST['subBtn']))
{
    $name = $_POST['name'];
    $category = $_POST['category'];
    $status = $_POST['status'];

    echo $category_obj->update_sub_category_filed($sc_id,$name,$category,$status);


}
if(isset($_POST['deleteBtn']))
{
    echo $category_obj->delete_sub_category($sc_id);
}
$cat_arr  = $category_obj->get_sub_category_by_id($sc_id);
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit Sub Category</h2>
            </div>
            <div class="body">
                <form id="form_validation" enctype="multipart/form-data" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="<? echo $cat_arr['sc_name']?>" required>
                            <label class="form-label">Sub Category</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <p>
                            <b>Parent Category</b>
                        </p>
                        <select name="category" class="form-control show-tick">
                           <?php
                           echo $category_obj->get_category_dropdown($cat_arr['c_id']);
                           ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <p>
                            <b>Status</b>
                        </p>
                        <select name="status" class="form-control show-tick">
                            <option value="1" <? echo $cat_arr['sc_status']==1?'selected':''; ?>>Active</option>
                            <option value="0" <? echo $cat_arr['sc_status']==0?'selected':''; ?>>In-Active</option>
                        </select>
                    </div>
                    <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT<i class="material-icons">send</i></button>
                    <button class="btn btn-danger waves-effect" name="deleteBtn" type="submit"><i class="material-icons">delete</i>Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>
<!-- #END# Basic Validation -->