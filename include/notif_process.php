<?php

$last_msg_id = $argv[1];
$is_from_admin = $argv[2];

if (!isset($last_msg_id) || !is_numeric($last_msg_id) || $last_msg_id <= 0) {
    echo "Invalid last message id\n";
    exit;
}
if (!isset($is_from_admin)) {
    $is_from_admin = 0;
}
else {
    $is_from_admin = abs(intval($is_from_admin));
}

$last_msg_id = intval($last_msg_id);

echo "Sending notifications for message-id=" . $last_msg_id . "\n";

include "user.php";
include "mysql.php";
include "/home/hellzpgk/public_html/chat_api/Classes/PushNotification.php";

$push = new PushNotification();

$DB = new mysql_functions();

$user = new user();

$con = mysqli_connect("localhost", "hellzpgk_chatUse", "C$[UD=WWGgLh", "hellzpgk_chatApp");

$sql = "SELECT m.message_id, m.user_id, m.category_id, m.sub_cat_id, m.message, m.created_at, c.c_name, sc.sc_name, u.u_name, u.u_country 
                        FROM messages m, users u, category c, sub_category sc
                        WHERE m.message_id=$last_msg_id
                        AND m.user_id=u.u_id 
                        AND m.category_id=c.c_id
                        AND m.sub_cat_id=sc.sc_id";
                        
if ($is_from_admin) {
    $sql = "SELECT m.message_id, m.user_id, m.category_id, m.sub_cat_id, m.message, m.created_at, c.c_name, sc.sc_name 
                            FROM messages m, category c, sub_category sc
                            WHERE m.message_id=$last_msg_id
                            AND m.category_id=c.c_id
                            AND m.sub_cat_id=sc.sc_id";
}


$res = mysqli_query($con, $sql);

echo $sql . "\n";
echo json_encode(mysqli_error($con)) . "\n";

$message = $res->fetch_assoc();

if (!$message) {
    echo "Message was not found \n";
    exit;
}

$user_token = null;
$tokens = null;

if (!$is_from_admin) {
    $tokens = $user->get_all_users_tokens($message["user_id"], $message["u_country"], $message['category_id'], $message['sub_cat_id']);
    $user_token = $user->get_user_token_by_id($message["user_id"]);
} else {
    $tokens = $user->get_all_users_tokens();
}
$chunks = array_chunk($tokens, 999);
if ($is_from_admin) {
    for ($x = 0; $x < count($chunks); $x++) {
        $push->sendToApprove($chunks[$x], "A message from Admin", $message["user_id"],"Admin",$message["created_at"],$message["message_id"],$message["category_id"],$message["sub_cat_id"],$message["c_name"],$message["sc_name"]);
    }
}
else {
    $msg = substr($message["message"], 0, 40);
    $msg = strlen($msg) == strlen($message["message"]) ? $msg : $msg . " ...";
    $msg = strlen($msg) == 0 ? "Image" : $msg; 
    
    $push->deleteFromAdmin($user_token, $msg, "Your message has been approved in " . $message["c_name"],"");
    
    for ($x = 0; $x < count($chunks); $x++) {
        $push->sendToApprove($chunks[$x], $msg, $message["user_id"], $message["u_name"],$message["created_at"],$message["message_id"],$message["category_id"],$message["sub_cat_id"],$message["c_name"],$message["sc_name"]);
    }
}