<?php
include_once "../include/header.php";

if(isset($_POST['submit']))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    echo $sub_admin->add_sub_admin($name,$email,$password);
}
?>

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Sub Admin</h2>
                </div>
                <div class="body">
                    <form id="form_validation" enctype="multipart/form-data" method="POST" action="">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" required>
                                <label class="form-label">Name</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="email" required>
                                <label class="form-label">Email</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" required>
                                <label class="form-label">Password</label>
                            </div>
                        </div>

                        <button class="btn btn-primary waves-effect" type="submit" name="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
include_once "../include/footer.php";
?>

