<?php
ob_start();
include_once "include/globals.php";
if(common::logged_in())
{header("location:/pages/home");}
?>
<!DOCTYPE html>
<html lang="en" >

<head>

  <meta charset="UTF-8">
  <title>Chat</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
  <link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'>
  <link rel="stylesheet" href="css/style_login.css">
    <!-- Sweetalert Css -->
    <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />

</head>

<body>

<form class="login" method="post" action="">
  <fieldset>
  	<legend class="legend">Login</legend>

          <div class="input">
              <input name="email" type="email" placeholder="Email" required />
              <span><i class="fa fa-envelope-o"></i></span>
          </div>

          <div class="input">
              <input name="pass" type="password" placeholder="Password" required />
              <span><i class="fa fa-lock"></i></span>
          </div>

          <button name="loginbtn" type="submit" class="submit"><i class="fa fa-long-arrow-right"></i></button>
    
  </fieldset>
</form>



  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <!-- SweetAlert Plugin Js -->
    <script src="plugins/sweetalert/sweetalert.min.js"></script>

<!--  <script  src="js/index.js"></script>-->
<?php
if(isset($_POST['loginbtn']))
{
    $params = array(
            'email' =>  $_POST['email'],
            'pass'  =>  $_POST['pass']
    );
    echo $sub_admin->login($params);
}
?>
</body>
</html>
