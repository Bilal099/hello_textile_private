<?php

// This script maintains the badges (i.e. unseen # of msgs) for ios users (now android as well)
// since ios (now android as well) notifications don't work well for this.
// The script is run in background, and shouldn't block the main process

function log_error($err, $sql = null) {
    file_put_contents("badges_errors.log", "\n===============\n", FILE_APPEND);
    if($sql) {
        file_put_contents("badges_errors.log", "$sql\n", FILE_APPEND);
    }
    file_put_contents("badges_errors.log", json_encode($err) . "\n", FILE_APPEND);
}

$type = $argv[1];
$uid = $argv[2];
$from_id = null;
$cid = null;
$sub_cid = null;
$count = null;
$msg_id = null;
$sql = null;

echo "Arguments: " . implode(array_slice($argv, 1, 7), " ") . "\n";

if ($type == "pc") { // example command's arguments for private chat (uid=1543, from_id=1098, count=1): update_badges.php pc 1543 1098 1 
    if (count($argv) != 5) {
        echo "Wrong number of arguments\n";
        exit;
    }
    $from_id = $argv[3];
    if (!isset($uid) || $uid <= 0) {
        echo "invalid uid";
        exit;
    }
    if (!isset($from_id) || $from_id <= 0) {
        echo "invalid from_id";
        exit;
    }
    $cid = -1;
    $sub_cid = -1;
    $count = $argv[4];
    $sql = "SELECT _id, type, u_id, c_id, sub_cid, from_id, count FROM badges 
            WHERE type='$type' AND u_id=$uid AND from_id=$from_id";
} 
else if ($type == "gc") { // example command's arguments for group chat (uid=-1 [multi-users], cid=2, sub_cid=3, count=1): update_badges.php gc -1 2 3 1 
    $cid = $argv[3];
    if (!isset($cid) || $cid <= 0) {
        echo "cid is invalid";
        exit;
    }
    $sub_cid = $argv[4];
    $sub_cid = isset($sub_cid) && $sub_cid > 0 ? $sub_cid : 'NULL';
    $count = $argv[5];
    if (count($argv) == 7) {
        $msg_id = $argv[6];
    }
    
    // For group chat, if uid is present, that means that specific user needs to clear the badges count
    if ($uid > 0) {
        $sql = "SELECT _id, type, u_id, c_id, sub_cid, from_id, count FROM badges 
                WHERE type='$type' AND u_id=$uid AND c_id=$cid AND sub_cid <=> $sub_cid";
    }
    
}
else if ($type == 'ns') { // example command's arguments for news (uid=1543, count=1): update_badges.php ns 1543 1 
    if (count($argv) != 4) {
        echo "Wrong number of arguments\n";
        exit;
    }
    $count = $argv[3];
    $cid = -1;
    $sub_cid = -1;
    $from_id = -1;
    if ($uid > 0) {
        $sql = "SELECT _id, type, u_id, c_id, sub_cid, from_id, count FROM badges 
                WHERE type='$type' AND u_id=$uid";
    }
}
else {
    echo "Invalid type '$type' was given\n";
    exit;
}

if (!isset($count)) {
    echo "count is missing";
    exit;
}


// if we are here, cli arguments are correct, and now we need to do actual stuff

$con = mysqli_connect("localhost", "hellzpgk_chatUse", "C$[UD=WWGgLh", "hellzpgk_chatApp");

$cid = isset($cid) && $cid > 0 ? $cid : 'NULL';                         // would be -1 for 'pc' and 'ns'
$sub_cid = isset($sub_cid) && $sub_cid > 0 ? $sub_cid : 'NULL';         // would be -1 for 'pc' and 'ns' 
$from_id = isset($from_id) && $from_id > 0? $from_id : 'NULL';          // would be -1 for 'gc' and 'ns'

// if it's updating or inserting a badge for single user
if ($uid > 0) {
    $res = mysqli_query($con, $sql);
    $err = mysqli_error($con);
    if ($err) {
        log_error($err, $sql);
    }
    
    // if the badge already exists
    if ($res = $res->fetch_assoc()) {
        $count = $res['count'] + intval($count);
        $count = max(0, $count);
        $_id = $res['_id'];
        $sql = "UPDATE badges SET count=$count 
                WHERE _id=$_id";
        
    } 
    else {
        $count = max(0, $count);
        $sql = "INSERT INTO badges (type, u_id, c_id, sub_cid, from_id, count) 
                VALUES ('$type', $uid, $cid, $sub_cid, $from_id, $count)";
                
    }
    mysqli_query($con, $sql);
    $err = mysqli_error($con);
    if ($err) {
        log_error($err, $sql);
    }
}
// if it's updating or inserting badges of multiple users
else {
    // Get country of message sender so users which have blocked that country (this info is in 'ex_countries') donot receive badge update
    if (!$msg_id) {
        echo "Message id required";
        exit;
    }
    $msg = mysqli_query($con, "SELECT u.u_country FROM messages m, users u WHERE u.u_id=m.user_id AND m.message_id=$msg_id");
    $msg = $msg->fetch_assoc();
    
    if (!$msg) {
        echo "Message not found for message_id=$msg_id";
        exit;
    }
    $msg_country = $msg['u_country'];
    
    // first version: only update badges for users who have allowed the related categories; FIND_IN_SET(2, '2,5,7') gives 1; FIND_IN_SET(2, '1,5,7') gives 0
    // now: update badges for all users
    
    $sql = "SELECT u_id FROM users WHERE ex_countries NOT LIKE '%$msg_country%'";

    $res = mysqli_query($con, $sql);
    $err = mysqli_error($con);
    if ($err) {
        log_error($err, $sql);
    }
    $user = $res->fetch_assoc();
    while ($user != null) {
        $userid = $user['u_id'];
        $sql = "SELECT _id, type, u_id, c_id, sub_cid, from_id, count FROM badges 
                WHERE type='$type' AND u_id=$userid AND c_id=$cid AND sub_cid <=> $sub_cid";
    
        $badge = mysqli_query($con, $sql);
        $badge = $badge->fetch_assoc();
        if ($badge != null) {
            $new_count = max(0, intval($count) + $badge['count']);
            $_id = $badge['_id'];
            $sql = "UPDATE badges SET count=$new_count WHERE _id=$_id";
        }
        else{
            $new_count = max(0, intval($count));
            $sql = "INSERT INTO badges (type, u_id, c_id, sub_cid, from_id, count) 
                             VALUES ('$type', $userid, $cid, $sub_cid, NULL, $new_count)";
        }
        mysqli_query($con, $sql);
        $err = mysqli_error($con);
        if ($err) {
            log_error($err, $sql);
        }
        $user = $res->fetch_assoc();
    } // end while
    
}

mysqli_close($con);