<?php
/**
 * Created by Arsalan.
 * User: Arsalan
 * Date: 11/8/2018
 * Time: 04:24 PM
 */

class pc_users
{
    function private_chat_users($id)
    {
        global $DB;
//         $sql = "select u_id, u_name, u_image from (SELECT sc_id,  from_user_id as uid ,msg_created_at FROM `single_chat` 
// WHERE 
// to_user_id = '$id'

// UNION

// SELECT sc_id, to_user_id as uid, msg_created_at FROM `single_chat` 
// WHERE 
//  from_user_id = '$id'
//               )_
//               inner join `users` on uid = u_id
//               group by  u_id, u_name, u_image
// order by MAX(sc_id) DESC";
        
        $sql = "SELECT u_id, u_name, u_image, max(str_to_date(msg_created_at, '%m/%d/%Y %r')) msg_created_at FROM single_chat, users 
                WHERE u_id != $id AND ((to_user_id=$id AND from_user_id=u_id) OR (to_user_id=u_id AND from_user_id=$id)) 
                GROUP BY u_id  
                ORDER BY msg_created_at DESC";
        $res = $DB->qr($sql);
        $data = null;
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $r['msg_created_at'] = date('m/d/Y h:m:s a', strtotime($r['msg_created_at']));
                $data[] = $r;
            }

        }
        else {
            $data = ['success'=>false, 'message'=> 'An error occurred'];
        }
        return json_encode($data);
    }

    function private_chat_message($from_id, $to_id,$limit)
    {
        global $DB;
        
        // don't show messages older than p_data_backup_duration
        // $sql = "SELECT p_data_backup_duration FROM users, packeages WHERE u_id=$from_id AND pkg_id=p_id";
        // $res = $DB->qr($sql);
        // $res = $DB->fa($res);
        // $data_backup_duration = '-' . $res['p_data_backup_duration'];
        // $start_date = date("m/d/Y", strtotime($data_backup_duration));
        // $timeFilter = "AND str_to_date(s.msg_created_at, '%m/%d/%Y %r') >= str_to_date('$start_date', '%m/%d/%Y')";
        
        $limit = isset($limit) ? "LIMIT $limit" : ""; 
        $sql = "SELECT s.sc_id, s.from_user_id, s.to_user_id, s.message, s.msg_created_at, s.images FROM single_chat s WHERE ((s.from_user_id = $from_id AND s.to_user_id=$to_id) OR (s.to_user_id =$from_id AND s.from_user_id=$to_id)) ORDER BY s.sc_id DESC $limit";
        $res = $DB->qr($sql);
        $data = [];
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = $r;
            }
        }
        return json_encode($data);
    }

    private function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
//                $temp_array[$i] = $val;
                array_push($temp_array,$val);
            }
            $i++;
        }
        return $temp_array;
    }


}