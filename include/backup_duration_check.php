<?php
// this module is run every midnight.
// expire messages which are older than user's p_data_backup_duration

// UPDATE messages SET expired=1 WHERE expired=0 AND str_to_date(created_at, '%m/%d/%Y %r') < DATE_SUB(NOW(), INTERVAL CAST((SELECT IF(p_data_backup_duration IS NULL, '30 Day', p_data_backup_duration) as p_data_backup_duration FROM users, packeages WHERE u_id=user_id AND p_id=pkg_id) AS UNSIGNED) DAY)

include "config.php";

echo "[".date('m/d/Y h:i:s a', time())."] Expiring messages \n";

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

$sql_users = "SELECT u_id, IF(p_data_backup_duration IS NULL, '30 Day', p_data_backup_duration) as p_data_backup_duration FROM users LEFT JOIN packeages ON p_id=pkg_id  WHERE u_id > 1";

$users = mysqli_query($con, $sql_users);

$affected_messages = 0;

while ($row = mysqli_fetch_assoc($users)) {
    $threshold_date = date('m/d/Y', strtotime('-' . $row['p_data_backup_duration']));
    $u_id = $row['u_id'];
    $update_sql = "UPDATE messages SET expired=1 WHERE user_id=$u_id AND str_to_date(created_at, '%m/%d/%Y %r') < str_to_date('$threshold_date', '%m/%d/%Y')";
    
    mysqli_query($con, $update_sql);
    $affected_messages = $affected_messages + mysqli_affected_rows($con);
}

mysqli_close($con);

echo "[".date('m/d/Y h:i:s a', time())."] Expired $affected_messages messages \n";
echo "last sql: \n $update_sql \n";