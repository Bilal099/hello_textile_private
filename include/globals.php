<?php
session_start();
include_once "function.php";

// My SQL Class Start
include_once "mysql.php";
$DB = new mysql_functions();
// My SQL Class End

include "user.php";
$user = new user();

include_once "db_connect.php";
$link = new DbConnect();

include_once "category.php";
$category_obj = new category();

include_once "news.php";
$news_obj = new news();

// include_once $_SERVER['DOCUMENT_ROOT']."/chat_api/Classes/PushNotification.php"; //
include_once $_SERVER['DOCUMENT_ROOT']."/www/HelloTextile11Feb2021/chat_api/Classes/PushNotification.php"; //
$push = new PushNotification();

include_once "chat_class.php";
$chat = new chat_class();

include_once "advertisment.php";
$ads = new advertisment();

include_once "Notifications.php";
$noti = new Notifications();

include_once "packages.php";
$package = new packages();

include_once "sub_admin.php";
$sub_admin = new sub_admin();

include_once "country.php";
$country = new country();