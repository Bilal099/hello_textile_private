<?php
session_start();

if (!isset($_SESSION['admin_email'])) {
    exit();
}

include "../include/config.php";

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

$video_length = $_POST['video_length'];
$num_views = $_POST['num_views'];
$duration_num = $_POST['duration_num'];
$duration_type = $_POST['duration_type'];
$ad_link = $_POST['ad_link'];
$ad_link = $ad_link == 'null' ? NULL : "$ad_link";
$advertiser_id = $_POST['advertiser_id'];
$advertiser_pwd = $_POST['advertiser_pwd'];
$view_countries = $_POST['view_countries'];
$view_countries = $view_countries == 'null' ? NULL : "$view_countries";
$is_live = $_POST['is_live'];
$ad_file = $_POST['ad_file'];

$result = mysqli_query($con, 
                "UPDATE video_ads SET 
                        video_length=$video_length,
                        num_views=$num_views,
                        views_left=(SELECT GREATEST(0, views_left + $num_views - num_views) FROM video_ads WHERE advertiser_id='$advertiser_id'),
                        duration_num=$duration_num,
                        duration_type='$duration_type',
                        ad_link='$ad_link',
                        advertiser_pwd='$advertiser_pwd',
                        view_countries='$view_countries',
                        is_live=$is_live,
                        ad_file='$ad_file'
                    WHERE advertiser_id='$advertiser_id'");
if (!$result) {
    $error = mysqli_error($con);
    http_response_code(400);
    echo  $error;
}

?>