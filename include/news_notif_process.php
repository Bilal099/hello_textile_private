<?php

$last_msg_id = $argv[1];

if (!isset($last_msg_id) || !is_numeric($last_msg_id) || $last_msg_id <= 0) {
    echo "Invalid last message id\n";
    exit;
}

$last_msg_id = intval($last_msg_id);

echo "Sending news notifications for news-id=" . $last_msg_id . "\n";

include "user.php";
include "mysql.php";
include "/home/hellzpgk/public_html/chat_api/Classes/PushNotification.php";

$push = new PushNotification();

$DB = new mysql_functions();

$user = new user();

$con = mysqli_connect("localhost", "hellzpgk_chatUse", "C$[UD=WWGgLh", "hellzpgk_chatApp");

$sql = "SELECT n_title, n_countries FROM news WHERE n_id=$last_msg_id";
                        
$res = mysqli_query($con, $sql);

echo $sql . "\n";
echo json_encode(mysqli_error($con)) . "\n";

$news = $res->fetch_assoc();

if (!$news) {
    echo "News not found \n";
    exit;
}

$tokens = $user->get_all_users_tokens(null, null, null, null, $news['n_countries']);


$chunks = array_chunk($tokens, 999);

for ($x = 0; $x < count($chunks); $x++) {
    $push->send_push_to_news($chunks[$x],"Textile News", $news['n_title'] ,"");
}
