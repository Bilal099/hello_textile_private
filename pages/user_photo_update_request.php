<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

include_once "../include/header.php";
?>

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    User Photo update Requests
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            <?php echo $user->get_users_for_update_approval();?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
<?php
include_once "../include/footer.php"
?>

<script>
$(document).on("click",".approve-btn",function(e){
    var user_id = $(this).data('user-id');
    console.log(user_id);
    var obj = $(this);
    $.ajax({
        type: "POST",
        data: {'request':'approve_user_photo_update_request','u_id':user_id},
        url: "ajaxcall.php",
        success: function(data)
        {
            var result = JSON.parse(data);
            var type = result.success ? 'success' : 'error';
            obj.parent().parent().remove();
            swal('Success', result.message, type);
        }
    });
});
$(document).on("click",".decline-btn",function(e){
    var user_id = $(this).data('user-id');
    var obj = $(this);
    $.ajax({
        type: "POST",
        data: {'request':'decline_user_photo_update_request','u_id':user_id},
        url: "ajaxcall.php",
        success: function(data)
        {
            var result = JSON.parse(data);
            var type = result.success ? 'success' : 'error';
            obj.parent().parent().remove();
            swal('Success', result.message, type);
        }
    });
});
</script>
