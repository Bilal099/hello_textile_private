<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 11/22/2018
 * Time: 3:30 PM
 */
include_once "../include/header.php";
?>

<!-- Default Media -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    UN-Approve CHATS
                    <small>The messages not approve yet.</small>
                </h2>
<!--                <ul class="header-dropdown m-r--5">-->
<!--                    <li class="dropdown">-->
<!--                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">-->
<!--                            <i class="material-icons">more_vert</i>-->
<!--                        </a>-->
<!--                        <ul class="dropdown-menu pull-right">-->
<!--                            <li><a href="javascript:void(0);">Action</a></li>-->
<!--                            <li><a href="javascript:void(0);">Another action</a></li>-->
<!--                            <li><a href="javascript:void(0);">Something else here</a></li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                </ul>-->
            </div>
            <div id="load_un_approve_chat_here" class="body">
                <!-- Page Loader -->
                <div id="loading" class="page-loader-wrapper">
                    <div class="loader">
                        <div class="preloader">
                            <div class="spinner-layer pl-red">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                        <p>Please wait...</p>
                    </div>
                </div>
                <!-- #END# Page Loader -->
                    <?php echo $chat->select_un_approve_chat();?>
            </div>
        </div>
    </div>
</div>
<!-- #END# Default Media -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <input type="text" id="current_message" class="form-control user_msg_view" tabindex="1">
<!--                        <label class="form-label">Message</label>-->
                    </div>
                </div>
<!--                <textarea rows="2" id="current_message" class="form-control user_msg_view"></textarea>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" tabindex="3" data-id="" id="btn_save">SAVE CHANGES</button>
                <button type="button" class="btn btn-link waves-effect" tabindex="4" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<?php
include_once "../include/footer.php";
?>
<script>
    // For Edit Buttin Click
    $(document).on('click','.edt_button',function () {
        var id = $(this).attr("data-id");
        $("#btn_save").attr("data-id",id);
        $('#defaultModalLabel').text($(this).closest(".media-heading").text());
        var msg = $(this).parent("h4").next("p").text();
        $('.user_msg_view').val(msg);
        $('#defaultModal').modal('show');
        $('#defaultModal').modal('refresh');

    });



    // For Edit Buttin Click
    $(document).on('click','#btn_save',function () {
        var name = $('#defaultModalLabel').text();
        var id = $(this).attr("data-id");
        var message = $('#current_message').val();
        approve_chat(id,message);
    });
    function approve_chat(chat_id,chat) {
        $('#defaultModal').modal('hide');
        $('.page-loader-wrapper').fadeIn();
        $.ajax({
            type: "POST",
            async:false,
            data: { request:'approve_chat', chat_id: chat_id,chat:chat },
            url: "ajaxcall.php",
            success: function(data)
            {
                load_unapprove_chat();
                get_unread_count_msg();
                $('.page-loader-wrapper').fadeOut();
                swal(data);
            }
        });
        // e.preventDefault();
        // e.destroy();
    }

    //For Enter Button
    $('#current_message').keyup(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            var id = $("#btn_save").attr("data-id");
            var message = $('#current_message').val();
            approve_chat(id,message);
        }
    });

</script>