<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

include_once "../include/header.php";
?>

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Subcategory List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parent Category</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Parent Category</th>
                                <th>Status</th>
                                <th>Edit</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php echo $category_obj->get_all_sub_category(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

<?php
include_once "../include/footer.php"
?>