<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 1:59 PM
 */
include_once "../include/header.php";
if(isset($_POST['subBtn']))
{
    $name = $_POST['name'];
    $status = $_POST['status'];
    $sub_cat = $_POST['sub_act'];
    echo $category_obj->add_category($name,$sub_cat,$status);

}
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Add New Category</h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <form id="form_validation" enctype="multipart/form-data" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" required>
                            <label class="form-label">Title</label>
                        </div>
                    </div>
                
                    <div class="col-md-6">
                        <p>
                            <b>Have Sub Category</b>
                        </p>
                        <select name="sub_act" class="form-control show-tick">
                            <option value="0">Select Option</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>

                    </div>
                    <div class="col-md-6">
                        <p>
                            <b>Status</b>
                        </p>
                        <select name="status" class="form-control show-tick">
                            <option value="0">Select Option</option>
                            <option value="1">Active</option>
                            <option value="0">In-Active</option>
                        </select>
                    </div>
                    <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT</button>
                </form>

            </div>
        </div>

    </div>
</div>

<?php
include_once "../include/footer.php";
?>
<!-- #END# Basic Validation -->
