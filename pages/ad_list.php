<?php
/**
 * Created by PhpStorm.
 * User: M.DilawarKhanAzeemi
 * Date: 1/24/2019
 * Time: 1:14 PM
 */
include_once "../include/header.php";
?>

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Ads List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Advertisement</th>
                                <th>Watch Time</th>
                                <th>Watch Time PerDay</th>
                                <th>Creation Time</th>
                                <th>Countries</th>
<!--                                <th>Edit</th>-->
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Advertisement</th>
                                <th>Watch Time</th>
                                <th>Watch Time PerDay</th>
                                <th>Creation Time</th>
                                <th>Countries</th>
<!--                                <th>Edit</th>-->
                                <th>Delete</th>
                            </tr>
                            </tfoot>
                            <tbody>
                                <? echo $ads->load_Ad(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

<?php
include_once "../include/footer.php"
?>

<script>
    $(".ad_delete_btn").click(function () {
        var id = $(this).val();
        var obj = $(this);
        delete_ad(id,obj);
        //alert(id);
    });

    function delete_ad(id,obj)
    {
        $.ajax({
            type: "POST",
            async:false,
            data: { request:'delete_ads', ad_id:id },
            url: "ajaxcall.php",
            success: function(data)
            {
                obj.parent().parent().remove();
                swal(data);
            }
        });
    }
</script>
