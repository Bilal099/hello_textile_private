<?php
session_start();
include "../include/config.php";

if (!isset($_SESSION['admin_email'])) {
    exit(1);
}

$ad_file = $_POST['ad_file'];
$adv_id = $_POST['adv_id'];

// when ad is in database (request comes from video_ad_list.php).
if (isset($adv_id)) {

    $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $result = mysqli_fetch_assoc(mysqli_query($con, "SELECT ad_file FROM video_ads WHERE advertiser_id='$adv_id'"));
    if(!$result) {
        echo json_encode(["success" => false]);
        http_response_code(400);
        exit();
    }
    
    $ad_file = $result['ad_file'];
    if(mysqli_query($con, "DELETE FROM video_ads WHERE advertiser_id='$adv_id'")){
        unlink("../upload/" . $ad_file);
        http_response_code(200);
    } else {
        http_response_code(400);
    }
}

// when ad may not be in database and we only need to delete the video file (request comes from edit_video_ad.php and new_video_ad.php)
else if (isset($ad_file)) {
    unlink("../upload/" . $ad_file);
    http_response_code(200);
}
else {
    http_response_code(400);
    echo json_encode(["success" => false]);
}

?>