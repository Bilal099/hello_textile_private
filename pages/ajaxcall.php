<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 11/22/2018
 * Time: 4:43 PM
 */
include_once realpath(__DIR__."/..")."/include/globals.php";
$req_key = $_POST['request'];
switch ($req_key)
{
    case 'get_unread_count_msg':
        echo $chat->get_unread_msg_count();
    break;

    case 'get_count_of_hold_messages':
        echo $chat->get_count_hold_msg();
        break;

    case 'load_un_approve_chat':
        echo $chat->select_un_approve_chat();
        break;

    case 'approve_chat':
        $chat_id = $_POST['chat_id'];
        $c_id = $_POST['cat_id'];
        $sc_id = $_POST['sub_cat_id'];
        $msg = $_POST['chat'];
        $approved_by = $_POST['approved_by'];
        
        if (isset($chat_id) && isset($c_id) && isset($sc_id) && isset($msg) && isset($approved_by)) {
            echo $chat->approve_chat($chat_id, $c_id, $sc_id, $msg, $approved_by);
        } else {
            echo "Missing params";
        }
        break;

    case 'approve_hold_chat':
        $chat_id = $_POST['chat_id'];
        echo $chat->approve_hold_chat($chat_id);
        break;

    case 'delete_message':
        $id = $_POST['id'];
        echo $chat->deleteMessage($id);
        break;

    case 'load_sub_category_for_send_message':
        $cat_id = $_POST['catt_id'];
        echo $category_obj->select_sub_category_name_by_id($cat_id);
        break;

    case 'delete_package':
        echo $package->delete_package($_POST['package_id']);
        break;

    case 'delete_ads':
        $id = $_POST['ad_id'];
        echo $ads->delete_ad($id);
        break;

    case 'new_msg_for_admin_approve':
        $should_load_all = $_POST['should_load_all'];
        echo $chat->new_msg_for_approve($should_load_all);
        break;
    case 'set_msg_is_loaded':
        $msg_id = $_POST['message_id'];
        if (isset($msg_id)) {
            echo $chat->set_msg_is_loaded($msg_id);
        } else {
            echo "message_id not given";
        }
        break;
    case 'hold_message_approve':
        echo $chat->hold_message();
        break;

    case 'send_msg_to_hold':
        $mid = $_POST['m_id'];
        echo $chat->msgToHold($mid);
        break;

    case 'assign_user_a_package':
        $data = array(
            'pid'   =>  $_POST['pid'],
            'prID'  =>  $_POST['prID'],
            'u_id'  =>  $_POST['u_id']
        );
        echo $package->assign_package_user($data);
        break;

    case 'delete_pkg_request':
        $pr_id = $_POST['pr_id'];
        echo $package->delete_package_request($pr_id);
        break;
        
    case 'approve_user_photo_update_request':
        $u_id = $_POST['u_id'];
        echo $user->approve_user_photo_update_request($u_id);
        break;
    
    case 'decline_user_photo_update_request':
        $u_id = $_POST['u_id'];
        echo $user->decline_user_photo_update_request($u_id);
        break;
        
    case 'get_e_rate':
        echo $package->get_e_rate();
        break;
        
    case 'update_e_rate':
        $e_rate = $_POST['e_rate'];
        if (isset($e_rate) && is_numeric($e_rate)) {
            echo $package->update_e_rate($e_rate);
        } else {
            echo json_encode(["success"=>false]);
        }
        break;

    case 'get_selected_country_user':
        $country        = $_POST['country'];
        echo $user->get_selected_country_user($country);
        break;

    case 'get_user_seller_profile_count':
        echo $user->get_user_seller_profile_count();
        break;

    case 'get_user_buyer_profile_count':
        echo $user->get_user_buyer_profile_count();
        break;

    case 'user_seller_profile_count_seen':
        echo $user->user_seller_profile_count_seen();
        break;

    case 'user_buyer_profile_count_seen':
        echo $user->user_buyer_profile_count_seen();
        break;

    default: 
        echo "Bad request";
        break;
}