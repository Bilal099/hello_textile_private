<?php
/**
 * Created by DK_KHAN.
 * User: DK_KHAN
 * Date: 10/4/2018
 * Time: 1:02 PM
 */
header('Content-type: application/json');
include_once "include/globals.php";

//take Request Key valuse from Andoird Via Post
$req_key  = $_POST['req_key'];

// check which Methode should execute
switch($req_key)
{
    //User Registration
    case 'user_registration':
        $user_arr = array(
            'name'              =>  trim($_POST['name']),
            'email'             =>  isset($_POST['email'])? $_POST['email']:'',
            'contact'           =>  isset($_POST['contact'])? $_POST['contact']:'',
            'pass'              =>  isset($_POST['pass'])? $_POST['pass']:'',
            'city'              =>  trim($_POST['city']),
            'company'           =>  trim($_POST['company']),
            'company_address'   =>  trim($_POST['company_address']),
            'bussniess'         =>  trim($_POST['bussniess']),
            'user_type'         =>  isset($_POST['user_type'])? $_POST['user_type']:'',
            'fcm_id'            =>  isset($_POST['fcm_id'])? $_POST['fcm_id']:'',
            'device_id'         =>  isset($_POST['device_id'])? $_POST['device_id']:'',
            'country'           =>  trim($_POST['country']),
            'ios'               =>  isset($_POST['ios']) ?  $_POST['ios'] : 0,
        );
        echo $users->user_registration($user_arr);
    break;
    
    // user save profile
    case 'save_profile':
        $u_id         = $_POST['u_id'];
        $profile_type = isset($_POST['profile_type'])?   $_POST['profile_type'] : "";
        $business_cat = isset($_POST['business_cat'])?   $_POST['business_cat'] : "";
        $business_nat = isset($_POST['business_nat'])?   $_POST['business_nat'] : "";
        $desc         = isset($_POST['desc'])?           $_POST['desc'] : "";
        $company      = isset($_POST['company'])?        $_POST['company'] : "";
        $phone        = isset($_POST['phone'])?          $_POST['phone'] : "";
        $email        = isset($_POST['email'])?          $_POST['email'] : "";
        $country      = isset($_POST['country'])?        $_POST['country'] : "";
        $city         = isset($_POST['city'])?           $_POST['city'] : "";
        $address      = isset($_POST['address'])?        $_POST['address'] : "";
        $products     = isset($_POST['products'])?       $_POST['products'] : "";
        
        if (!isset($u_id)) {
            echo json_encode(['success' => false, 'message' => 'user id is missing']);
        }
        else if (!isset($profile_type) || !in_array($profile_type, ['seller', 'buyer'])) {
            echo json_encode(['success' => false, 'message' => 'profile type is missing or invalid']);
        }
        else if (!isset($business_cat) || !in_array($business_cat, ['manufacturer', 'trader', 'importer', 'exporter', 'wholesaler', 'other'])) {
            echo json_encode(['success' => false, 'message' => 'business category is missing or invalid']);
        }
        else if (!isset($desc)) {
            echo json_encode(['success' => false, 'message' => 'desc is missing']);
        }
        else if (!isset($company)) {
            echo json_encode(['success' => false, 'message' => 'company is missing']);
        }
        else if (!isset($phone)) {
            echo json_encode(['success' => false, 'message' => 'phone is missing']);
        }
        else if (!isset($email)) {
            echo json_encode(['success' => false, 'message' => 'email is missing']);
        }
        else if (!isset($country)) {
            echo json_encode(['success' => false, 'message' => 'country is missing']);
        }
        else if (!isset($city)) {
            echo json_encode(['success' => false, 'message' => 'city is missing']);
        }
        else if (!isset($address)) {
            echo json_encode(['success' => false, 'message' => 'address is missing']);
        }
        // else if (!isset($products) || !is_array($products)) {
        //     echo json_encode(['success' => false, 'message' => 'products is missing or invalid']);
        // }
        else {
            $arr = array(
                'u_id'          => trim($u_id),
                'profile_type'  => $profile_type,
                'business_cat'  => $business_cat,
                'business_nat'  => trim($business_nat),
                'desc'          => trim($desc),
                'company'       => trim($company),
                'phone'         => trim($phone),
                'email'         => trim($email),
                'country'       => trim($country),
                'city'          => trim($city),
                'address'       => trim($address),
                'products'      => ($products!="")? implode(" , ", array_map("trim", $products)):"",
            );
            echo $users->save_profile($arr);
        }
        break;
        
// user profile search
    case 'search_profiles':
        $u_id           = isset($_POST['u_id'])?             $_POST['u_id']:"";
        $page           = isset($_POST['page'])?             $_POST['page']:"";
        $limit          = isset($_POST['limit'])?            $_POST['limit']:"";
        $profile_type   = isset($_POST['profile_type'])?     $_POST['profile_type']:"";
        $business_cat   = isset($_POST['business_cat'])?     $_POST['business_cat']:""; 
        $business_nat   = isset($_POST['business_nat'])?     $_POST['business_nat']:""; 
        $country        = isset($_POST['country'])?          $_POST['country']:"";
        $city           = isset($_POST['city'])?             $_POST['city']:"";
        $keywords       = isset($_POST['keywords'])?         $_POST['keywords']:"";    
        $company        = isset($_POST['company'])?          $_POST['company']:"";             
        
        if (!isset($u_id)) {
            echo json_encode(['success' => false, 'message' => 'user id is missing']);
        }
        else if (!isset($profile_type) || !in_array($profile_type, ['seller', 'buyer'])) {
            echo json_encode(['success' => false, 'message' => 'profile type is missing or invalid']);
        }
        else {
            $arr = array(
                'u_id'          => trim($u_id),
                'profile_type'  => $profile_type,
                'business_cat'  => trim($business_cat),
                'business_nat'  => trim($business_nat),
                'country'       => trim($country),
                'city'          => trim($city),
                'keywords'      => trim($keywords),
                'page'          => intval($page),
                'limit'          => intval($limit),
                'company'       => $company,
            );
            echo $users->search_profiles($arr);
        }
        break;

    //User Login
    case 'user_login':
        $email  = $_POST['email'];
        $pass = $_POST['pass'];
        $token = $_POST['token'];
        $device_id = $_POST['device_id'];
        $ios = isset($_POST['ios']) ? $_POST['ios'] : 0;
        echo $users->user_login($email,$pass,$token,$device_id,$ios);
        break;

    case 'delete_group_message':
        $u_id = $_POST['u_id'];
        $msg_id = $_POST['msg_id'];
        echo $massenger->delete_group_message($u_id, $msg_id);
        break;
    
    case 'delete_private_message':
        $u_id = $_POST['u_id'];
        $msg_id = $_POST['msg_id'];
        echo $massenger->delete_private_message($u_id, $msg_id);
        break;
        
    case 'delete_all_groups_messages':
        $u_id = $_POST['u_id'];
        echo $massenger->delete_all_groups_messages($u_id);
        break;
        
        
    case 'delete_all_private_messages':
        $u_id = $_POST['u_id'];
        $to_id = $_POST['to_id'];
        echo $massenger->delete_all_private_messages($u_id, $to_id);
        break;
        
    case 'delete_all_messages':
        $u_id = $_POST['u_id'];
        echo $massenger->delete_all_messages($u_id);
        break;
        
    // All Category
    case 'all_category':
        $u_id = ((isset($_POST['u_id']))? $_POST['u_id']: 0);
        echo $cat->get_all_category($u_id);
        break;

    // All Sub Category
    case 'all_sub_category':
        $c_id = $_POST['c_id'];
        echo $cat->get_sub_cateory($c_id);
        break;
    
    case 'get_all_cat_and_subcat':
        echo $cat->get_all_cat_and_subcat();
        break;

    // add masseging
    case 'add_messges':
        $cat_id = $_POST['cat_id'];
        $sub_cat_id = $_POST['sub_cat_id'];
        $user_id = $_POST['user_id'];
        $mesg = addslashes($_POST['message']);

        //for web push notification
        $mesg_arr[] = array(
            'cat_id'        =>  $cat_id,
            'sub_cat_id'    =>  $sub_cat_id,
            'msg'           => $mesg
        );
        $json_arr = json_encode($mesg_arr[0]);
        $push->notify_web($json_arr);

        //call the function
        echo $massenger->add_message($cat_id,$sub_cat_id,$user_id,$mesg,$_FILES);

    break;

    // Retrive Masseges for category
    case 'retrive_category_masseges':
        $c_id = $_POST['c_id'];
        $countries = isset($_POST['countries']) ? $_POST['countries'] : null;
        $time = isset($_POST['time']) ? $_POST['time'] : null;
        $u_id = isset($_POST['u_id']) ? $_POST['u_id'] : null;
        echo $massenger->get_Category_messages($c_id, $countries, $time, $u_id);
        break;


    // Retrive All un approve message of user a/c category
    case 'load_un_approve_messges_of_user_ac_category':
        $c_id = $_POST['c_id'];
        $sc_id = $_POST['sc_id'];
        $user_id = $_POST['user_id'];
        echo $massenger->get_un_Approve_msg_of_user($c_id,$sc_id,$user_id);
        break;

    // Retrive Masseges for Subcategory
    case 'retrive_sub_category_masseges':
        $c_id = $_POST['c_id'];
        $sc_id = $_POST['sc_id'];
        $countries = isset($_POST['countries']) ? $_POST['countries'] : null;
        $time = isset($_POST['time']) ? $_POST['time'] : null;
        $u_id = isset($_POST['u_id']) ? $_POST['u_id'] : null;
        echo $massenger->get_Sub_Category_messages($c_id,$sc_id, $countries, $time, $u_id);
        break;

    // User Profile Detail By Id
    case 'get_user_profile_detail_by_id':
        $user_id = $_POST['u_id'];
        $own_id = $_POST['own_id'];
        echo $users->get_user_profile_detail_by_id($user_id,$own_id);
        break;

    // get all News
    case 'get_all_news':
        $country = $_POST['u_country'];
        echo $news->get_news($country);
        break;

    // get all Notification
    case 'get_all_Notification':
        $country = $_POST['u_country'];
        echo $news->get_Notification($country);
        break;

    // get all Single Chat
    case 'get_all_singal_chat':
        $from_id = $_POST['from_id'];
        $to_id = $_POST['to_id'];
        $time = isset($_POST['time']) ? $_POST['time'] : null;
        echo $massenger->get_single_chat($from_id,$to_id, $time);
        break;

    // add single chat
    case 'add_singal_chat':
        $from_id = $_POST['from_id'];
        $to_id = $_POST['to_id'];
        $msg = addslashes($_POST['message']);
        echo $massenger->add_single_chat($from_id,$to_id,$msg, $_FILES);
        break;

    // Check if user package is expired
    case 'check_user_expiration':
        $user_id = $_POST['user_id'];
        $device_id = $_POST['device_id'];
        echo $users->check_if_user_expire($user_id, $device_id);
        break;

    // getting all ads
    case 'all_advertisments':
        $country = $_POST['u_country'];
        echo $ads->get_ads($country);
        break;

    // Check  user chats with other for place holder
    case 'private_chat':
            $user_id = $_POST['usr_id'];
        echo $pcu->private_chat_users($user_id);
        break;

    case 'update_token':
        $user_id = $_POST['u_id'];
        $token = $_POST['token'];
        echo $users->update_token($user_id, $token);
        break;
    // update device id for login to new device
    case 'update_device_id_for_new_device':
        $device_id = $_POST['device_id'];
        $email = $_POST['email'];
        echo $users->update_device_id($email,$device_id);
        break;

    // get user package detail by id
    case 'get_my_package_detail':
        $user_id = $_POST['user_id'];
        echo $users->select_user_package_detail($user_id);
        break;


    // Private chat messages
    case 'private_chat_message':
        $from_id = $_POST['from_id'];
        $to_id = $_POST['to_id'];
        $limit = $_POST['limit'];
        echo $pcu->private_chat_message($from_id,$to_id,$limit);
        break;

        //get all blogs
    case 'get_all_blogs':
        echo $blogs->get_blogs();
        break;

    // get pkg for pakistani user
    case 'get_package_for_pakistani_user':
        echo $pkg->load_pkg_for_pakistani();
        break;

    // get pkg for International user
    case 'get_package_for_international_user':
        echo $pkg->load_pkg_for_international();
        break;

    // When request for buy a package
    case 'user_request_to_buy_pkg':
        $param = array(
            'pkg_id'    => $_POST['pkg_id'],
            'user_id'   => $_POST['user_id'],
        );
        echo $pkg->user_request_for_package_buy($param);
        break;

    // When user buy a package
    case 'user_want_to_buy_pkg':
        $param = array(
            'pkg_id'    => $_POST['pkg_id']
        );
        echo $pkg->user_want_to_buy_package($param);
        break;

    // update Ads watch time perday
    case 'update_ads_Watch_time':
        $ad_id = $_POST['ad_id'];
        echo $ads->update_ads_Watch_time($ad_id);
        break;

    // Forgot Password
    case 'forgot_password':
        $email = $_POST['email'];
        echo $users->forgot_password($email);
        break;
    
    case 'update_password':
        $u_id = $_POST['u_id'];
        $opwd = $_POST['opwd'];
        $pwd = $_POST['pwd'];
        $cpwd = $_POST['cpwd'];
        if (isset($u_id) && isset($opwd) && isset($pwd) && isset($cpwd)) {
            echo $users->update_password($u_id, $opwd, $pwd, $cpwd);   
        } else {
            echo json_encode(["success" => false, "message" => "Missing fields."]);
        }
        break;

    // Contact Us
    case 'Contact_us':
        $param = array(
            'name'      => $_POST['name'],
            'email'     => $_POST['email'],
            'phone'     => $_POST['phone'],
            'message'   => $_POST['message'],
            'subject'   => $_POST['subject']
        );
        echo $users->contact_us($param);
        break;

    // Update User Detail Start
    case 'update_user_detail':
       $img = "";
        $u_id 		= $_POST['u_id'];
        $u_name 	= $_POST['u_name'];
        $u_contact 	= $_POST['u_contact'];
        $u_city = $_POST['u_city'];
        $u_company = $_POST['u_company'];
        $u_nature = $_POST['u_nature'];
        $u_address = $_POST['u_address'];
        $country = $_POST['country'];

        $directoryName = "../upload/";
        $target_file = $directoryName . basename($_FILES["file_name"]["name"]);
        $name = basename($_FILES["file_name"]["name"]);
        if(!empty($name))
        {
            if (move_uploaded_file($_FILES["file_name"]["tmp_name"], $target_file))
            {
                // Call the function
                $img = "https://".$_SERVER['SERVER_NAME']."/upload/".basename($_FILES["file_name"]["name"]);
                echo $users->update_user_detail($u_id,$u_name,$u_contact,$u_city,$u_company,$u_nature,$u_address,$img,$country);
            }
            else
            {
                $msg = "Unable to update profile.";
                $data = ['success'=>false,'message'=>$msg];
                echo json_encode($data);
            }
        }
        else
        {
            echo $users->update_user_detail($u_id,$u_name,$u_contact,$u_city,$u_company,$u_nature,$u_address,$img,$country);
        }

        break;
        
        // Default Case
    case "get_badges":
        $u_id = $_POST['u_id'];
        if (isset($u_id) && intval($u_id) > 0) {
            echo $massenger->get_badges($u_id);
        } 
        else {
            echo json_encode(["success"=> false, "message"=>"Invalid u_id"]);
        }
        break;
    
    case "update_badges":
        $type = $_POST['type'];
        $u_id = $_POST['u_id'];
        $c_id = $_POST['c_id'];
        $sub_cid = $_POST['sub_cid'];
        $from_id = $_POST['from_id'];
        $count = $_POST['count'];
        $all = $_POST['all'];
        $all = isset($all) ? $all : false;
        
        if ($all && isset($u_id)) {
            echo $massenger->update_badges(trim($type), trim($u_id), null, null, null, null);
            break;
        }
        if (!isset($type) || !isset($u_id) || !isset($count)) {
            echo json_encode(["success" => false, "message"=> "Missing params"]);
            break;
        } 
        $type = trim($type);
        $u_id = trim($u_id);
        $count = intval($count);
        if ($u_id < 0) {
            echo json_encode(["success" => false, "message"=> "Invalid u_id"]);
            break;
        }
        if ($count >= 0) {
            echo json_encode(["success" => false, "message"=> "count should be negative"]);
            break;
        }
        if ($type == "pc") {
            if (!isset($from_id)) {
                echo json_encode(["success" => false, "message"=> "Missing params"]);
                break;
            }
            $c_id = -1;
            $sub_cid = -1;
        }
        if ($type == "gc") {
            if (!isset($c_id)) {
                echo json_encode(["success" => false, "message"=> "Missing params"]);
                break;
            }
            $from_cid = -1;
            $sub_cid = isset($sub_cid) ? $sub_cid : -1;
        }
        if ($type == 'ns') {
            $cid = -1;
            $sub_cid = -1;
            $from_id = -1;
        }
        echo $massenger->update_badges($type, $u_id, $c_id, $sub_cid, $from_id, $count);
        break;
        
    case 'buy_package':
        $u_id = trim($_POST['u_id']);
        $p_id = trim($_POST['p_id']);
        $type = trim($_POST['type']);
        $mobile = trim($_POST['mobile']);
    
        if (isset($u_id) && isset($p_id) && isset($type)) {
            if ($type == 'ma' || $type == 'cc') {
                echo $pkg->buy_package($u_id, $p_id, $type, $mobile);
            } 
            else {
                echo json_encode(["success" => false, 'message'=> 'Invalid type']);            
            }
        }
        else {
            echo json_encode(["success" => false, 'message'=> 'Missing params']);        
        }
        break;
        
    
    case "update_country_filter":
        $u_id = trim($_POST['u_id']);
        $countries = trim($_POST['countries']); // excluded countries, comma separated
        if (isset($u_id) && isset($countries)) {
            echo $users->update_country_filter($u_id, $countries);
        } else {
            echo json_encode(["success" => false, 'message' => ' Missing params']);
        }
        break;
        
    case "update_group_notif_settings":
        $u_id = intval($_POST['u_id']);
        $c_id = intval($_POST['c_id']);
        $sub_cid = intval($_POST['sub_cid']);
        $should_block = intval($_POST['block']); // boolean
        if (
            $u_id > 0 && 
            ($should_block == 0 || $should_block == 1) && 
            ($c_id > 0 || $sub_cid > 0)) {

            echo $users->update_group_notif_settings($u_id, $c_id, $sub_cid, $should_block);
        } else {
            echo json_encode(["success" => false, 'message' => 'Missing or invalid params']);
        }
        break;
    case "get_group_notif_settings":
        $u_id = intval($_POST['u_id']);
        if ($u_id > 0) {
            echo $users->get_group_notif_settings($u_id);
        } else {
            echo json_encode(["success" => false, 'message' => 'Missing or invalid params']);
        }
        break;
    case "fetch_video_ad":
        $u_id = intval($_POST['u_id']);
        if ($u_id > 0) {
            echo $ads->fetch_video_ad($u_id);
        } else {
            echo json_encode(["success" => false, 'message' => 'Missing or invalid params']);
        }
        break;
    case "update_video_ad_link_click":
        $u_id = intval($_POST['u_id']);
        $ad_id = $_POST['ad_id'];
        if ($u_id > 0 && isset($ad_id)) {
            echo $ads->update_video_ad_link_click($u_id, trim($ad_id));
        } else {
            echo json_encode(["success" => false, 'message' => 'Missing or invalid params']);
        }
        break;
    case "update_ad_view":
        $u_id = intval($_POST['u_id']);
        $ad_id = $_POST['ad_id'];
        if ($u_id > 0) {
            echo $ads->update_ad_view($u_id, $ad_id);
        } else {
            echo json_encode(["success" => false, 'message' => 'Missing or invalid params']);
        }
        break;

        // user profile search new 
    case 'search_profiles_new':
        $u_id           = isset($_POST['u_id'])?             $_POST['u_id']:"";
        $page           = isset($_POST['page'])?             $_POST['page']:"";
        $limit          = isset($_POST['limit'])?            $_POST['limit']:"";
        $profile_type   = isset($_POST['profile_type'])?     $_POST['profile_type']:"";
        $business_cat   = isset($_POST['business_cat'])?     $_POST['business_cat']:""; 
        $business_nat   = isset($_POST['business_nat'])?     $_POST['business_nat']:""; 
        $country        = isset($_POST['country'])?          $_POST['country']:"";
        $city           = isset($_POST['city'])?             $_POST['city']:"";
        $keywords       = isset($_POST['keywords'])?         $_POST['keywords']:"";    
        $company        = isset($_POST['company'])?          $_POST['company']:"";     
        $search_column  = isset($_POST['search_column'])?    $_POST['search_column']:"";           
        
        if (!isset($u_id)) {
            echo json_encode(['success' => false, 'message' => 'user id is missing']);
        }
        else if (!isset($profile_type) || !in_array($profile_type, ['seller', 'buyer'])) {
            echo json_encode(['success' => false, 'message' => 'profile type is missing or invalid']);
        }
        else {
            $arr = array(
                'u_id'          => trim($u_id),
                'profile_type'  => $profile_type,
                'business_cat'  => trim($business_cat),
                'business_nat'  => trim($business_nat),
                'country'       => trim($country),
                'city'          => trim($city),
                'keywords'      => trim($keywords),
                'page'          => intval($page),
                'limit'          => intval($limit),
                'company'       => $company,
                'search_column' => $search_column,
            );
            echo $users->search_profiles_new($arr);
        }
        break;

    case 'all_category_with_sub_category':
        echo $cat->get_all_category_with_sub_category();
        break;

    default:
        $message = "Bad Request";
        $data 	= ['success' => false,'message'=>$message, 'req_key'=>$req_key];
        echo json_encode($data);
        break;
}
