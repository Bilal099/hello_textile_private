<?php
/**
 * Created by DK_KHAN.
 * User: DK_KHAN
 * Date: 10/9/2018
 * Time: 1:13 PM
 */

class Massenger
{
    function delete_group_message($u_id, $msg_id){
        $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $sql = "DELETE FROM messages WHERE message_id = '$msg_id' AND user_id = '$u_id'";
        $res = mysqli_query($con, $sql);
        $af = mysqli_affected_rows($con);
        $msg = "";
        $success = false;
        if ($res && $af == 1) {
            $success = true;
            $msg = "Message deleted successfully";
        } else {
            $msg = "Could not delete message";
        }
        
        return json_encode(['success'=>$success, 'message'=>$msg]);
    }
    function delete_private_message($u_id, $msg_id){
        $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $sql = "DELETE FROM single_chat WHERE sc_id = '$msg_id' AND from_user_id = '$u_id'";
        $res = mysqli_query($con, $sql);
        $af = mysqli_affected_rows($con);
        $msg = "";
        $success = false;
        if ($res && $af == 1) {
            $success = true;
            $msg = "Message deleted successfully";
        } else {
            $msg = "Could not delete message";
        }
        
        return json_encode(['success'=>$success, 'message'=>$msg]);
    }
    function delete_all_groups_messages($u_id){
        $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $sql = "DELETE FROM messages WHERE user_id = '$u_id'";
        $res = mysqli_query($con, $sql);
        $msg = "";
        $success = false;
        if ($res) {
            $success = true;
            $msg = "All of your messages from all groups deleted successfully";
        } else {
            $msg = "Could not delete messages";
        }
        
        return json_encode(['success'=>$success, 'message'=>$msg]);
    }
    function delete_all_private_messages($u_id, $to_id){
        $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $sql = "DELETE FROM single_chat WHERE (from_user_id = '$u_id' AND to_user_id = '$to_id') OR (from_user_id = '$to_id' AND to_user_id = '$u_id')";
        $res = mysqli_query($con, $sql);
        $msg = "";
        $success = false;
        if ($res) {
            $success = true;
            $msg = "All of your private messages deleted successfully";
        } else {
            $msg = "Could not delete messages";
        }
        
        return json_encode(['success'=>$success, 'message'=>$msg]);
    }
    function delete_all_messages($u_id){
        $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        $sql1 = "DELETE FROM single_chat WHERE from_user_id = '$u_id'";
        $sql2 = "DELETE FROM messages WHERE user_id = '$u_id'";
        $res1 = mysqli_query($con, $sql1);
        $res2 = mysqli_query($con, $sql2);
        
        $msg = "";
        $success = false;
        if ($res1 && $res2) {
            $success = true;
            $msg = "All of your private and groups messages deleted successfully";
        } else {
            $msg = "Could not delete messages";
        }
        
        return json_encode(['success'=>$success, 'message'=>$msg]);
    }
    function check_user_total_msg($u_id)
    {
        global $DB;
        $sql = "SELECT u.u_total_msg FROM users u WHERE u.u_id = '$u_id'";
        $res = $DB->qr($sql);
        $tot_msg = $DB->fa($res);
        return $tot_msg['u_total_msg'];
    }

    function update_user_remain_user_msg($u_id)
    {
        global $DB;
        $total_msg = $this->check_user_total_msg($u_id);
        if(!$total_msg == 0)
        {
            $sql = "UPDATE users SET u_total_msg = u_total_msg-'1' WHERE u_id = '$u_id' ";
            if($DB->qr($sql))
            {
                return 1; // User message packege update success
            }
            else
            {
                return 2; // Error while user message package update
            }
        }
        else
        {
            return 3; // user dose not have message packages
        }
        return $total_msg;

    }
    function are_files_valid($files)
    {
        $valid_extensions = ['image/png', 'image/jpeg'];
        foreach($files as $file) {
            if (!in_array($file['type'], $valid_extensions)) {
                return 2; // invalid image type
            }
            if ($file['size'] > 10000000) {
                return 3; // size greater than 10 MB
            }
            if ($file['error']) {
                return 4; // upload error
            }
        }
        return 1;
    }
    function upload_files($files, $file_name_prefix) {
        $filesnames = [];
        foreach($files as $file) {
            $file_name_suffix = "_" . strval(time()) . "_" . basename($file['name']);
            $filename = $file_name_prefix . $file_name_suffix;
            if (move_uploaded_file($file["tmp_name"], image_upload_path. $filename)) {
                $filesnames[] = $filename;
            }
        }
        return $filesnames;
    }
     function sendPostRequest($fields) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://smt-reg.herokuapp.com/send_message/1",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => 1,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($fields),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;

    }
    function increment_msg_consumption($u_id) {
        global $DB;
        $DB->qr("UPDATE users SET u_msg_consume = u_msg_consume + 1 WHERE u_id=$u_id");
    }
    function add_message($cat_id,$sub_cat_id, $user_id, $message,$files)
    {
        global $DB, $users, $push;
        // $daily_msg_count = $this->get_daily_group_msg_count($user_id);
        // echo $daily_msg_count;
        // echo "bilal";
        // die();
        $date = date('m/d/Y h:i:s a', time());
        if (isset($files) && count($files) > 0) {
            if (count($files) > 10) {
                return json_encode(['success'=>false,'message'=> "You cannot upload more than 10 images at a time"]);
            }
            $are_files_valid = $this->are_files_valid($files);
            $error_messages = array(
                2 => "Only jpg and png files are allowed",
                3 => "Files size must be less than 10MB",
                4 => "Could not upload one or more images"
            );
            if ($are_files_valid != 1) {
                $data = ['success'=>false,'message'=>$error_messages[$are_files_valid]];
                return json_encode($data);
            } 
            else {
                $files_prefix = $cat_id . "_" . $sub_cat_id . "_" . $user_id;
                $filesnames = implode(",", $this->upload_files($files, $files_prefix));
                $sql = "INSERT INTO messages(category_id, sub_cat_id, user_id, message, images, created_at) VALUES ('$cat_id', '$sub_cat_id', '$user_id', '$message', '$filesnames', '$date')";
            }
        }
        else
        {
            $sql = "INSERT INTO messages(category_id, sub_cat_id, user_id, message, created_at) VALUES ('$cat_id', '$sub_cat_id', '$user_id', '$message', '$date')";
            
        }
        $last_msg_id = $DB->get_last_id($sql); // execuute the query and return the insert id
        if(isset($last_msg_id))
        {
            $this->increment_msg_consumption($user_id);
            $msg_sql = "SELECT message_id, category_id, sub_cat_id, user_id, message, created_at, status FROM messages WHERE message_id = '$last_msg_id' order by message_id asc ";
            $res = $DB->qr($msg_sql);
            $result = $DB->fa($res);
            $token = $users->get_one_user_tokens($user_id);
            $user_data = $users->get_user_by_id($result['user_id']);
            if($result['sub_cat_id'] == 0)
            {
                $msg = "Please wait for Admin approval."; // $result['message']
                $push->sendToTopic($token, $msg,$result['user_id'],$user_data['u_name'],$result['created_at'],"category",$result['category_id'],$result['sub_cat_id']);
                $data = ['success'=>true,'message'=>$msg];
            }
            else
            {
                $msg = "Please wait for Admin approval.";
                $data = ['success'=>true,'message'=>$msg];
                $push->sendToTopic($token,$msg,$result['user_id'],$user_data['u_name'],$result['created_at'],"sub_category",$result['category_id'],$result['sub_cat_id']);
            }
            
        }
        else
        {
            //error
            $msg = "Failed to send message.";
            $data = ['success'=>false,'message'=>$msg];
        }
        return json_encode($data);


    }

    function get_Category_messages($cat_id, $countries = null, $time = null, $u_id = null)
    {
//        $unapprove_msg = $this->get_un_Approve_msg_of_user($cat_id,$user_id);
//        $category_message = $this->load_Category_messages($cat_id);
//        $msg =
//        array(
//            'un_approve_messages'    =>  $unapprove_msg,
//            'category_messages'      =>  $category_message
//        );
//        return json_encode($msg);

        global $DB;
        $sql = "";
        $countries = isset($countries) ? trim($countries) : null;
        
        // don't show messages before $time or if $time is not given, dont show 6 month older messages
        // $timeFilter = "AND str_to_date(m.created_at, '%m/%d/%Y %r') >= DATE_ADD(NOW(), INTERVAL -6 MONTH)";
        $timeFilter = "";
        // if u_id is given, dont show messages older than p_data_backup_duration
        if (isset($u_id)) {
            $sql = "SELECT p_data_backup_duration FROM users, packeages WHERE u_id=$u_id AND pkg_id=p_id";
            $res = $DB->qr($sql);
            $res = $DB->fa($res);
            $data_backup_duration = '-' . $res['p_data_backup_duration'];
            $start_date = date("m/d/Y", strtotime($data_backup_duration));
            // $timeFilter = "AND str_to_date(m.created_at, '%m/%d/%Y %r') >= str_to_date('$start_date', '%m/%d/%Y')";
            $this->update_user_last_active($u_id);
        }
        
        
        $uIdFilter = isset($u_id) ? "u.u_id=$u_id or " : "";
        if (isset($countries) && !empty($countries)) 
        {
            $countries = array_map(function($a){return "'".trim($a)."'";}, explode(',', $countries));
        }
        if (isset($countries) && !empty($countries) && count($countries) > 0) 
        {
            $countries = implode(',', $countries);
            $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.images, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold FROM messages m LEFT JOIN users u on u.u_id = m.user_id WHERE m.category_id = '$cat_id' and m.status = 1 and (m.user_id=1 or ($uIdFilter u.u_country in (".$countries."))) $timeFilter AND m.expired = 0 order by message_id asc";
        } 
        else {
            $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.images, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold FROM messages m LEFT JOIN users u on u.u_id = m.user_id WHERE m.category_id = '$cat_id' and m.status = 1 $timeFilter AND m.expired = 0 order by message_id asc ";
        }
        $res = $DB->qr($sql);
        $data = [];
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $r['u_id'] = $r['u_id'] == null ? '1' : $r['u_id'];
                $r['u_name'] = $r['u_name'] == null ? 'Admin' : $r['u_name'];
                $data[] = $r;
            }
        }
        return json_encode($data);
    }

    function load_Category_messages($cat_id)
    {
        global $DB;
        $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold FROM messages m LEFT JOIN users u on u.u_id = m.user_id WHERE m.category_id = '$cat_id' and m.status = 1 order by message_id asc ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = $r;
            }
        }
        return $data;
    }

    function get_un_Approve_msg_of_user($cat_id,$sub_cat,$user_id)
    {
        global $DB;
        $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold, m.images FROM messages m LEFT JOIN users u on u.u_id = m.user_id WHERE m.category_id = '$cat_id' and m.sub_cat_id = '$sub_cat' and m.user_id = '$user_id' and m.status = 0 order by message_id asc ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = $r;
            }
        }
        else
        {
            $data[] = array(
            'success'   =>  false,
            'message'   =>  'Messages not found.'
            );
        }
        return json_encode($data);
    }

    function get_Sub_Category_messages($cat_id,$sub_cat_id, $countries = null, $time = null, $u_id = null)
    {
        global $DB;
        $sql = "";
        $countries = isset($countries) ? trim($countries) : null;
        
        // don't show messages before $time or if $time is not given, dont show 6 month older messages
        // $timeFilter = "AND str_to_date(m.created_at, '%m/%d/%Y %r') >= DATE_ADD(NOW(), INTERVAL -6 MONTH)";
        $timeFilter = "";
        // if u_id is given, dont show messages older than p_data_backup_duration
        if (isset($u_id)) {
            $sql = "SELECT p_data_backup_duration FROM users, packeages WHERE u_id=$u_id AND pkg_id=p_id";
            $res = $DB->qr($sql);
            $res = $DB->fa($res);
            $data_backup_duration = '-' . $res['p_data_backup_duration'];
            $start_date = date("m/d/Y", strtotime($data_backup_duration));
            // $timeFilter = "AND str_to_date(m.created_at, '%m/%d/%Y %r') >= str_to_date('$start_date', '%m/%d/%Y')";    
        }
        
        
        $uIdFilter = isset($u_id) ? "u.u_id=$u_id or " : "";
        
        if (isset($countries) && !empty($countries)) 
        {
            $countries = array_map(function($a){return "'".trim($a)."'";}, explode(',', $countries));
        }
        if (isset($countries) && !empty($countries) && count($countries) > 0) 
        {
            $countries = implode(',', $countries);
            $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.images, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold FROM messages m LEFT JOIN users u on u.u_id = m.user_id WHERE m.category_id = '$cat_id' and m.sub_cat_id='$sub_cat_id' and m.status = 1 and (m.user_id=1 or ($uIdFilter u.u_country in (".$countries."))) $timeFilter AND m.expired = 0 order by message_id asc";
        } 
        else {
            $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.images, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold FROM messages m LEFT JOIN users u on u.u_id = m.user_id WHERE m.category_id = '$cat_id' and m.sub_cat_id='$sub_cat_id' and m.status = 1 $timeFilter AND m.expired = 0 order by message_id asc ";
        }
        $res = $DB->qr($sql);
        $data = [];
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $r['u_id'] = $r['u_id'] == null ? '1' : $r['u_id'];
                $r['u_name'] = $r['u_name'] == null ? 'Admin' : $r['u_name'];
                $data[] = $r;
            }
        }
        return json_encode($data);
        
        // if (isset($countries) && !empty($countries)) 
        // {
        //     $countries = array_map(function($a){return "'".trim($a)."'";}, explode(',', $countries));
        // }
        // if (isset($countries) && !empty($countries) && count($countries) > 0) 
        // {
        //     $countries = implode(',', $countries);
        //     $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.images, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold FROM messages m, users u 
        //             WHERE u.u_id = m.user_id AND m.category_id = '$cat_id' AND m.sub_cat_id = '$sub_cat_id' and m.status = 1 and ($uIdFilter u.u_country in (".$countries."))
        //             UNION
        //             SELECT 1 as u_id, 'Admin' as u_name, null as u_image, message_id, message, images, status, created_at, admin_msg, is_msg_on_hold FROM messages 
        //             WHERE user_id=1 AND category_id = '$cat_id' AND sub_cat_id = '$sub_cat_id'
        //             $timeFilter order by message_id asc ";
        // } 
        // else {
        //     $sql = "SELECT u.u_id, u.u_name, u.u_image,m.message_id, m.message, m.images, m.status, m.created_at, m.admin_msg, m.is_msg_on_hold FROM messages m, users u 
        //             WHERE u.u_id = m.user_id AND m.category_id = '$cat_id' AND m.sub_cat_id = '$sub_cat_id' and m.status = 1 
        //             UNION
        //             SELECT 1 as u_id, 'Admin' as u_name, null as u_image, message_id, message, images, status, created_at, admin_msg, is_msg_on_hold FROM messages 
        //             WHERE user_id=1 AND category_id = '$cat_id' AND sub_cat_id = '$sub_cat_id'
        //             $timeFilter  order by message_id asc ";
        // }
        
        // $res = $DB->qr($sql);
        // if($DB->nr($res)>0)
        // {
        //     while($r = $DB->fa($res))
        //     {
        //         $data[] = $r;
        //     }
        // }
        // else
        // {
        //     $data = array(
        //         'success'   =>  false,
        //         'message'   =>  'Sub Category messages not found'
        //     );
        // }
        // return json_encode($data);
    }

    function get_single_chat($from_id,$to_id, $time = null)
    {
        global $DB;
        $timeFilter = isset($time) && !empty(trim($time)) ? "AND str_to_date(m.created_at, '%m/%d/%Y %r') >= str_to_date('".trim($time)."', '%m/%d/%Y %r')" : "";
        //$sql = "SELECT * FROM single_chat where from_user_id = '$from_id' or to_user_id = '$to_id' and disable_status = 1";
        $sql = "SELECT * FROM single_chat where (from_user_id = '$from_id' and to_user_id = '$to_id') or (from_user_id = '$to_id' and to_user_id = '$from_id') and disable_status = 1 $timeFilter ORDER BY sc_id desc ";
        $res = $DB->qr($sql);
        $data = [];
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = $r;
            }
        }
        return json_encode($data);
    }

    function add_single_chat($from_id, $to_id,$messaage, $files)
    {
        $date = date('m/d/Y h:i:s a', time());
        global $DB, $users, $push;
        $result_from_update = $this->update_user_remain_user_msg($from_id);
        $data = null;
        if($result_from_update == 1)
        {
            if (isset($files) && count($files) > 0) {
                if (count($files) > 10) {
                    return json_encode(['success'=>false,'message'=> "You cannot upload more than 10 images at a time"]);
                }
                $are_files_valid = $this->are_files_valid($files);
                $error_messages = array(
                    2 => "Only jpg and png files are allowed",
                    3 => "Files size must be less than 10MB",
                    4 => "Could not upload one or more images"
                );
                if ($are_files_valid != 1) {
                    $data = ['success'=>false,'message'=>$error_messages[$are_files_valid]];
                    return json_encode($data);
                } 
                else {
                    $files_prefix = $from_id . "_" . $to_id;
                    $filesnames = implode(",", $this->upload_files($files, $files_prefix));
                    $sql = "INSERT INTO single_chat(from_user_id, to_user_id, message, images, msg_created_at) VALUES ('$from_id' , '$to_id' , '$messaage', '$filesnames' , '$date')";
                }
            } 
            else {
                $sql = "INSERT INTO single_chat(from_user_id, to_user_id, message, msg_created_at) VALUES ('$from_id' , '$to_id' , '$messaage' , '$date')";
            }
            $last_msg_id = $DB->get_last_id($sql);
            if(isset($last_msg_id))
            {
                $msg_sql = "SELECT from_user_id, to_user_id, message, msg_created_at, images FROM single_chat WHERE sc_id = '$last_msg_id' order by sc_id asc ";
                $res = $DB->qr($msg_sql);
                $result = $DB->fa($res);

                $from_user_token = $users->get_one_user_tokens($from_id);
                $to_user_token = $users->get_one_user_tokens($to_id);
                
                $tokens = array(
                    $from_user_token,
                    $to_user_token
                );
                if ($from_user_token[1]) { // if from_user is ios, don't send him notif
                    $tokens = array($to_user_token);
                }
                $to_u_info = $users->get_user_by_id($from_id);
                $pushres = $push->for_single_chating($tokens,$result['from_user_id'],$result['to_user_id'],$to_u_info['u_name'],$result['message'],$result['msg_created_at'], $result['images'], $last_msg_id);
                $data = ['success'=>true, 'images'=>$result['images']];
                // send to old portal
                // if (!isset($_POST["ref"]) || !$_POST["ref"]) {
                //     $this->sendPostRequest([
                //         "from_id" => $from_id,
                //         "to_id" => $to_id,
                //         "message" => $messaage,
                //         "req_key" => "add_singal_chat",
                //         "ref" => 1
                //     ]);
                // }

                // update badges in background
                // if ($to_user_token[1]) { // if he is ios user
                    $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/chat_api/Classes/update_badges.php";
                    $cmd = "php -f $path_to_script pc $to_id $from_id 1";
                    $outputfile = "badges_logs.txt";
                    $pidfile = "badges_pids.txt";
                    exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
                // }
                
                $this->increment_msg_consumption($from_id);
            }
            else
            {
                //error
                $msg = "Failed to send message.";
                $data = ['success'=>false,'message'=>$msg];
            }
        }
        elseif ($result_from_update == 2)
        {
            $msg  = "Error while user message package update."; //"Error while user message package update";
            $data = ['success'=>false,'message'=>$msg];
        }
        elseif ($result_from_update == 3)
        {
            $msg  = "You don't have message packages";
            $data = ['success'=>false,'message'=>$msg];
        }
        else
        {
            $msg  = "Server error please try again"; //unable to user remaining mesg
            $data = ['success'=>false,'message'=>$msg];
        }
        return json_encode($data);
    }
    
    function get_badges($u_id) {
        global $DB, $users;
        $user_token = $users->get_one_user_tokens($u_id);
        // if (!$user_token[1]) {
        //     return json_encode(["success"=> false, "message"=> "This service is only for iOS users"]);
        // }
        
        $sql = "SELECT type, from_id, c_id, sub_cid, count, last_modified FROM badges
                WHERE u_id = $u_id";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = $r;
            }
        }
        return json_encode($data);   
    }
    
    function update_badges($type, $uid, $cid, $sub_cid, $from_id, $count) {
        global $DB, $users;
        $user_token = $users->get_one_user_tokens($uid);
        // if (!$user_token[1]) {
        //     return json_encode(["success"=> false, "message"=> "This service is only for iOS users"]);
        // }
        if ($cid == null && $sub_cid == null && $from_id == null && $count == null && isset($uid)) {
            $type_filter = isset($type) && in_array($type, ['pc', 'gc', 'ns']) ? "AND type='$type'" : "";
            $res = $DB->qr("UPDATE badges SET count=0 WHERE u_id = $uid $type_filter");
            if ($res) {
                return json_encode(["success"=> true, "message"=>"Badges updated"]);   
            }
            else {
                return json_encode(["success"=> false, "message"=>"Badges were not updated"]);
            }
        }
        $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/chat_api/Classes/update_badges.php";
        $cmd = null;
        if ($type == "pc") {
            $cmd = "php -f $path_to_script pc $uid $from_id $count";
        } 
        else if ($type == "gc") {
            $cmd = "php -f $path_to_script gc $uid $cid $sub_cid $count";
        } 
        else if ($type == "ns") {
            $cmd = "php -f $path_to_script ns $uid $count";
        }
        $outputfile = "badges_logs.txt";
        $pidfile = "badges_pids.txt";
        exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));  
        return json_encode(["success"=> true, "message"=>"Badges updated"]);
    }
    
    // Add by Bilal
    public function update_user_last_active($u_id)
    {
        global $DB;
        $date = date('Y-m-d H:i:s');
        $sql = "UPDATE users SET u_last_active = '$date'  WHERE u_id = '$u_id' ";
        if($DB->qr($sql))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_daily_group_msg_count($user_id)
    {
        global $DB;
        $date = date("Y-m-d H:i:s");
        // echo $date;
        // die();
        $sql = "SELECT * FROM user_group_msg_logs gm WHERE gm.user_id = '$user_id'";
        $res = $DB->qr($sql);
        // $tot_msg = $DB->fa($res);
        // print_r($tot_msg);
        // echo $res->lengths;
        if(mysqli_num_rows($res) != 0)
        {
            echo "ahmed";
            $tot_msg = $DB->fa($res);
            return $tot_msg['msg_count'];
        }
        else{
            echo "sidd";
            $sql_insert = "INSERT INTO user_group_msg_logs(user_id, msg_count, flag_date) VALUES ($user_id, 0,'$date')";
            $response = $DB->qr($sql_insert);

            return 0;
        }
        
    }
}