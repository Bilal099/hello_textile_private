<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 10/2/2018
 * Time: 12:38 PM
 */
include_once "../include/header.php";
$u_id = $_GET['id'];
if(isset($_POST['subBtn']))
{
    $data = array(
            'name'          =>  $_POST['name'],
            'email'         =>  $_POST['email'],
            'contact'       =>  $_POST['contact'],
            'company'       =>  $_POST['company'],
            'comp_address'  =>  $_POST['company_address'],
            'n_bsn'         =>  $_POST['nature_business'],
            'create_at'     =>  $_POST['created_at'],
            'expiry_date'   =>  $_POST['expiry_date'],
            'pkg_type'      =>  $_POST['package_type'],
            'total_msg'     =>  $_POST['total_msg'],
            'u_paid'        =>  $_POST['user_type'],
            'u_status'      =>  $_POST['status'],
            'u_city'        => $_POST['city'],
            'u_country'     => $_POST['country'],
            'id'            =>  $u_id
    );
    echo $user->update_user_filed($data);
}
if(isset($_POST['deleteBtn']))
{
    echo $user->delete_user($u_id);
}
$cat_arr  = $user->get_user_by_id($u_id);
?>
<!-- Multi Column -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Edit User
                </h2>
            </div>
            <div class="body">
                <form id="form_validation" method="POST">
                <div class="row clearfix">
                    <div class="col-md-6">
                    </div>
                    <?php if (!empty(trim($cat_arr['u_image']))) : ?>
                    <div class="col-xs-6 col-md-3">
                        <a href="javascript:void(0);" class="thumbnail">
                            <img width="500px" height="300px" src="<?php echo $cat_arr['u_image'];?>" class="img-responsive">
                            <a class="btn btn-danger img-decline-btn" data-user-id="<?php echo $u_id;?>"><i class="material-icons">close</i> <span style="margin:0px;">Decline Photo</span></a>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" value="<? echo $cat_arr['name']?>" required>
                                <label class="form-label">Name</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="email" class="form-control" name="email" value="<? echo $cat_arr['email']?>" required>
                                <label class="form-label">Email</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="contact" value="<? echo $cat_arr['contact']?>" required>
                                <label class="form-label">Contact</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="company" value="<? echo $cat_arr['company']?>" required>
                                <label class="form-label">Company</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="city" value="<? echo $cat_arr['city']?>" required>
                                <label class="form-label">City</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="country" value="<? echo $cat_arr['country']?>" required>
                                <label class="form-label">Country</label>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="company_address" value="<? echo $cat_arr['company_address']?>" required>
                                    <label class="form-label">Company Address</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="nature_business" value="<? echo $cat_arr['nature_of_business']?>" required>
                                    <label class="form-label">Nature of business</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="created_at" value="<?php echo $cat_arr['u_created_at'];?>" required>
                                    <label class="form-label">Created At</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="expiry_date" value="<?php echo $cat_arr['u_expiry_date'];?>" required>
                                    <label class="form-label">Expiry Date</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="total_msg" value="<? echo $cat_arr['u_total_msg']?>" required>
                                    <label class="form-label">Balance Messages</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="package_type" value="<? echo $cat_arr['u_pkg_type']?>" required>
                                    <label class="form-label">Package Type</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <p>
                                        <b>User Type</b>
                                    </p>
                                    <select name="user_type" class="form-control show-tick">
                                        <option value="0" <? echo $cat_arr['u_is_paid']==0?'selected':''; ?>>Free</option>
                                        <option value="1" <? echo $cat_arr['u_is_paid']==1?'selected':''; ?>>Paid</option>
                                        <option value="2" <? echo $cat_arr['u_is_paid']==2?'selected':''; ?>>Expire</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <p>
                                        <b>Status</b>
                                    </p>
                                    <select name="status" class="form-control show-tick">
                                        <option value="1" <? echo $cat_arr['status']==1?'selected':''; ?>>Active</option>
                                        <option value="0" <? echo $cat_arr['status']==0?'selected':''; ?>>In-Active</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT</button>
                    <button class="btn btn-danger waves-effect" name="deleteBtn" type="submit">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Multi Column -->

<?php
include_once "../include/footer.php";
?>
<script>
$(document).on("click",".img-decline-btn",function(e){
    var user_id = $(this).data('user-id');
    var obj = $(this);
    $.ajax({
        type: "POST",
        data: {'request':'decline_user_photo_update_request','u_id':user_id},
        url: "ajaxcall.php",
        success: function(data)
        {
            var result = JSON.parse(data);
            var type = result.success ? 'success' : 'error';
            obj.parent().parent().remove();
            swal('Success', result.message, type);
        }
    });
});
</script>
<!-- #END# Basic Validation -->