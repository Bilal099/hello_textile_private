<?php
include_once "../include/header.php";
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Add New Blog</h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <form id="form_validation" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" required>
                            <label class="form-label">Title</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea name="description" cols="30" rows="5" class="form-control no-resize" required></textarea>
                            <label class="form-label">Description</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-line">
                            <label class="form-label">Blog Picture</label>
                            <input type="file" class="form-control" name="file_name" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p>
                            <b>Status</b>
                        </p>
                        <select name="status" class="form-control show-tick">
                            <option value="0">Select Option</option>
                            <option value="1">Active</option>
                            <option value="0">In-Active</option>
                        </select>

                    </div>
                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>