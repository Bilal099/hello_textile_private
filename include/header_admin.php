
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Chat App</title>
    <!-- Favicon-->
    <link rel="icon" href="../favicon.ico" type="image/x-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Sweet Alert Css -->
    <link href="../plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <!-- Sweet Alert Plugin Js -->
<!--    <script src="../plugins/sweetalert/sweetalert.min.js"></script>-->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Bootstrap Select Css -->
    <link href="../plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- Bootstrap Spinner Css -->
    <link href="../plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">
    <link href="../plugins/jquery-multiselect/jquery.dropdown.min.css" rel="stylesheet">

    <!-- Chat layout -->
<!--    <link rel="stylesheet" type="text/css" href="../css/chat-application.css">-->
<!--    <link rel="stylesheet" type="text/css" href="../scss/chat-application.scss">-->

    <style>
        .dropdown-menu.open {
            overflow: unset !important;
        }
    </style>
    <!--Push Notification For Web Start -->
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('ea28f4594432416f57d9', {
            cluster: 'us2',
            forceTLS: true
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            //alert(JSON.stringify(data['msg']));
            var d = JSON.parse(data);
            //swal("Info", "Admin You have new Notification", "info");
            // load_message(d);
            get_unread_count_msg();
            // load_unapprove_chat();
            get_unread_count_of_msg_in_lis_in_home_page();
        });
    </script>
    <!--Push Notification For Web End -->

    <script>
        var current_cat = 0;
        var current_scat = 0;
        function load_message(msg)
        {
            var message_layout = '<li class="sent">\n' +
                '                    <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" style="visibility:hidden"/>\n' +
                '                    <p>'+msg.msg+'</p>\n' +
                '                </li>';
            // alert(msg.cat_id+":"+current_cat);
            if(msg.cat_id == current_cat)
            {
                $(".messages ul").append(message_layout);
                $(".messages").scrollTop(1000);
            }
        }
        function get_unread_count_msg()
        {
            $.ajax({
                type: "POST",
                data: {'request':'get_unread_count_msg'},
                url: "ajaxcall.php",
                success: function(data)
                {
                    $("#chat_icon_noti").text(data);
                }
            });
        }
    </script>


</head>
<body class="theme-purple">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="home">Chat</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <!--                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>-->
                <!-- #END# Call Search -->
                <!--                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>-->

                <!-- Notifications -->
                <li class="dropdown">
                    <a  href="javascript:void(0);" class="dropdown-toggle" role="button">
                        <i class="material-icons">notifications</i>
                        <span id="chat_icon_noti" class="label-count"><?php echo $chat->get_unread_msg_count(); ?></span>
                    </a>
                </li>
                <!-- #END# Notifications -->

            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->

<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="../images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['admin_name'];?></div>
                <div class="email"><?php echo $_SESSION['admin_email'];?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="profile?id=<?php echo $_SESSION["admin_id"]; ?>"><i class="material-icons">person</i>Profile</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active">
                    <a href="home">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                    <a href="notification">
                        <i class="material-icons">notifications</i>
                        <span>Push Notification</span>
                    </a>
                </li>

                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">notifications</i>
                        <span>Notification</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="add_notification">Send New Notification</a>
                        </li>
                        <li>
                            <a href="notification_list">Notification List</a>
                        </li>

                    </ul>
                </li>
                <?php if($_SESSION['is_sub_admin'] == 0) : ?>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">poll</i>
                        <span>Package</span>
                        <?php global $package; echo $count_el=$package->load_num_package_request(); ?>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="create_package">Create New Package</a>
                        </li>
                        <li>
                            <a href="package_list">Package List</a>
                        </li>
                        <li>
                            <a href="package_request">Package Request</a>
                        </li>
                        <li>
                            <a href="package_orders">Package Orders</a>
                        </li>
                        <li>
                            <a href="package_assign">Assign Package to User</a>
                        </li>

                    </ul>
                </li>
                <?php endif; ?>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">chat</i>
                        <span>Chats</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="chat">Approved Chat</a>
                        </li>
                        <li>
                            <a href="send_message">Send Message</a>
                        </li>

                    </ul>
                </li>
                <?php if($_SESSION['is_sub_admin'] == 0) : ?>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">person</i>
                        <span>Users</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="user_list">User List</a>
                        </li>
                        <li>
                            <a href="user_request">User Request</a>
                        </li>
                        <li>
                            <a href="user_photo_update_request">User Photo Update Request</a>
                        </li>
                        <li>
                            <a href="user_seller_profile">User Seller Profile List</a>
                        </li>
                        <li>
                            <a href="user_buyer_profile">User Buyer Profile List</a>
                        </li>

                    </ul>
                </li>
                
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_comfy</i>
                        <span>Category</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="add_category">Add New Category</a>
                        </li>
                        <li>
                            <a href="category_list">Category List</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_comfy</i>
                        <span>Sub Category</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="add_subcategory">Add New Sub Category</a>
                        </li>
                        <li>
                            <a href="subcategory_list">Sub Category List</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">fiber_new</i>
                        <span>News</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="add_news">Add New News</a>
                        </li>
                        <li>
                            <a href="news_list">News List</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">person</i>
                        <span>Sub Admin</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="add_sub_admin">Add Sub Admin</a>
                        </li>
                        <li>
                            <a href="list_sub_admin">Sub Admin List</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">live_tv</i>
                        <span>Video Ads</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="new_video_ad.php">Add New Ad</a>
                        </li>
                        <li>
                            <a href="video_ad_list.php">Ads List</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">live_tv</i>
                        <span>Ads</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="add_ads">Add New Ads</a>
                        </li>
                        <li>
                            <a href="ad_list">Ads List</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">search</i>
                        <span>User Search Records</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="user_search_records">Record List</a>
                        </li>

                    </ul>
                </li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2019 - 2020 <a href="#">Hello Textile</a>
            </div>
            <div class="version">
                <b>Version: </b> 1.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
</section>

<section class="content">
    <div class="container-fluid">