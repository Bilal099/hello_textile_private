<?php

class sub_admin
{
    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
    function login($param)
    {
        $stmt = $this->conn->prepare("SELECT sub_admin_id, sub_admin_name, sub_admin_email, sub_admin_password FROM sub_admin WHERE sub_admin_email = ? AND sub_admin_password = ?");
        $stmt->bind_param("ss",$param['email'],$param['pass']);
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['name'],$param['email'],$param['pass']);
        $stmt->store_result();
        $stmt->fetch();
        if($stmt->num_rows == 1)
        {
            $_SESSION['sub_admin_email'] = $param['email'];
            $_SESSION["sub_admin_name"] = $param['name'];
            $_SESSION["sub_admin_id"] = $param['id'];
            $msg = header("location:pages/home");
        }
        else
        {
            $msg = '<script>swal("Oops!", "Invalid Login Detail.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    function add_sub_admin($name, $email, $password)
    {
        $stmt = $this->conn->prepare("INSERT INTO admin_info(a_sub, a_name, a_email, a_pass) VALUES (1,?,?,?)");
        $stmt->bind_param("sss", $name, $email, $password);
        if ($stmt->execute())
        {
            $msg = '<script>swal("Good Job!", "Sub admin add successfully.", "success");</script>';
        }
        else
            $msg = '<script>swal("Oops!", "Unable to add sub admin.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    //select all sub_admin
    function get_all_sub_admin()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT a_id, a_name, a_email, a_pass FROM admin_info WHERE a_sub=1 ");
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['name'],$param['email'], $param['password']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<tr>
                            <td>'.$param['id'].'</td>
                            <td>'.$param['name'].'</td>
                            <td>'.$param['email'].'</td>
                            <td>
                                <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_sub_admin?id='.$param['id'].'"><i class="material-icons">edit</i></a>
                            </td>
                        </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Sub Admin Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    //update sub_admin
    function update_sub_admin($sub_admin_id, $name, $email, $password)
    {
//        global $push, $user;
//        $tokens = $user->get_all_users_tokens();

        $stmt = $this->conn->prepare("UPDATE admin_info SET a_name = ?, a_email = ?, a_pass = ? WHERE  a_id = ?");
        $stmt->bind_param("sssi",$name,$email,$password,$sub_admin_id);
        if($stmt->execute())
        {
            $msg = '<script>swal("Good Job!", "Sub admin update successfully.", "success");</script>';
        }
        else
            $msg = '<script>swal("Oops!", "Unable to update sub admin.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    //delete sub_admin
    function delete_sub_admin($sub_admin_id)
    {
        $stmt = $this->conn->prepare("DELETE FROM admin_info WHERE a_id = ?");
        $stmt->bind_param("i",$sub_admin_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Sub Admin Delete successfully.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to Delete Sub Admin.", "error");</script>';
        $stmt->close();
        return $msg;
    }


    //get news sub_admin
    function get_sub_admin_by_id($sub_admin_id)
    {
        $stmt = $this->conn->prepare("SELECT a_name, a_email, a_pass FROM admin_info WHERE a_id = ?");
        $stmt->bind_param("s",$sub_admin_id);
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['name'],$cat_arr['email'],$cat_arr['password']);
        $stmt->store_result();
        $stmt->fetch();
        return $cat_arr;
        $stmt->close();
    }
}

