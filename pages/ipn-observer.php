<?php

$url = $_GET['url'];

// directly using url is not reliable and could give unwanted result.
// instead, we extract the order id from url and use the inquire transaction api

$parsed = parse_url($url);

if (!$parsed || ($parsed['host'] != "easypay.easypaisa.com.pk" && $parsed['host'] != "easypay.easypaisa.com.pk")) {
    file_put_contents("ipn-observer-logs", "Invalid url:\n$url\n", FILE_APPEND);
    exit;
}

$path_c = explode("/", $parsed["path"]); // path looks like /easypay-service/rest/v1/order-status/20074/HT-1-2-3
$orderRefNum = $path_c[6]; // HT-1-2-3
$order_id = explode("-", $orderRefNum);

if (count($path_c) != 7 || count($order_id) != 4) {
    file_put_contents("ipn-observer-logs", "1.Invalid order:\n$orderRefNum\n", FILE_APPEND);
    file_put_contents("ipn-observer-logs", json_encode($path_c), FILE_APPEND);
    exit;
}

// $order_id[0] is 'HT'
$p_id = $order_id[1];
$u_id = $order_id[2];
$order_id = $order_id[3];

if (!is_numeric($p_id) || !is_numeric($u_id) || !is_numeric($order_id)) {
    file_put_contents("ipn-observer-logs", "2.Invalid order:\n$orderRefNum\n", FILE_APPEND);
    exit;
}

$fields = [
	"storeId"    => "60479",
	"orderId"    => $orderRefNum,
	"accountNum" => "113807981"
];

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "https://easypay.easypaisa.com.pk/easypay-service/rest/v4/inquire-transaction",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => 1,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => json_encode($fields),
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "Credentials: SGVsbG9UZXh0aWxlOlBheW1lbnRBY2M3ODZA" // base64encode("username:password")
  ),
));

$response = curl_exec($curl);

curl_close($curl);

$response = json_decode($response, true);

if ($response["responseCode"] != "0000") {
    file_put_contents("ipn-observer-logs", "Transaction failed\n" . json_encode($response) . "\n\n", FILE_APPEND);
    exit;
}

// {
//     "responseCode": "0003",
//     "responseDesc": "INVALID ORDER ID"
// }

// {
//     "orderId": "123632",
//     "accountNum": "20074",
//     "storeId": 10154,
//     "storeName": "HELLO TEXTILE",
//     "transactionStatus": "FAILED",
//     "transactionAmount": 89.0,
//     "transactionDateTime": "30/04/2020 12:41 AM",
//     "msisdn": "03458501072",
//     "paymentMode": "MA",
//     "responseCode": "0000",
//     "responseDesc": "SUCCESS"
// }

include "/home/hellzpgk/public_html/chat_api/Classes/package.php";
include "/home/hellzpgk/public_html/chat_api/include/mysql.php";
include "/home/hellzpgk/public_html/chat_api/Classes/users.php";
include "/home/hellzpgk/public_html/chat_api/Classes/PushNotification.php";

$users = new users();
$push = new PushNotification();
$DB = new mysql_functions();

$packages_api = new package();
file_put_contents("ipn-observer-logs", "Transaction:\n" . json_encode($response) . "\n\n", FILE_APPEND);
if ($response["transactionStatus"] == "PAID") {
    if ($packages_api->update_order($order_id, $response)) {
        file_put_contents("ipn-observer-logs", "Order updated:" . $order_id . "\n\n", FILE_APPEND);
        $packages_api->assign_package($order_id, $u_id, $p_id);
    } else {
        file_put_contents("ipn-observer-logs", "Transaction paid but order not updated:" . $order_id . "\n\n", FILE_APPEND);
    }
} else {
    file_put_contents("ipn-observer-logs", "Transaction not paid:\n" . json_encode($response) . "\n\n", FILE_APPEND);
}