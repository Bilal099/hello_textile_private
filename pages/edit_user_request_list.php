<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 10/2/2018
 * Time: 12:38 PM
 */
include_once "../include/header.php";
$u_id = $_GET['id'];
if(isset($_POST['subBtn']))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $contact = $_POST['contact'];
    $city = $_POST['city'];
    $company = $_POST['company'];
    $companyaddr = $_POST['company_address'];
    $natureofbusiness = $_POST['nature_business'];
    $status = $_POST['status'];
    if(isset($u_id))
    {
        echo $user->update_user_filed($name,$email,$contact,$city,$company,$companyaddr,$natureofbusiness,$status,$u_id);
    }
    
}
if(isset($_POST['deleteBtn']))
{
    echo $user->delete_user($u_id);
}
$cat_arr  = $user->get_request_user_by_id($u_id);
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Approve User</h2>
            </div>
            <div class="body">
                <form id="form_validation" enctype="multipart/form-data" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="<? echo $cat_arr['name']?>" required>
                            <label class="form-label">Name</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" value="<? echo $cat_arr['email']?>" required>
                            <label class="form-label">Email</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="contact" value="<? echo $cat_arr['contact']?>" required>
                            <label class="form-label">Contact</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="city" value="<? echo $cat_arr['city']?>" required>
                            <label class="form-label">City</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="company" value="<? echo $cat_arr['company']?>" required>
                            <label class="form-label">Company</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="company_address" value="<? echo $cat_arr['company_address']?>" required>
                            <label class="form-label">Company Address</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="nature_business" value="<? echo $cat_arr['nature_of_business']?>" required>
                            <label class="form-label">Nature of business</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p>
                            <b>Status</b>
                        </p>
                        <select name="status" class="form-control show-tick">
                            <!--<option selected value="--><?// echo $cat_arr['status'] ?><!--">--><?// echo $cat_arr['status'] == 1 ? "Active" : "In-Active" ?><!--</option>-->
                            <option value="1" <? echo $cat_arr['status']==1?'selected':''; ?>>Active</option>
                            <option value="0" <? echo $cat_arr['status']==0?'selected':''; ?>>In-Active</option>
                        </select>
                    </div>

                    <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT<i class="material-icons">send</i></button>
                    <button class="btn btn-danger waves-effect" name="deleteBtn" type="submit"><i class="material-icons">delete</i>Delete</button>

                </form>
            </div>
        </div>

        <?php
        include_once "../include/footer.php";
        ?>
        <!-- #END# Basic Validation -->