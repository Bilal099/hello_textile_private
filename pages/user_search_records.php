<?php
// echo "bilal";
include_once "../include/header.php";

?>

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    User Search Record List
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Contact</th>
                                <th>Company</th>
                                <th>No of Searches</th>
                                <th>Buyer Products</th>
                                <th>Seller Products</th>
                                <th>Buyer Business Nature</th>
                                <th>Seller Business Nature</th>
                                <th>Buyer Description</th>
                                <th>Seller Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Contact</th>
                                <th>Company</th>
                                <th>No of Searches</th>
                                <th>Buyer Products</th>
                                <th>Seller Products</th>
                                <th>Buyer Business Nature</th>
                                <th>Seller Business Nature</th>
                                <th>Buyer Description</th>
                                <th>Seller Description</th>
                                <th>Action</th>

                            </tr>
                        </tfoot>
                        <tbody>
                            <?= $user->user_search_record_list() ?>
                           
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
<!-- <div id="message"></div> -->

<div id="view_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-2"><label for="">Name</label></div>
                    <div class="col-md-10">
                        <p id="name"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Country</label></div>
                    <div class="col-md-10">
                        <p id="country"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Contact</label></div>
                    <div class="col-md-10">
                        <p id="contact"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Company</label></div>
                    <div class="col-md-10">
                        <p id="company"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">No of Searches</label></div>
                    <div class="col-md-10">
                        <p id="no_of_searches"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Buyer Products</label></div>
                    <div class="col-md-10">
                        <p id="buyer_products"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Seller Products</label></div>
                    <div class="col-md-10">
                        <p id="seller_products"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Buyer Business Nature</label></div>
                    <div class="col-md-10">
                        <p id="buyer_business_nature"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Seller Business Nature</label></div>
                    <div class="col-md-10">
                        <p id="seller_business_nature"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2"><label for="">Buyer Description</label></div>
                    <div class="col-md-10">
                        <p id="buyer_description"></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"><label for="">Seller Description</label></div>
                    <div class="col-md-10">
                        <p id="seller_description"></p>
                    </div>
                </div>
                

            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php"
?>

<script>
    // $(".pkg_ApproveBtn").click(function () {
    //     var obj = $(this);
    //     approve_package(obj);
    // });

    $(".pkg_deletBtn").click(function () {
        var id = $(".pkg_deletBtn").val();
        var obj = $(this);
        //alert(id);
        $.ajax({
            type: "POST",
            async: false,
            data: {
                request: 'delete_pkg_request',
                pr_id: id
            },
            url: "ajaxcall.php",
            success: function (data) {
                obj.parent().parent().remove();
                $("#message").html(data);
            }
        });
    });

    $(".btn_view_record").click(function (e) {
        e.preventDefault();
        $("#name").                     text($(this).closest('tr').find('.u_name').text());
        $("#country").                  text($(this).closest('tr').find('.u_country').text())
        $("#contact").                  text($(this).closest('tr').find('.u_contact').text())
        $("#company").                  text($(this).closest('tr').find('.u_company').text())
        $("#no_of_searches").           text($(this).closest('tr').find('.count').text())
        $("#buyer_products").           text($(this).closest('tr').find('.u_buyer_products').text())
        $("#seller_products").          text($(this).closest('tr').find('.u_seller_products').text())
        $("#buyer_business_nature").    text($(this).closest('tr').find('.u_buyer_business_nat').text())
        $("#seller_business_nature").   text($(this).closest('tr').find('.u_seller_business_nat').text())
        $("#buyer_description").        text($(this).closest('tr').find('.u_buyer_desc').data('buyer'))
        $("#seller_description").       text($(this).closest('tr').find('.u_seller_desc').data('seller'))

        $("#view_modal").modal("show");
    });

    // function approve_package(obj)
    // {
    //     var id = $(".pkg_ApproveBtn").val();
    //     var pkg_id = $(".pkg_ApproveBtn").attr('data-pkgID');
    //     var user_id = $(".pkg_ApproveBtn").attr('data-userID');
    //     //alert('Request ID: '+id+' Package ID: '+pkg_id+' user iD: '+user_id);

    //     $.ajax({
    //         type: "POST",
    //         async:false,
    //         data: { request:'assign_user_a_package', pid:pkg_id , prID:id , u_id:user_id},
    //         url: "ajaxcall.php",
    //         success: function(data)
    //         {
    //             obj.parent().parent().remove();
    //             $("#message").html(data);
    //         }
    //     });
    // }
</script>