<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 10/2/2018
 * Time: 12:38 PM
 */
include_once "../include/header.php";
$c_id = $_GET['id'];
if(isset($_POST['subBtn']))
{
    $name = $_POST['name'];
    $sub_cat = $_POST['sub_Cat'];
    $status = $_POST['status'];
	
    echo $category_obj->update_category_filed($c_id,$name,$sub_cat,$status);
    
    
}
if(isset($_POST['deleteBtn']))
{
    echo $category_obj->delete_category($c_id);
}
$cat_arr  = $category_obj->get_category_by_id($c_id);
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit Category</h2>
                <!-- <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul> -->
            </div>
            <div class="body">
                <form id="form_validation" enctype="multipart/form-data" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" value="<? echo $cat_arr['name']?>" required>
                            <label class="form-label">Title</label>
                        </div>
                    </div>

                        <div class="col-md-6">
                            <p>
                                <b>Have Sub Category</b>
                            </p>
                            <select name="sub_Cat" class="form-control show-tick">
<!--                                <option selected value="--><?// echo $cat_arr['sub_cat'] ?><!--">--><?// echo $cat_arr['sub_cat'] == 1 ? "Yes" : "No" ?><!--</option>-->
                                <option value="1" <? echo $cat_arr['sub_cat']==1?'selected':''; ?>>Yes</option>
                                <option value="0" <? echo $cat_arr['sub_cat']==0?'selected':''; ?>>No</option>
                            </select>

                        </div>
                        <div class="col-md-6">
                            <p>
                                <b>Status</b>
                            </p>
                            <select name="status" class="form-control show-tick">
<!--                                <option selected value="--><?// echo $cat_arr['status'] ?><!--">--><?// echo $cat_arr['status'] == 1 ? "Active" : "In-Active" ?><!--</option>-->
                                <option value="1" <? echo $cat_arr['status']==1?'selected':''; ?>>Active</option>
                                <option value="0" <? echo $cat_arr['status']==0?'selected':''; ?>>In-Active</option>
                            </select>
                        </div>
                    <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT<i class="material-icons">send</i></button>
                    <button class="btn btn-danger waves-effect" name="deleteBtn" type="submit"><i class="material-icons">delete</i>Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>
<!-- #END# Basic Validation -->