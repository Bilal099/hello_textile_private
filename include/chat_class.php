<?php
class chat_class
{
    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    //admin chat in categories
    function add_admin_msg_in_cat($param)
    {
        global $push, $user;
        $one = 1;
        // $tokens = $user->get_all_users_tokens();
        // $tokens = [["d4DZUJEP9K0:APA91bGlcJFHZN--IS8VkqbNYgHpgpRCbjC_XGxSVPrazzF8oTqLEk33KA1D-i7xzPVll0mSc9aSk14w1OmHi2W84Em_m6jDz4ACRPedCXyoOeWHGzTz5C65ADcqU-aYE95SXWLrGtb8", 0]];

        $date = date('m/d/Y h:i:s a', time());
        $stmt = $this->conn->prepare("INSERT INTO messages(category_id, sub_cat_id, user_id, message, created_at, admin_msg, status) VALUES (?,?,?,?,?,?,1)");
        $stmt->bind_param("ssssss",$param['category_id'], $param['sub_cat_id'], $param['user_id'], $param['message'], $date, $one);
        if ($stmt->execute())
        {
            $msg = '<script>swal("Good Job!", "Message sent successfully.", "success");</script>';
            $last_id = $stmt->insert_id;
            // send notification to users
            $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/include/notif_process.php";
            $cmd = "php -f $path_to_script $last_id 1";
            $outputfile = "notif_process_logs.txt";
            $pidfile = "notif_process_pids.txt";
            exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
            
            // update badges for ios users
            
            $sub_cid = $param['sub_cat_id'];
            $cid = $param['category_id'];
            $sub_cid = isset($sub_cid) && $sub_cid > 0 ? $sub_cid : -1;
            
            $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/chat_api/Classes/update_badges.php";
            $cmd = "php -f $path_to_script gc -1 $cid $sub_cid 1";
            $outputfile = "badges_logs.txt";
            $pidfile = "badges_pids.txt";
            exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
        

        }
        else
            $msg = '<script>swal("Oops!", "Unable to sent Message.", "error");</script>';
        $stmt->close();
        return $msg;
    }
    function sendPostRequest($fields) {

        // Set POST variables
        $url = 'https://hellotextiles.com/chat_api/index.php';

        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($fields)),
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Accept-Encoding: gzip, deflate, br",
            "Accept: */*",
            "Host: hellotextiles.com"
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }


    //Delete Message
    function deleteMessage($id)
    {
        // return json_encode($this->sendPostRequest([
        //             "cat_id" => $id,
        //             "req_key" => "add_messge"
        //         ]));
        global $push, $user;
        $message    = $this->get_message_details($id);
       // echo $message["user_id"];exit;
        $token = $user->get_user_token_by_id($message["user_id"]);
        $stmt = $this->conn->prepare("DELETE FROM messages WHERE message_id = ?");
        $stmt->bind_param("i",$id);
        if($stmt->execute())
        {
            // echo $token;
            // exit;
            
            $msg = "Message Delete successfuly.";
            $push->deleteFromAdmin($token,"Info","Your message has been deleted By Admin","");
        }
        else
            $msg = "Unable to Delete message.";
        $stmt->close();
        return $msg;
    }

    function new_msg_for_approve($should_load_all)
    {
        $is_loaded_filter = $should_load_all == 1 ? '': 'and is_loaded=0';
        $stmt = $this->conn->prepare("SELECT m.message_id, u.u_name, u.u_country, u.u_contact, u.u_pkg_type, m.message, c.c_id, c.c_name, sc.sc_id , sc.sc_name, m.images 
                                    FROM messages m LEFT JOIN users u ON u.u_id = m.user_id 
                                    LEFT JOIN category c on c.c_id = m.category_id 
                                    LEFT JOIN sub_category sc on sc.sc_id = m.sub_cat_id 
                                    WHERE m.admin_msg = 0 and m.status = 0 and m.is_msg_on_hold = 0 $is_loaded_filter
                                    ORDER BY m.created_at ASC LIMIT 1");
        $stmt->execute();
        $data = array();
        $result = $stmt->bind_result($data['message_id'], $data['u_name'], $data['u_country'], $data['u_contact'], $data['u_pkg_type'], $data['message'], $data['c_id'], $data['c_name'] , $data['sc_id'], $data['sc_name'], $data['images']);
        $stmt->store_result();
        $stmt->fetch();
        $updateload = $this->conn->prepare("UPDATE messages SET is_loaded=1 WHERE message_id=".$data['message_id']);
        if ($updateload) {
            $updateload->execute();
            $updateload->close();
        }
        $stmt->close();
        
        return json_encode($data);
    }
    // set is_loaded to 0 if admin closes the message approve dialog without any further action
    function set_msg_is_loaded($msg_id){
        $stmt = $this->conn->prepare("UPDATE messages SET is_loaded=0 WHERE message_id=$msg_id");
        $data = ["success"=>false];
        if ($stmt && $stmt->execute()) {
            $data["success"] = true;
        }
        $stmt->close();
        return json_encode($data);
    }
    function hold_message()
    {
        $stmt = $this->conn->prepare("SELECT m.message_id, u.u_name, u.u_country, u.u_contact, u.u_pkg_type, m.message, c.c_id, c.c_name, sc.sc_id , sc.sc_name, m.images FROM messages m LEFT JOIN users u ON u.u_id = m.user_id LEFT JOIN category c on c.c_id = m.category_id LEFT JOIN sub_category sc on sc.sc_id = m.sub_cat_id WHERE m.admin_msg = 0 and m.status = 0 and m.is_msg_on_hold = 1 ORDER BY m.created_at ASC LIMIT 1");
        $stmt->execute();
        $data = array();
        $stmt->bind_result($data['message_id'], $data['u_name'], $data['u_country'], $data['u_contact'], $data['u_pkg_type'], $data['message'], $data['c_id'], $data['c_name'] , $data['sc_id'], $data['sc_name'], $data['images']);
        $stmt->store_result();
        $stmt->fetch();
        return json_encode($data);
        $stmt->close();
    }

    function msgToHold($msgID)
    {
        $stmt = $this->conn->prepare("UPDATE messages SET is_msg_on_hold = 1 WHERE message_id = ?");
        $stmt->bind_param("i",$msgID);
        if($stmt->execute())
            $msg = 'Message is on hold now.';
        else
            $msg = 'Unable to hold message.';
        $stmt->close();
        return $msg;
    }

    //Get All Un Approved Chat
    function select_un_approve_chat()
    {
        $msg='';
        $stmt = $this->conn->prepare("SELECT u.u_name, u.u_image, m.message_id , m.message, m.created_at, c.c_name, sc.sc_name FROM users u LEFT JOIN messages m on m.user_id = u.u_id LEFT JOIN category c on c.c_id = m.category_id LEFT JOIN sub_category sc on sc.sc_id = m.sub_cat_id WHERE m.status = 0 ORDER BY message_id desc ");
        $stmt->execute();
        $msg_arr = array();
        $stmt->bind_result($msg_arr['u_name'], $msg_arr['u_image'], $msg_arr['message_id'], $msg_arr['message'], $msg_arr['created_at'], $msg_arr['c_name'], $msg_arr['sc_name']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '
                    <div class="media">
                        <div class="media-left">
                            <a href="javascript:void(0);">
                                <img class="media-object" src="'.$msg_arr['u_image'].'" width="64" height="64">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">
                                '.$msg_arr['u_name'].' -> '.$msg_arr['c_name'].' -> '.$msg_arr['sc_name'].'
                                 <i data-id="'.$msg_arr['message_id'].'" class="material-icons edt_button" role="button">edit</i>
                            </h4>
                            <p class="user_message">'.$msg_arr['message'].'<p>
                            <p>
                               '.$msg_arr['created_at'].'
                            </p>
                        </div>
                    </div>';
            }
        }
        else
        {
            $msg = '<script>swal("Info!", "You Dont have un-approve messages.", "info");</script>';
        }
        return $msg;
        $stmt->close();
    }

    //chat approval
    function approve_chat($chat_id, $c_id, $sc_id, $chat, $approved_by)
    {
        $chat_id = intval($chat_id);
        $c_id = intval($c_id);
        $sc_id = intval($sc_id);
        $chat = addslashes($chat);
        $approved_by = isset($approved_by) ? $approved_by : "";
        $stmt = $this->conn->prepare("UPDATE messages 
                                      SET message='$chat', category_id=$c_id, sub_cat_id=$sc_id, status=1, is_msg_on_hold=0, approved_by='$approved_by' 
                                      WHERE message_id=$chat_id");
        $msg = null;
        if($stmt && $stmt->execute())
        {
            // send notification to users
            $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/include/notif_process.php";
            $cmd = "php -f $path_to_script $chat_id 0";
            $outputfile = "notif_process_logs.txt";
            $pidfile = "notif_process_pids.txt";
            exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
            // $out = array();
            // exec($cmd, $out);

            // update badges
            $sub_cid = isset($sc_id) && $sc_id > 0 ? $sc_id : -1;
            $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/chat_api/Classes/update_badges.php";
            $cmd = "php -f $path_to_script gc -1 $c_id $sub_cid 1 $chat_id";
            $outputfile = "badges_logs.txt";
            $pidfile = "badges_pids.txt";
            exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));

            $msg = "Message Approved!";
            $stmt->close();
        }
        else
        {
            $msg = 'Error to update chat!';
        }
        return $msg;
    }

    //get message details
    function get_message_details($chat_id)
    {
        $message    = '';
        $db         = new DbConnect();
        $this->conn = $db->connect();
//        $query      = "SELECT m.user_id,m.category_id,m.sub_cat_id FROM messages m WHERE m.message_id =$chat_id";
        $query      = "SELECT m.message_id,m.user_id,m.category_id,m.sub_cat_id,m.message,m.created_at,c.c_name,sc.sc_name,u.u_name 
                        FROM messages m, users u, category c, sub_category sc 
                        WHERE m.message_id = '".$chat_id."' 
                        AND m.user_id=u.u_id 
                        AND m.category_id=c.c_id
                        AND m.sub_cat_id=sc.sc_id";
        $result     = mysqli_query($this->conn,$query);
        if($result->num_rows > 0)
        {
            while($r = $result->fetch_assoc())
            {
                $message = $r;
            }
        }
        return $message;
    }


    //Get Count of Unread Messages (Not Approve Yet By Admin)
    function get_unread_msg_count()
    {
        $stmt = $this->conn->prepare("SELECT m.message_id FROM messages m WHERE m.status = 0 and m.is_msg_on_hold = 0");
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;
        return $count;
    }

    //Get Count of Hold Messages (the message which was hold by admin)
    function get_count_hold_msg()
    {
        $stmt = $this->conn->prepare("SELECT m.message_id FROM messages m WHERE m.status = 0 and m.is_msg_on_hold = 1");
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;
        return $count;
    }

    ///************************************ Chat Delete on Day By Day  *************************************************///

    function deleteMessageForTimeDiffrence($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM messages WHERE message_id = ?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
    }

    function load_chat_for_cheaking()
    {
        global $DB;
        $sql = "SELECT message_id, user_id, created_at FROM messages WHERE status = 1 AND admin_msg = 0 AND is_msg_on_hold = 0";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = array(
                    'message_id'  =>  $r['message_id'],
                    'user_id'     =>  $r['user_id'],
                    'created_at'  =>  $r['created_at']
                );
            }
        }
        return $data;
    }

    function delete_chat_on_time_base_automatic()
    {
        global $user, $package;
        $chat_detail = $this->load_chat_for_cheaking();
        foreach ($chat_detail as $chat)
        {
            $user_deatil = $user->get_user_by_id($chat['user_id']);
            if (!empty($user_deatil['pkg_id']) && $user_deatil['pkg_id'] != "0")
            {
                $package_detail =  $package->get_single_pakage($user_deatil['pkg_id']);
                $backup_time = explode(" ",$package_detail['p_data_backup_duration']);
                $msg_duration = $backup_time[0];

                $chat_date_break = explode(" ",$chat['created_at']) ;
                $chat_date = strtotime($chat_date_break[0]);
                $today = strtotime(date('m/d/Y'));
                $total_mesg_days = round(($today - $chat_date) / (60 * 60 * 24));

                if($total_mesg_days > $msg_duration)
                {
                    $this->deleteMessageForTimeDiffrence($chat['message_id']);
                }
            }
        }
    }


}