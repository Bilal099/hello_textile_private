<?php
session_start();

$was_advertiser = false;
if (isset($_SESSION['adv_email'])) {
    $was_advertiser = true;
}

session_destroy();
if ($was_advertiser) {
    header('location:../advertise.php');
} else {
    header('location:../index.php');
}
