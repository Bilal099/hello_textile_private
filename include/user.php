<?php
class user
{
    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    function login($param)
    {
        $stmt = $this->conn->prepare("SELECT a_id, a_name, a_email, a_pass, a_sub FROM admin_info WHERE a_email = ? AND a_pass = ?");
        $stmt->bind_param("ss",$param['email'],$param['pass']);
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['name'],$param['email'],$param['pass'], $param['is_sub_admin']);
        $stmt->store_result();
        $stmt->fetch();
        if($stmt->num_rows == 1)
        {
            $_SESSION['admin_email'] = $param['email'];
            $_SESSION["admin_name"] = $param['name'];
            $_SESSION["admin_id"] = $param['id'];
            $_SESSION["is_sub_admin"] = $param['is_sub_admin']; 
            if (isset($_GET['redir'])) {
                $msg = header("location:" . $_GET['redir']);
            } 
            else {
                $msg = header("location:pages/home");
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Invalid Login Detail.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }
    
    function ad_login($param)
    {
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email FROM users WHERE u_email = ? AND u_pass = ?");
        $stmt->bind_param("ss",$param['email'],$param['pass']);
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['name'],$param['email']);
        $stmt->store_result();
        $stmt->fetch();
        if($stmt->num_rows == 1)
        {
            $_SESSION['adv_email'] = $param['email'];
            $_SESSION["adv_name"] = $param['name'];
            $_SESSION["adv_id"] = $param['id'];
            $msg = header("location:pages/new_ad");
        }
        else
        {
            $msg = '<script>swal("Invalid Credentials!", "Use a correct email and password\n Or \n Make sure you have an account on HelloTextiles App", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    function update_admin_profile($param)
    {
        $stmt = $this->conn->prepare("UPDATE admin_info SET a_name = ? , a_email = ? , a_pass = ? WHERE a_id = ?");
        $stmt->bind_param("ssss",$param['name'],$param['email'],$param['pass'],$param['id']);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Profile Update successfuly.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Invalid Login Detail.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function get_admin_detail($a_id)
    {
        $stmt = $this->conn->prepare("SELECT a_name, a_email, a_pass FROM admin_info WHERE a_id = ? ");
        $stmt->bind_param("s",$a_id);
        $stmt->execute();
        $admin_arr = array();
        $stmt->bind_result($admin_arr['name'],$admin_arr['email'],$admin_arr['pass']);
        $stmt->store_result();
        $stmt->fetch();
        return $admin_arr;
        $stmt->close();
    }

    function get_FCM_token()
    {
        $msg = "";
        $stmt = $this->conn->prepare("SELECT u_name, u_contact, u_fcm_id, ios FROM users WHERE u_status = 1");
        $stmt->execute();
        $fcm_token = array();
        $stmt->bind_result($fcm_token['name'],$fcm_token['u_contact'],$fcm_token['token'], $fcm_token['ios']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($r = $stmt->fetch())
            {
                $msg .= '<option value="'.$fcm_token['ios'].$fcm_token['token'].'">'.$fcm_token['name']." ".$fcm_token['u_contact'].'</option>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Users Not Found.", "error");</script>';
        }
        return $msg;
        $stmt->close();
    }

    function seletc_user_name_for_packages()
    {
        $msg = "";
        $stmt = $this->conn->prepare("SELECT u_name, u_contact, u_id FROM users WHERE u_status = 1");
        $stmt->execute();
        $details = array();
        $stmt->bind_result($details['name'],$details['u_contact'],$details['u_id']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($r = $stmt->fetch())
            {
                $msg .= '<option value="'.$details['u_id'].'">'.$details['name']." ".$details['u_contact'].'</option>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Users Not Found.", "error");</script>';
        }
        return $msg;
        $stmt->close();
    }

    //get requested user
     function get_all_user()
    {
        $msg = "";
        $param = array();
        // $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email, u_contact,u_city, u_country, u_company,u_comp_address,u_nature_bussniess,u_created_at,u_pkg_type, u_is_paid, u_total_msg, u_msg_consume, u_comp_address FROM users WHERE u_id != 1 Order By u_id DESC");
        $stmt = $this->conn->prepare("SELECT u.u_id, u.u_name, u.u_email, u.u_contact, u.u_city, u.u_country, u.u_company, u.u_comp_address, u.u_nature_bussniess, u.u_created_at, u.u_pkg_type, u.u_is_paid, u.u_total_msg, u.u_msg_consume, u.u_comp_address, au.ads_count FROM users u LEFT JOIN (SELECT user_id, SUM(ads_count) AS ads_count FROM user_ads_view GROUP BY user_id) au ON u.u_id = au.user_id WHERE u.u_id != 1 ORDER BY u.u_id DESC");
        $stmt->execute();
        $stmt->bind_result($param['u_id'],$param['u_name'],$param['u_email'],$param['u_contact'],$param['u_city'], $param['u_country'], $param['u_company'],$param['u_comp_address'],$param['u_nature_bussniess'],$param['u_created_at'],$param['u_pkg_type'], $param['u_is_paid'], $param['u_total_msg'], $param['u_msg_consume'], $param['u_comp_address'], $param['ads_count']);
        $stmt->store_result();
        $u_types = ['Free', 'Paid', 'Expire']; 

        // return $stmt->num_rows; 
        // $stmt->fetch();
        // return $stmt->fetch();

        // $i = 0;

        if($stmt->num_rows > 0)
        {
            for ($i=0; $i < $stmt->num_rows; $i++) 
            { 
                if($stmt->fetch())
                {
                    $msg .= '<tr>
                        <td>'.($i+1).'</td>
                        <td class="u_name">'.$param['u_name'].'</td>
                        <td class="u_email">'.$param['u_email'].'</td>
                        <td class="u_total_msg">'.$param['u_total_msg'].'</td>
                        <td class="u_msg_consume">'.$param['u_msg_consume'].'</td>
                        <td class="u_is_paid">'.$u_types[$param['u_is_paid']].'</td>
                        <td class="u_contact">'.$param['u_contact'].'</td>
                        <td class="u_country">'.$param['u_country'].'</td>
                        <td class="u_city">'.$param['u_city'].'</td>
                        <td class="u_company">'.$param['u_company'].'</td>
                        <td class="u_nature_bussniess" data-u_nature_bussniess="'.$param['u_nature_bussniess'].'">'.substr($param['u_nature_bussniess'], 0, 20). (($param['u_nature_bussniess'] != null)? "...":"" ) .'</td>
                        <td class="u_created_at">'.$param['u_created_at'].'</td>
                        <td class="u_pkg_type">'.$param['u_pkg_type'].'</td>
                        <td class="u_comp_address" data-u_comp_address="'.$param['u_comp_address'].'">'.substr($param['u_comp_address'], 0, 20). (($param['u_comp_address'] != null)? "...":"" ) .'</td>
                        <td>'.$param['ads_count'].'</td>
                        <td>
                            <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_users?id='.$param['u_id'].'"><i class="material-icons">edit</i></a>
                            <button class="btn btn-default btn-circle btn_view_record"> <span class="material-icons"> manage_accounts </span></button>
                        </td>
                    </tr>';
                }
                
            }
            // while($stmt->fetch())
            // {
            //     $i++;
            //     if($i == 7000)
            //     {
            //         break;
            //     }
            //     // $msg .= '<tr>
            //     //             <td class="u_name">'.$param['u_name'].'</td>
            //     //             <td>
            //     //                 <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_users?id='.$param['u_id'].'"><i class="material-icons">edit</i></a>
            //     //                 <button class="btn btn-default btn-circle btn_view_record"> <span class="material-icons"> manage_accounts </span></button>
            //     //             </td>
            //     //         </tr>';
                    
            //        $msg .= '<tr>
            //                 <td class="u_name">'.$param['u_name'].'</td>
            //                 <td class="u_email">'.$param['u_email'].'</td>
            //                 <td class="u_total_msg">'.$param['u_total_msg'].'</td>
            //                 <td class="u_msg_consume">'.$param['u_msg_consume'].'</td>
            //                 <td class="u_is_paid">'.$u_types[$param['u_is_paid']].'</td>
            //                 <td class="u_contact">'.$param['u_contact'].'</td>
            //                 <td class="u_country">'.$param['u_country'].'</td>
            //                 <td class="u_city">'.$param['u_city'].'</td>
            //                 <td class="u_company">'.$param['u_company'].'</td>
            //                 <td class="u_nature_bussniess" data-u_nature_bussniess="'.$param['u_nature_bussniess'].'">'.substr($param['u_nature_bussniess'], 0, 20). (($param['u_nature_bussniess'] != null)? "...":"" ) .'</td>
            //                 <td class="u_created_at">'.$param['u_created_at'].'</td>
            //                 <td class="u_pkg_type">'.$param['u_pkg_type'].'</td>
            //                 <td class="u_comp_address" data-u_comp_address="'.$param['u_comp_address'].'">'.substr($param['u_comp_address'], 0, 20). (($param['u_comp_address'] != null)? "...":"" ) .'</td>
            //                 <td>
            //                     <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_users?id='.$param['u_id'].'"><i class="material-icons">edit</i></a>
            //                     <button class="btn btn-default btn-circle btn_view_record"> <span class="material-icons"> manage_accounts </span></button>
            //                 </td>
            //             </tr>';
            // }
        }
        else
        {
            $msg = '<script>swal("Oops!", "User Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

// update user

    function update_user_filed($data)
    {
        global $push;
        $token = $this->get_user_token_by_id($data['id']);
        $stmt = $this->conn->prepare("UPDATE users SET u_city = ?, u_country = ?, u_name= ? , u_email = ? , u_contact = ? , u_company = ? , u_comp_address = ? , u_nature_bussniess = ? , u_created_at = ? , u_expiry_date = ? , u_pkg_type = ?, u_total_msg = ? , u_is_paid = ?, u_status = ? WHERE u_id = ?");
        $stmt->bind_param("sssssssssssiiii",$data['u_city'], $data['u_country'], $data['name'],$data['email'],$data['contact'],$data['company'],$data['comp_address'],$data['n_bsn'],$data['create_at'],$data['expiry_date'],$data['pkg_type'],$data['total_msg'],$data['u_paid'],$data['u_status'],$data['id']);
        if($stmt->execute())
        {
            $msg = '<script>swal("Good Job!", "User update successfully.", "success");</script>';
            $push->send($token,"Account Approved","Dear ".$data['name']." ! your account has been approved.","");
        }
        else
            $msg = '<script>swal("Oops!", "Unable to update user.", "error");</script>';
        $stmt->close();
        return $msg;
    }

//get user by id

    function get_user_by_id($u_id)
    {
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email, u_contact,u_city, u_country, u_company,u_comp_address,u_nature_bussniess,u_status, u_created_at, u_expiry_date, u_pkg_type, u_total_msg, u_is_paid, u_image, pkg_id FROM users WHERE u_id = ? ");
        $stmt->bind_param("i",$u_id);
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['id'], $cat_arr['name'],$cat_arr['email'],$cat_arr['contact'],$cat_arr['city'], $cat_arr['country'], $cat_arr['company'],$cat_arr['company_address'],$cat_arr['nature_of_business'],$cat_arr['status'],$cat_arr['u_created_at'],$cat_arr['u_expiry_date'],$cat_arr['u_pkg_type'],$cat_arr['u_total_msg'],$cat_arr['u_is_paid'],$cat_arr['u_image'],$cat_arr['pkg_id']);
        $stmt->store_result();
        $stmt->fetch();
        return $cat_arr;
        $stmt->close();
    }

    //delete user
    function delete_user($u_id)
    {
        $stmt = $this->conn->prepare("DELETE FROM users WHERE u_id = ?");
        $stmt1 = $this->conn->prepare("UPDATE messages SET user_id=-1 WHERE user_id= ?");
        $stmt2 = $this->conn->prepare("UPDATE single_chat SET from_user_id=-1 WHERE from_user_id= ?");
        $stmt3 = $this->conn->prepare("UPDATE single_chat SET to_user_id=-1 WHERE to_user_id= ?");
        $stmt4 = $this->conn->prepare("DELETE FROM package_request WHERE p_user_id= ?");
        $stmt->bind_param("i",$u_id);
        $stmt1->bind_param("i",$u_id);
        $stmt2->bind_param("i",$u_id);
        $stmt3->bind_param("i",$u_id);
        $stmt4->bind_param("i",$u_id);
        if($stmt->execute()) {
            $stmt1->execute();
            $stmt2->execute();
            $stmt3->execute();
            $stmt4->execute();
            $msg = '<script>swal("Good Job!", "User Delete successfully.", "success");</script>';
        }
        else
            $msg = '<script>swal("Oops!", "Unable to Delete user.", "error");</script>';
        $stmt->close();
        $stmt1->close();
        $stmt2->close();
        $stmt3->close();
        $stmt4->close();
        return $msg;
    }


//get requested user
    function get_requested_user()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email, u_contact,u_city,u_company,u_status FROM users WHERE u_status=0");
        $stmt->execute();
        $stmt->bind_result($param['u_id'],$param['u_name'],$param['u_email'],$param['u_contact'],$param['u_city'],$param['u_company'],$param['u_status']);
        $stmt->store_result();

        if($param['u_status']==0)
            $param['res_status'] = '<span class="label label-danger">In-Active</span>';
        else
            $param['res_status'] = '<span class="label label-success">Active</span>';
        if(u_status == 0)
        {
            while($stmt->fetch())
            {

                if($param['u_status'] == 0 )
                    $msg .= '<tr>
                            <td>'.$param['u_name'].'</td>
                            <td>'.$param['u_email'].'</td>
                            <td>'.$param['u_contact'].'</td>
                            <td>'.$param['u_city'].'</td>
                            <td>'.$param['u_company'].'</td>
                            <td>'.$param['res_status'].'</td>
                            <td>
                                <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_users?id='.$param['u_id'].'"><i class="material-icons">edit</i></a>
                            </td>
                        </tr>';
            }
        }
        else
        {
            $msg = '<o>swal("Oops!", "User Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }
    
    function get_users_for_update_approval()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_image FROM users WHERE image_approved=0");
        $stmt->execute();
        $stmt->bind_result($param['u_id'],$param['u_name'],$param['u_image']);
        $stmt->store_result();

        
        while($stmt->fetch())
        {
            $msg .= '<tr>
                    <td>'.$param['u_name'].'</td>
                    <td><a href="'.$param['u_image'].'" target="_blank"><img src="'.$param['u_image'].'" width="200"/></a></td>
                    <td>
                        <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple approve-btn" data-user-id="'.$param['u_id'].'"><i class="material-icons">done</i></a>
                        <a class="btn btn-danger btn-circle waves-effect waves-circle waves-purple decline-btn" data-user-id="'.$param['u_id'].'"><i class="material-icons">close</i></a>
                    </td>
                </tr>';
        }
        $stmt->close();
        return $msg;
    }
    
    function approve_user_photo_update_request($u_id)
    {
        $stmt = $this->conn->prepare("UPDATE users SET image_approved=1 WHERE u_id=".$u_id);
        $msg = "";
        $success = false;
        if ($stmt->execute())
        {
            $msg = "Image Approved";
            $success = true;
        } 
        else 
        {
            $msg = "Could not complete operation";
        }
        return json_encode(array('success'=>$success, 'message'=>$msg));
    }
    function decline_user_photo_update_request($u_id)
    {
        global $push;
        $stmt = $this->conn->prepare("UPDATE users SET u_image=null, image_approved=1 WHERE u_id=".$u_id);
        $msg = "";
        $success = false;
        if ($stmt->execute())
        {
            $msg = "Image Declined";
            $success = true;
            $token = $this->get_user_token_by_id($u_id);
            $push->send($token,"Profile Photo Declined","Dear user, admin has declined your profile photo. Please try a different image or contact admin for more info","");
        } 
        else 
        {
            $msg = "Could not complete operation";
        }
        return json_encode(array('success'=>$success, 'message'=>$msg));
    }
// get requested user by id
    function get_request_user_by_id($u_id)
    {
        $stmt = $this->conn->prepare("SELECT u_id, u_name, u_email, u_contact,u_city,u_company,u_comp_address,u_nature_bussniess,u_status FROM users WHERE u_id = ?");
        $stmt->bind_param("i",$u_id);
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['id'], $cat_arr['name'],$cat_arr['email'],$cat_arr['contact'],$cat_arr['city'],$cat_arr['company'],$cat_arr['company_address'],$cat_arr['nature_of_business'],$cat_arr['status']);
        $stmt->store_result();
        $stmt->fetch();
        return $cat_arr;
        $stmt->close();
    }

    function get_all_users_tokens($exclude_id = null, $exclude_country = null, $cid = null, $sub_cid = null, $include_countries = null)
    {
        global $DB;

        $exclude_id_filter = isset($exclude_id) ? "AND u_id != $exclude_id" : "";
        $exclude_country_filter = isset($exclude_country) ? "AND ex_countries NOT LIKE '%$exclude_country%'": "";
        $include_countries_filter = isset($include_countries) &&  strlen($include_countries) > 0 ? "AND FIND_IN_SET(u_country,'$include_countries') > 0" : "";
        $exclude_category_filter = isset($cid) && $cid > 0 ? "AND FIND_IN_SET($cid, blocked_cat) = 0" : "";
        $exclude_sub_category_filter = isset($sub_cid) && $sub_cid > 0 ? "AND FIND_IN_SET($sub_cid, blocked_sub_cat) = 0" : "";
        
        $sql = "SELECT u_fcm_id, ios from users where u_status = 1 $exclude_id_filter $exclude_country_filter $exclude_category_filter $exclude_sub_category_filter $include_countries_filter";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $tokens[] = [$r['u_fcm_id'], $r['ios']];
            }
        }
        return $tokens;
    }

    function get_user_token_by_id($user_id)
    {
        global $DB;
        $sql = "SELECT u_fcm_id, ios from users where u_id = '$user_id' and u_status = 1";
        $res = $DB->qr($sql);
        $token = $DB->fa($res);
        return [$token['u_fcm_id'], $token['ios']];
    }

    function get_user_pkgExpiry_renew()
    {
        global $DB;
        $sql = "SELECT u_id, pkg_id, u_expiry_date, renew_msg_date FROM users WHERE u_status = 1";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = array(
                    'u_id'           =>  $r['u_id'],
                    'pkg_id'         =>  $r['pkg_id'],
                    'u_expiry_date'  =>  $r['u_expiry_date'],
                    'renew_msg_date' =>  $r['renew_msg_date']
                );
            }
        }
        return $data;
    }

    function user_search_record_list()
    {
        $msg = "";
        $param = array();

        $stmt = $this->conn->prepare("SELECT u.u_id, u.u_name, u.u_country, u.u_contact, u.u_company, (select count(*) from user_search us where us.user_id = u.u_id) as count, u.u_buyer_products, u.u_seller_products, u.u_buyer_business_nat, u.u_seller_business_nat, u.u_buyer_desc, u.u_seller_desc FROM (select * from users u1 where (select count(*) from user_search us1 where us1.user_id = u1.u_id) > 0) u ORDER BY count ASC");
        $stmt->execute();
        $stmt->bind_result($param['u_id'],$param['u_name'],$param['u_country'],$param['u_contact'],$param['u_company'],$param['count'],$param['u_buyer_products'],$param['u_seller_products'],$param['u_buyer_business_nat'],$param['u_seller_business_nat'],$param['u_buyer_desc'],$param['u_seller_desc']);
        $stmt->store_result();

        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<tr>
                            <td class="u_name">'. $param['u_name'] .'</td>
                            <td class="u_country">'. $param['u_country'] .'</td>
                            <td class="u_contact">'. $param['u_contact'] .'</td>
                            <td class="u_company">'. $param['u_company'] .'</td>
                           	<td class="count">'. $param['count'] .'</td>
                            <td class="u_buyer_products">'. $param['u_buyer_products'] .'</td>
                            <td class="u_seller_products">'. $param['u_seller_products'] .'</td>
                            <td class="u_buyer_business_nat">'. $param['u_buyer_business_nat'] .'</td>
                            <td class="u_seller_business_nat">'. $param['u_seller_business_nat'] .'</td>
                            <td class="u_buyer_desc" data-buyer="'.$param['u_buyer_desc'].'">'. substr($param['u_buyer_desc'], 0, 20). (($param['u_buyer_desc'] != null)? "...":"" ) .'</td>
                            <td class="u_seller_desc" data-seller="'.$param['u_seller_desc'].'">'. substr($param['u_seller_desc'], 0, 20). (($param['u_seller_desc'] != null)? "...":"" ) .'</td>
                            <td > <button class="btn btn-default btn-circle btn_view_record"> <span class="material-icons">
                            manage_accounts
                            </span></button></td>
                        </tr>';
            }

            // <td> <button class="btn btn-default btn-circle waves-effect waves-circle waves-purple"><i class="far fa-eye"></i></button> </td>

        }
        else
        {
            $msg = '<script>swal("Oops!", "User Search Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
        // $msg = "";
        // $param = array();
        // $stmt = $this->conn->prepare("SELECT u.u_name,u.u_country,u.u_city,us.search_looking_for,us.search_business_nature,us.search_country,us.search_city,us.search_business_category,us.search_keywords FROM user_search AS us JOIN users AS u ON u.u_id = us.user_id");
        // $stmt->execute();
        // $stmt->bind_result($param['u_name'],$param['u_country'],$param['u_city'],$param['search_looking_for'],$param['search_business_nature'],$param['search_country'],$param['search_city'],$param['search_business_category'],$param['search_keywords']);
        // $stmt->store_result();

        // if($stmt->num_rows > 0)
        // {
        //     while($stmt->fetch())
        //     {
        //         $msg .= '<tr>
        //                 <td>'. $param['u_name'].'</td>
        //                 <td>'. $param['u_country'].'</td>
        //                 <td>'. $param['u_city'].'</td>
        //                 <td>'. $param['search_looking_for'].'</td>
        //                 <td>'. $param['search_business_nature'].'</td>
        //                 <td>'. $param['search_country'].'</td>
        //                 <td>'. $param['search_city'].'</td>
        //                 <td>'. $param['search_business_category'].'</td>
        //                 <td>'. $param['search_keywords'].'</td>
        //             </tr>';
        //     }
        // }
        // else
        // {
        //     $msg = '<o>swal("Oops!", "Packages request Not Found.", "error");</o>';
        // }
        // $stmt->close();
        // return $msg;
    }

    function get_selected_country_user($country)
    {
        $msg = '';                   
        
        $stmt = $this->conn->prepare("SELECT u_name, u_contact, u_fcm_id, ios FROM users WHERE u_status = 1 AND u_country = '$country'");
        $stmt->execute();
        $fcm_token = array();
        $stmt->bind_result($fcm_token['name'],$fcm_token['u_contact'],$fcm_token['token'], $fcm_token['ios']);
        $stmt->store_result();

        if($stmt->num_rows > 0)
        {
            while($r = $stmt->fetch())
            {
                $msg .= '<option value="'.$fcm_token['ios'].$fcm_token['token'].'">'.$fcm_token['name']." ".$fcm_token['u_contact'].'</option>';
            }
        }
        else
        {
            $msg = '<option>Users Not Found</option>';
        }
        return json_encode($msg);
        $stmt->close();

    }

    function get_user_seller_profile_list($id=null)
    {
        if($id!=null)
        {
            $query = "SELECT u_id,u_name,u_email,u_contact,u_country,u_city,u_seller_desc,u_seller_company,u_seller_city,u_seller_country,u_seller_business_cat,u_seller_business_nat,u_seller_phone,u_seller_email,u_seller_address,u_seller_products FROM users WHERE u_id IN ($id) Order By u_id";
        }
        else{
            $query = "SELECT u_id,u_name,u_email,u_contact,u_country,u_city,u_seller_desc,u_seller_company,u_seller_city,u_seller_country,u_seller_business_cat,u_seller_business_nat,u_seller_phone,u_seller_email,u_seller_address,u_seller_products FROM users Order By u_id";
        }
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->bind_result($param['u_id'],$param['u_name'],$param['u_email'],$param['u_contact'],$param['u_country'],$param['u_city'],$param['u_seller_desc'],$param['u_seller_company'],$param['u_seller_city'],$param['u_seller_country'],$param['u_seller_business_cat'],$param['u_seller_business_nat'],$param['u_seller_phone'],$param['u_seller_email'],$param['u_seller_address'],$param['u_seller_products']);
        
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                        
                $msg .= '<tr>
                        <td class="u_name">'                                                                                    .((isset($param['u_name'])                 && $param['u_name']                 != "")? $param['u_name']                                      :"").'</td>
                        <td class="u_email">'                                                                                   .((isset($param['u_email'])                && $param['u_email']                != "")? $param['u_email']                                     :"").'</td>
                        <td class="u_contact">'                                                                                 .((isset($param['u_contact'])              && $param['u_contact']              != "")? $param['u_contact']                                   :"").'</td>
                        <td class="u_country">'                                                                                 .((isset($param['u_country'])              && $param['u_country']              != "")? $param['u_country']                                   :"").'</td>
                        <td class="u_city">'                                                                                    .((isset($param['u_city'])                 && $param['u_city']                 != "")? $param['u_city']                                      :"").'</td>
                        <td class="u_seller_desc" data-u_seller_desc="'.@$param['u_seller_desc'].'">'                           .((isset($param['u_seller_desc'])          && $param['u_seller_desc']          != "")? substr($param['u_seller_desc'], 0, 20)."..."          :"").'</td>
                        <td class="u_seller_company">'                                                                          .((isset($param['u_seller_company'])       && $param['u_seller_company']       != "")? $param['u_seller_company']                            :"").'</td>
                        <td class="u_seller_city">'                                                                             .((isset($param['u_seller_city'])          && $param['u_seller_city']          != "")? $param['u_seller_city']                               :"").'</td>
                        <td class="u_seller_country">'                                                                          .((isset($param['u_seller_country'])       && $param['u_seller_country']       != "")? $param['u_seller_country']                            :"").'</td>
                        <td class="u_seller_business_cat" data-u_seller_business_cat="'.$param['u_seller_business_cat'].'">'    .((isset($param['u_seller_business_cat'])  && $param['u_seller_business_cat']  != "")? substr($param['u_seller_business_cat'], 0, 20)."..."  :"").'</td>
                        <td class="u_seller_business_nat" data-u_seller_business_nat="'.$param['u_seller_business_nat'].'">'    .((isset($param['u_seller_business_nat'])  && $param['u_seller_business_nat']  != "")? substr($param['u_seller_business_nat'], 0, 20)."..."  :"").'</td>
                        <td class="u_seller_phone">'                                                                            .((isset($param['u_seller_phone'])         && $param['u_seller_phone']         != "")? $param['u_seller_phone']                              :"").'</td>
                        <td class="u_seller_email">'                                                                            .((isset($param['u_seller_email'])         && $param['u_seller_email']         != "")? $param['u_seller_email']                              :"").'</td>
                        <td class="u_seller_address" data-u_seller_address="'.$param['u_seller_address'].'">'                   .((isset($param['u_seller_address'])       && $param['u_seller_address']       != "")? substr($param['u_seller_address'], 0, 20)."..."       :"").'</td>
                        <td class="u_seller_product" data-u_seller_products="'.$param['u_seller_products'].'">'                 .((isset($param['u_seller_products'])       && $param['u_seller_products']     != "")? substr($param['u_seller_products'], 0, 20)."..."      :"").'</td>
                        <td class="action_btn"> 
                            <button class="btn btn-default btn-circle btn_view_record"> <span class="material-icons"> manage_accounts </span></button>
                        </td>
                    </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "User Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
        
    }
    
    
    function get_user_buyer_profile_list($id=null)
    {
        if($id!=null)
        {
            $query = "SELECT u_id,u_name,u_email,u_contact,u_country,u_city,u_buyer_desc,u_buyer_company,u_buyer_city,u_buyer_country,u_buyer_business_cat,u_buyer_business_nat,u_buyer_phone,u_buyer_email,u_buyer_address,u_buyer_products FROM users WHERE u_id IN ($id) Order By u_id";
        }
        else{
            $query = "SELECT u_id,u_name,u_email,u_contact,u_country,u_city,u_buyer_desc,u_buyer_company,u_buyer_city,u_buyer_country,u_buyer_business_cat,u_buyer_business_nat,u_buyer_phone,u_buyer_email,u_buyer_address,u_buyer_products FROM users Order By u_id";
        }
        // $query = "SELECT u_id,u_name,u_email,u_contact,u_country,u_city,u_buyer_desc,u_buyer_company,u_buyer_city,u_buyer_country,u_buyer_business_cat,u_buyer_business_nat,u_buyer_phone,u_buyer_email,u_buyer_address,u_buyer_products FROM users Order By u_id";
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $stmt->bind_result($param['u_id'],$param['u_name'],$param['u_email'],$param['u_contact'],$param['u_country'],$param['u_city'],$param['u_buyer_desc'],$param['u_buyer_company'],$param['u_buyer_city'],$param['u_buyer_country'],$param['u_buyer_business_cat'],$param['u_buyer_business_nat'],$param['u_buyer_phone'],$param['u_buyer_email'],$param['u_buyer_address'],$param['u_buyer_products']);
        
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<tr>
                        <td class="u_name">'                                                                                .((isset($param['u_name'])                 && $param['u_name']                 != "")? $param['u_name']                                     :"").'</td>
                        <td class="u_email">'                                                                               .((isset($param['u_email'])                && $param['u_email']                != "")? $param['u_email']                                    :"").'</td>
                        <td class="u_contact">'                                                                             .((isset($param['u_contact'])              && $param['u_contact']              != "")? $param['u_contact']                                  :"").'</td>
                        <td class="u_country">'                                                                             .((isset($param['u_country'])              && $param['u_country']              != "")? $param['u_country']                                  :"").'</td>
                        <td class="u_city">'                                                                                .((isset($param['u_city'])                 && $param['u_city']                 != "")? $param['u_city']                                     :"").'</td>
                        <td class="u_buyer_desc" data-u_buyer_desc="'.@$param['u_buyer_desc'].'">'                          .((isset($param['u_buyer_desc'])          && $param['u_buyer_desc']          != "")? substr($param['u_buyer_desc'], 0, 20)."..."            :"").'</td>
                        <td class="u_buyer_company">'                                                                       .((isset($param['u_buyer_company'])       && $param['u_buyer_company']       != "")? $param['u_buyer_company']                              :"").'</td>
                        <td class="u_buyer_city">'                                                                          .((isset($param['u_buyer_city'])          && $param['u_buyer_city']          != "")? $param['u_buyer_city']                                 :"").'</td>
                        <td class="u_buyer_country">'                                                                       .((isset($param['u_buyer_country'])       && $param['u_buyer_country']       != "")? $param['u_buyer_country']                              :"").'</td>
                        <td class="u_buyer_business_cat" data-u_buyer_business_cat="'.$param['u_buyer_business_cat'].'">'   .((isset($param['u_buyer_business_cat'])  && $param['u_buyer_business_cat']  != "")? substr($param['u_buyer_business_cat'], 0, 20)."..."    :"").'</td>
                        <td class="u_buyer_business_nat" data-u_buyer_business_nat="'.$param['u_buyer_business_nat'].'">'   .((isset($param['u_buyer_business_nat'])  && $param['u_buyer_business_nat']  != "")? substr($param['u_buyer_business_nat'], 0, 20)."..."    :"").'</td>
                        <td class="u_buyer_phone">'                                                                         .((isset($param['u_buyer_phone'])         && $param['u_buyer_phone']         != "")? $param['u_buyer_phone']                                :"").'</td>
                        <td class="u_buyer_email">'                                                                         .((isset($param['u_buyer_email'])         && $param['u_buyer_email']         != "")? $param['u_buyer_email']                                :"").'</td>
                        <td class="u_buyer_address" data-u_buyer_address="'.$param['u_buyer_address'].'">'                  .((isset($param['u_buyer_address'])       && $param['u_buyer_address']       != "")? substr($param['u_buyer_address'], 0, 20)."..."         :"").'</td>
                        <td class="u_buyer_product" data-u_buyer_product="'.$param['u_buyer_products'].'">'                 .((isset($param['u_buyer_products'])       && $param['u_buyer_products']     != "")? substr($param['u_buyer_products'], 0, 20)."..."        :"").'</td>
                        <td class="action_btn"> 
                            <button class="btn btn-default btn-circle btn_view_record"> <span class="material-icons"> manage_accounts </span></button>
                        </td>
                    </tr>';
                        
                // $msg .= '<tr>
                //         <td>'.((isset($param['u_name'])                 && $param['u_name']                 != "")? $param['u_name']                :"").'</td>
                //         <td>'.((isset($param['u_email'])                && $param['u_email']                != "")? $param['u_email']               :"").'</td>
                //         <td>'.((isset($param['u_contact'])              && $param['u_contact']              != "")? $param['u_contact']             :"").'</td>
                //         <td>'.((isset($param['u_country'])              && $param['u_country']              != "")? $param['u_country']             :"").'</td>
                //         <td>'.((isset($param['u_city'])                 && $param['u_city']                 != "")? $param['u_city']                :"").'</td>
                //         <td>'.((isset($param['u_buyer_desc'])          && $param['u_buyer_desc']          != "")? $param['u_buyer_desc']         :"").'</td>
                //         <td>'.((isset($param['u_buyer_company'])       && $param['u_buyer_company']       != "")? $param['u_buyer_company']      :"").'</td>
                //         <td>'.((isset($param['u_buyer_city'])          && $param['u_buyer_city']          != "")? $param['u_buyer_city']         :"").'</td>
                //         <td>'.((isset($param['u_buyer_country'])       && $param['u_buyer_country']       != "")? $param['u_buyer_country']      :"").'</td>
                //         <td>'.((isset($param['u_buyer_business_cat'])  && $param['u_buyer_business_cat']  != "")? $param['u_buyer_business_cat'] :"").'</td>
                //         <td>'.((isset($param['u_buyer_business_nat'])  && $param['u_buyer_business_nat']  != "")? $param['u_buyer_business_nat'] :"").'</td>
                //         <td>'.((isset($param['u_buyer_phone'])         && $param['u_buyer_phone']         != "")? $param['u_buyer_phone']        :"").'</td>
                //         <td>'.((isset($param['u_buyer_email'])         && $param['u_buyer_email']         != "")? $param['u_buyer_email']        :"").'</td>
                //         <td>'.((isset($param['u_buyer_address'])       && $param['u_buyer_address']       != "")? $param['u_buyer_address']      :"").'</td>
                //         <td>'.((isset($param['u_buyer_product'])       && $param['u_buyer_product']       != "")? $param['u_buyer_product']      :"").'</td>
                //         <td> 
                //             <button class="btn btn-default btn-circle btn_view_record"> <span class="material-icons"> manage_accounts </span></button>
                //         </td>
                //     </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "User Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
        
    }


    public function get_user_seller_profile_count()
    {
        // return json_encode('bilal');
        $stmt = $this->conn->prepare("SELECT * FROM `user_profile_update` WHERE profile = 0 AND is_seen = 0");
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;
        return $count;

    }

    public function get_user_buyer_profile_count()
    {
        // return json_encode('bilal');
        $stmt = $this->conn->prepare("SELECT * FROM `user_profile_update` WHERE profile = 1 AND is_seen = 0");
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;
        return $count;

    }

    public function user_seller_profile_count_seen()
    {
        $result = $this->get_updated_seller_unseen_profile();
        global $DB;
        $query= "UPDATE user_profile_update SET is_seen=1 WHERE profile = 0";
        if($DB->qr($query))
        {
            return $result;
        }
        else
        {
            return $result;
        }
    }

    public function user_buyer_profile_count_seen()
    {
        $result = $this->get_updated_buyer_unseen_profile();
        global $DB;
        $query= "UPDATE user_profile_update SET is_seen=1 WHERE profile = 1";
        if($DB->qr($query))
        {
            return $result;
        }
        else
        {
            return "";
        }
    }

    public function get_updated_seller_unseen_profile() // user seller profile for admin portal
    {
        $id_list = array();
        $stmt = $this->conn->prepare("SELECT user_id FROM `user_profile_update` WHERE profile = 0 AND is_seen = 0");
        $stmt->execute();
        $stmt->bind_result($param['id']);
        $stmt->store_result();
        if($stmt->num_rows)
        {
            while ($stmt->fetch()) 
            {
                $id_list[] = $param['id'];
            }
        }
        $id = implode(',',$id_list);

        $result = $this->get_user_seller_profile_list($id);

        return $result;
    }

    public function get_updated_buyer_unseen_profile() // user buyer profile for admin portal
    {
        $id_list = array();
        $stmt = $this->conn->prepare("SELECT user_id FROM `user_profile_update` WHERE profile = 1 AND is_seen = 0");
        $stmt->execute();
        $stmt->bind_result($param['id']);
        $stmt->store_result();
        if($stmt->num_rows)
        {
            while ($stmt->fetch()) 
            {
                $id_list[] = $param['id'];
            }
        }
        $id = implode(',',$id_list);
        
        $result = $this->get_user_buyer_profile_list($id);

        return $result;
    }

 }
?>