<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 10/2/2018
 * Time: 12:38 PM
 */
include_once "../include/header.php";
$n_id = $_GET['id'];
if(isset($_POST['subBtn']))
{
    $name = $_POST['title'];
    $desc = $_POST['desc'];
    echo $news_obj->update_news($n_id,$name,$desc);
}
if(isset($_POST['deleteBtn']))
{
    echo $news_obj->delete_news($n_id);
}
    $cat_arr  = $news_obj->get_news_by_id($n_id);
?>
<!-- Basic Validation -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit News</h2>
            </div>
            <div class="body">
                <form id="form_validation" enctype="multipart/form-data" method="POST" action="">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="title" value="<? echo $cat_arr['title']?>" required>
                            <label class="form-label">Title</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea name="desc" cols="30" rows="5" class="form-control no-resize" required><? echo $cat_arr['desc']?></textarea>
                            <label class="form-label">Description</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                        <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT<i class="material-icons">send</i></button>
                        <button class="btn btn-danger waves-effect" name="deleteBtn" type="submit"><i class="material-icons">delete</i>DELETE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>
<!-- #END# Basic Validation -->