<?php

class news
{
    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }


    function add_news($title, $description, $countries)
    {
        // global $push, $user;
        // $tokens = $user->get_all_users_tokens();
        $date = date('m/d/Y h:i:s a', time());

        $stmt = $this->conn->prepare("INSERT INTO news(n_title, n_desc, n_date, n_countries) VALUES (?,?,?,?)");
        $stmt->bind_param("ssss", $title, $description, $date, $countries);
        if ($stmt->execute())
        {
            $last_id = $stmt->insert_id;
            $msg = '<script>swal("Good Job!", "News add successfully.", "success");</script>';
            
            // send notification to users
            $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/include/news_notif_process.php";
            $cmd = "php -f $path_to_script $last_id";
            $outputfile = "news_notif_process_logs.txt";
            $pidfile = "news_notif_process_pids.txt";
            exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
            
            // $chunks = array_chunk($tokens, 999);
            // for ($x = 0; $x < count($chunks); $x++) {
            //      $push->send_push_to_news($chunks[$x],"News Update","Please check New News","");
            // }
            
            // update badges for ios users
            // $path_to_script = $_SERVER['DOCUMENT_ROOT'] . "/chat_api/Classes/update_badges.php";
            // $cmd = "php -f $path_to_script ns -1 1";
            // $outputfile = "badges_logs.txt";
            // $pidfile = "badges_pids.txt";
            // exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
           
        }
        else
            $msg = '<script>swal("Oops!", "Unable to add news.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    //select all news
    function get_all_news()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT n_id, n_title, n_desc, n_date, n_countries FROM news ");
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['title'],$param['desc'], $param['n_date'], $param['n_countries']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $c = $param['n_countries'];
                if (empty($c)) {
                    $c = "All";    
                }
                $msg .= '<tr>
                            <td>'.$param['title'].'</td>
                            <td>'.$param['desc'].'</td>
                            <td>'.$param['n_date'].'</td>
                            <td>'.$c.'</td>
                            <td>
                                <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_news?id='.$param['id'].'"><i class="material-icons">edit</i></a>
                            </td>
                        </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Category Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    //update news
    function update_news($n_id,$name,$desc)
    {
        global $push, $user;
        $tokens = $user->get_all_users_tokens();

        $stmt = $this->conn->prepare("UPDATE news SET n_title = ?, n_desc = ? WHERE  n_id = ?");
        $stmt->bind_param("ssi",$name,$desc,$n_id);
        if($stmt->execute())
        {
            $msg = '<script>swal("Good Job!", "News update successfully.", "success");</script>';
            $chunks = array_chunk($tokens, 999);
            for ($x = 0; $x < count($chunks); $x++) {
                 $push->send_push_to_news($chunks[$x],"News Update","Please check New News","");
            }
        }
        else
            $msg = '<script>swal("Oops!", "Unable to update news.", "error");</script>';
        $stmt->close();
        return $msg;
    }


    //delete news
    function delete_news($n_id)
    {
        $stmt = $this->conn->prepare("DELETE FROM news WHERE n_id = ?");
        $stmt->bind_param("i",$n_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "News Delete successfully.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to Delete News.", "error");</script>';
        $stmt->close();
        return $msg;
    }


    //get news by id
    function get_news_by_id($n_id)
    {
        $stmt = $this->conn->prepare("SELECT n_title, n_desc FROM news WHERE n_id = ?");
        $stmt->bind_param("s",$n_id);
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['title'],$cat_arr['desc']);
        $stmt->store_result();
        $stmt->fetch();
        return $cat_arr;
        $stmt->close();
    }
}

