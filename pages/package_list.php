<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 12:24 PM
 */
include_once "../include/header.php";
?>
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Package List
                </h2>
                <br>
                Exchange Rate: <input type="text" name="e_rate" id="input_e_rate" value="" />
                <input type="submit" id="btn_save_e_rate" value="Save"/>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>Package</th>
                            <th>Messages</th>
                            <th>Package Duration</th>
                            <th>Price</th>
                            <th>Advertisement</th>
                            <th>Paid</th>
                            <th>Edit/Delete</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Package</th>
                            <th>Messages</th>
                            <th>Package Duration</th>
                            <th>Price</th>
                            <th>Advertisement</th>
                            <th>Paid</th>
                            <th>Edit/Delete</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php echo $package->get_all_pakages();?>
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="message">
</div>
<!-- #END# Exportable Table -->
<?php
include_once "../include/footer.php"
?>
<!--Delete package-->
<script>
$(document).on("click",".btn_delete",function(e){
    var package_id = $(this).data('package-id');
    var obj = $(this);
    $.ajax({
        type: "POST",
        data: {'request':'delete_package','package_id':package_id},
        url: "ajaxcall.php",
        success: function(data)
        {
            obj.parent().parent().remove();
            $("#message").html(data);
        }
    });
});

$(document).on("click","#btn_save_e_rate",function(e){
    var e_rate = $('#input_e_rate').val();
    e_rate = parseFloat(e_rate);
    if (!isNaN(e_rate)) {
        $.ajax({
            type: "POST",
            data: {'request':'update_e_rate', 'e_rate': e_rate},
            url: "ajaxcall.php",
            success: function(data)
            {
                swal("Success", "Exchange rate updated!", "success");
            }
        });
    }
});

$.ajax({
    type: "POST",
    data: {'request':'get_e_rate'},
    url: "ajaxcall.php",
    success: function(data)
    {
        data = JSON.parse(data);
        if (data.e_rate) {
            $('#input_e_rate').val(data.e_rate);
        }
    }
});

</script>
<!--//Delete package-->