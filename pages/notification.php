<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 1:59 PM
 */
include_once "../include/header.php";
include_once "../chat_api/Classes/PushNotification.php";

$push = new PushNotification();
$result = null;
function extractTokenAndDeviceType($token) {
    return [substr($token,1), $token[0]];
}
if(isset($_POST['sendSinglePush']))
{
    
    $single_token = [substr($_POST['single_token'], 1), $_POST['single_token'][0]];
    $title  = $_POST['single_name'];
    $msg = $_POST['single_description'];
    $image = $_POST['big_image_url_single'];
    $result = $push->send($single_token,$title,$msg,$image);
    
}
if(isset($_POST['sendmultiplePush']))
{


    // set_time_limit(0);
    // $temp = array();
    // $count = 0;
    // foreach ($_POST['multiple_token'] as $key => $value) {
    //     $temp[$count] = $value;
    //     $count++;
    // }
    // $arr2 = json_encode($count);
    // $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
    // fwrite($myfile, $arr2);
    // fclose($myfile);


    $multiple_tokens = array_map("extractTokenAndDeviceType", $_POST['multiple_token']);

    
    $m_title  = $_POST['multiple_name'];
    $m_msg = $_POST['multiple_description'];
    $m_image = $_POST['big_image_url_multiple'];
    $result =  $push->send_multiple($multiple_tokens,$m_title,$m_msg, $m_image);
    echo "<script>console.log('" . json_encode($result) . "');</script>";

    $arr2 = json_encode($result);
    // // $arr2 = json_encode($temp);
    $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
    fwrite($myfile, $arr2);
    fclose($myfile);
}

if ($result) {
echo '<script>
    var result = ' . $result. ';
    console.log(result);
    if (!result["success"]) {
        swal("Error", "Failed to send push notification", "error");    
    }
    else if (result["success"] >= 1 && result["failure"] == 0){
        swal("Success", "Successfully sent push notification", "success");
    } 
    else{
        swal("Info", "Successfully sent push notification to " + result["success"] + " user(s) \n Failed to send to " + result["failure"] + " user(s)", "info");
    }
    </script>';
}


?>

<!-- Tabs With Icon Title -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    SEND PUSH NOTIFICATION
                </h2>
            </div>
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home_with_icon_title" data-toggle="tab">
                            <i class="material-icons">person</i> Single User
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#profile_with_icon_title" data-toggle="tab">
                            <i class="material-icons">group</i> Multiple Users
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                        <!-- <b>Home Content</b> -->
                        <div class="body">
                            <form id="form_validation" action="" method="POST">
                                <div class="form-group form-float">
                                    <p>
                                        <b>Select Country</b>
                                    </p>
                                    <select name="single_token_country" id="single_token_country" class="form-control show-tick" data-live-search="true" data-value="single_user">
                                        <option selected disabled>Select Option</option>
                                        <?= $country->get_country(); ?>
                                    </select>
                                </div>
                                <div class="form-group form-float">
                                    <p>
                                        <b>Select User</b>
                                    </p>
                                    <select name="single_token" class="form-control show-tick" data-live-search="true" id="single_user_select">
                                        
                                    </select>

                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="single_name" required>
                                        <label class="form-label">Title</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea name="single_description" cols="30" rows="5" class="form-control no-resize" required></textarea>
                                        <label class="form-label">Description</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="big_image_url_single">
                                        <label class="form-label">Big Image URL</label>
                                    </div>
                                </div>

                                <button class="btn btn-primary waves-effect" name="sendSinglePush" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                        <div class="body">
                            <form id="form_validation" action="" method="POST">

                                <div class="form-group form-float">
                                    <p>
                                        <b>Select Country</b>
                                    </p>
                                    <select name="multiple_token_country" id="multi_token_country" class="form-control show-tick" data-live-search="true" data-value="multi_user">
                                        <option selected disabled>Select Option</option>
                                        <?= $country->get_country(); ?>
                                    </select>
                                </div>
                                <div class="form-group form-float">
                                    <p>
                                        <b>Select Users</b>
                                    </p>
                                    <select name="multiple_token[]" multiple class="form-control show-tick" data-live-search="true" data-actions-box="true" id="multi_user_select">
                                        
                                    </select>

                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="multiple_name" required>
                                        <label class="form-label">Title</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <textarea name="multiple_description" cols="30" rows="5" class="form-control no-resize" required></textarea>
                                        <label class="form-label">Description</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="big_image_url_multiple">
                                        <label class="form-label">Big Image URL</label>
                                    </div>
                                </div>

                                <button class="btn btn-primary waves-effect" name="sendmultiplePush" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Tabs With Icon Title -->

<?php
include_once "../include/footer.php";
?>
<!-- #END# Basic Validation -->

<script>

$('#single_token_country').change(function(){
    var dorpDownType = $('#single_token_country').data('value');
    countryUser(dorpDownType);
});
$('#multi_token_country').change(function(){
    var dorpDownType = $('#multi_token_country').data('value');

    countryUser(dorpDownType);
});
function countryUser(val)
{
    var dorpDownType = val;

    if(dorpDownType == 'single_user'){
        var country = $('#single_token_country').val();
    }
    else if (dorpDownType == 'multi_user') {
        var country = $('#multi_token_country').val();
    }
    var count = 0;
    $.ajax({
        type: "POST",
        async:false,
        data: {'request':'get_selected_country_user','country':country},
        url: "ajaxcall.php",
        success: function(data)
        {
            var d = JSON.parse(data);

            if(dorpDownType == 'single_user'){
                $("#single_user_select").html("");
                $("#single_user_select").selectpicker("refresh");

                $("#single_user_select").append(d);
                $("#single_user_select").selectpicker("refresh");
                
            }
            else if (dorpDownType == 'multi_user') {

                $("#multi_user_select").html("");
                $("#multi_user_select").selectpicker("refresh");
                $("#multi_user_select").append(d);
                $("#multi_user_select").selectpicker("refresh");
                
            }
         

        }
    });
   
}

</script>