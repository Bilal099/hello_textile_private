<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

include_once "../include/header.php";
?>

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    News List
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Date Time</th>
                            <th>Viewer Countries</th>
                            <th>Edit</th>

                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Date Time</th>
                            <th>Viewer Countries</th>
                            <th>Edit</th>

                        </tr>
                        </tfoot>
                        <tbody>
<!--                        <tr>-->
<!--                            <td><span class="label label-danger">In-Active</span></td>-->
<!--                            <td>-->
<!--                                <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float">-->
<!--                                    <i class="material-icons">edit</i>-->
<!--                                </button>-->
<!--                            </td>-->
<!--                        </tr>-->
                        <?php echo $news_obj->get_all_news(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

<?php
include_once "../include/footer.php"
?>
