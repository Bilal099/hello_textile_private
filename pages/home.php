<?php
include_once "../include/header.php";
?>

<!-- List Example -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Unread Message
                    <small>Here you can find what's for approval | Waiting.</code></small>
                </h2>
            </div>
            <div class="body">
                <div class="list-group">
                    <div>
                        <input type="checkbox" id="load_all" name="load_all" /> <label for="load_all"
                            title="Check this only if you see 'No messages to show' but badge count is greater than 0">Should
                            Load All Messages</label>
                    </div>
                    <a id="open_modal" href="javascript:void(0);" class="list-group-item">
                        <span id="span_count_msg" class="badge bg-pink"></span> You have unread messages
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# List Example -->

<!-- List Example -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Message on Hold
                    <small>Here you can find all hold messages.</code></small>
                </h2>
            </div>
            <div class="body">
                <div class="list-group">

                    <a id="hold_modal" href="javascript:void(0);" class="list-group-item">
                        <span id="count_hold_msg" class="badge bg-pink"></span> You have hold messages
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# List Example -->

<!-- List of seller profile -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    User updated profiles as seller
                    <small>Here you can find updated users.</code></small>
                </h2>
            </div>
            <div class="body">
                <div class="list-group">

                    <a id="seller_modal" href="javascript:void(0);" class="list-group-item">
                        <input type="hidden" name="seller_count" id="seller_count" value="0">
                        <span id="count_seller_profile" class="badge bg-pink"></span> Updated Profiles
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# List of seller profile -->

<!-- List of buyer profile -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    User updated profiles as buyer
                    <small>Here you can find updated users.</code></small>
                </h2>
            </div>
            <div class="body">
                <div class="list-group">

                    <a id="buyer_modal" href="javascript:void(0);" class="list-group-item">
                        <input type="hidden" name="buyer_count" id="buyer_count" value="0">
                        <span id="count_buyer_profile" class="badge bg-pink"></span> Updated Profiles
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# List of buyer profile -->

<div class="modal fade" id="defaultModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Category</b>
                        </p>
                        <select name="status" id="category" class="form-control show-tick">
                            <? echo $category_obj->select_category();?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Sub Category</b>
                        </p>
                        <select name="status" id="subCategory" class="form-control show-tick">
                            <option value="1">Active</option>
                            <option value="0">In-Active</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label>Message</label>
                        <textarea id="current_message" name="single_description" cols="30" rows="5"
                            class="form-control no-resize" required></textarea>
                        <div id="images"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" id="btn_allow">ALLOW</button>
                <button type="button" class="btn btn-link waves-effect" id="btn_hold">HOLD</button>
                <button type="button" class="btn btn-link waves-effect" id="btn_delete">DELETE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="HoldModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="holdLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Category</b>
                        </p>
                        <select name="status" id="hcategory" class="form-control show-tick">
                            <? echo $category_obj->select_category();?>
                        </select>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Sub Category</b>
                        </p>
                        <select name="status" id="hsubCategory" class="form-control show-tick">
                            <option value="1">Active</option>
                            <option value="0">In-Active</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-float">
                    <div class="form-line">
                        <label>Message</label>
                        <textarea id="hcurrent_message" name="single_description" cols="30" rows="5"
                            class="form-control no-resize" required></textarea>
                        <div id="himages"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" id="hbtn_allow">ALLOW</button>
                <button type="button" class="btn btn-link waves-effect" id="hbtn_delete">DELETE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ProfileSellerModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="holdLabel">Users Profiles</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Seller</b>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Country</th>
                                        <th>City</th>
                                        <th>Seller Description</th>
                                        <th>Seller Company</th>
                                        <th>Seller City</th>
                                        <th>Seller Country</th>
                                        <th>Seller Business Category</th>
                                        <th>Seller Business Nature</th>
                                        <th>Seller Phone</th>
                                        <th>Seller Email</th>
                                        <th>Seller Address</th>
                                        <th>Seller Product</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Country</th>
                                        <th>City</th>
                                        <th>Seller Description</th>
                                        <th>Seller Company</th>
                                        <th>Seller City</th>
                                        <th>Seller Country</th>
                                        <th>Seller Business Category</th>
                                        <th>Seller Business Nature</th>
                                        <th>Seller Phone</th>
                                        <th>Seller Email</th>
                                        <th>Seller Address</th>
                                        <th>Seller Product</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </tfoot>
                                <tbody id="seller_table_body" >

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ProfileBuyerModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="holdLabel">Users Profiles</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-float">
                    <div class="form-line">
                        <p>
                            <b>Buyer</b>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Buyer Description</th>
                                    <th>Buyer Company</th>
                                    <th>Buyer City</th>
                                    <th>Buyer Country</th>
                                    <th>Buyer Business Category</th>
                                    <th>Buyer Business Nature</th>
                                    <th>Buyer Phone</th>
                                    <th>Buyer Email</th>
                                    <th>Buyer Address</th>
                                    <th>Buyer Product</th>
                                    <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Buyer Description</th>
                                    <th>Buyer Company</th>
                                    <th>Buyer City</th>
                                    <th>Buyer Country</th>
                                    <th>Buyer Business Category</th>
                                    <th>Buyer Business Nature</th>
                                    <th>Buyer Phone</th>
                                    <th>Buyer Email</th>
                                    <th>Buyer Address</th>
                                    <th>Buyer Product</th>
                                    <!-- <th>Action</th> -->
                                    </tr>
                                </tfoot>
                                <tbody id="buyer_table_body">

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>

<script>
    $(document).ready(function () {
        get_unread_count_of_msg_in_lis_in_home_page();
        get_hold_msg_count();
        get_user_seller_profile_count();
        get_user_buyer_profile_count();
    });
    $("#category").change(function (e) {
        load_subcategories();
    });
    $("#hcategory").change(function (e) {
        H_load_subcategories();
        //alert();
    });
    $("#open_modal").click(function (e) {

        if (load_unapproved_msgs() == 1) {
            $('#defaultModal').modal('show');
            // $('#open_modal').keyup(function (e)
            // {
            //     if(e.which == 13) {
            //         alert("enter press");
            //     }
            //     else
            //     {
            //         alert(e.which);
            //         //DK here
            //     }
            // });
        } else {
            swal("There are no any messages to approve!");
        }
        // $('#defaultModal').modal('refresh');
    });
    $('#defaultModal').on('hide.bs.modal', function () {
        $.ajax({
            type: "POST",
            data: {
                request: 'set_msg_is_loaded',
                message_id: $('#defaultModal').attr('data-msg-id')
            },
            url: "ajaxcall.php"
        });
    })
    $("#hold_modal").click(function () {
        if (loadHoldMessages() == 1) {
            $('#HoldModal').modal('show');
            // $('#open_modal').keyup(function (e)
            // {
            //     if(e.which == 13) {
            //         alert("enter press");
            //     }
            //     else
            //     {
            //         alert(e.which);
            //         //DK here
            //     }
            // });
        } else {
            swal("There are no any messages to approve in Hold!");
        }
    });

    $("#seller_modal").click(function (e) {
        let count = $("#seller_count").val();
        if (count > 0) {

            $.ajax({
                type: "POST",
                data: {
                    'request': 'user_seller_profile_count_seen'
                },
                url: "ajaxcall.php",
                success: function (data) {
                    $("#seller_table_body").html(data);
                    $(".action_btn").hide();
                    $("#ProfileSellerModal").modal('show');
                    $("#seller_count").val(0);
                    $("#count_seller_profile").text(0 + " New");

                }
            });
        }
    });

    $("#buyer_modal").click(function (e) {
        let count = $("#buyer_count").val();
        if (count > 0) {
            $.ajax({
                type: "POST",
                data: {
                    'request': 'user_buyer_profile_count_seen'
                },
                url: "ajaxcall.php",
                success: function (data) {
                    $("#buyer_table_body").html(data);
                    $(".action_btn").hide();
                    $("#ProfileBuyerModal").modal('show');
                    $("#buyer_count").val(0);
                    $("#count_buyer_profile").text(0 + " New");

                }
            });
        }
    });

    $(".btn_view_record").click(function (e) { 
        e.preventDefault();
        var arr = [
            '.u_name',
            '.u_email',
            '.u_contact',
            '.u_country',
            '.u_city',
            '.u_seller_desc',
            '.u_seller_company',
            '.u_seller_city',
            '.u_seller_country',
            '.u_seller_business_cat',
            '.u_seller_business_nat',
            '.u_seller_phone',
            '.u_seller_email',
            '.u_seller_address',
            '.u_seller_product'
        ];

        var label = [
            'Name',
            'Email',
            'Contact',
            'Country',
            'City',
            'Seller Description',
            'Seller Company',
            'Seller City',
            'Seller Country',
            'Seller Business_cat',
            'Seller Business_nat',
            'Seller Phone',
            'Seller Email',
            'Seller Address',
            'Seller Product'
        ];

        var i;
        var text = '';
        var val = '';
        for (i = 0; i < label.length; i++) 
        {
            if (arr[i] == '.u_seller_business_cat' || arr[i] == '.u_seller_business_nat' || arr[i] == '.u_seller_address' || arr[i] == '.u_seller_product' || arr[i] == '.u_seller_desc' ) 
            {
                val = $(this).closest('tr').find(arr[i]).data(arr[i].replace('.',''));
                let temp = (val != null)? val:"";
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+temp+`</p>
                    </div>
                    </div>`;
            }
            else{
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+$(this).closest('tr').find(arr[i]).text()+`</p>
                    </div>
                    </div>`;
            }
            
        }

        $(".modal-body").html(text);
        $("#view_modal").modal("show");

    });
</script>
<!--SQ code-->
<script>
    //load message into modal
    var current_chat_id = null;

    function load_unapproved_msgs() {
        // console.log("bilal");
        $("#images").html("");
        var count = 0;
        var package_id = $(this).data('package-id');
        var obj = $(this);
        var shouldLoadAll = $('#load_all').is(':checked') ? 1 : 0;
        $.ajax({
            type: "POST",
            async: false,
            data: {
                'request': 'new_msg_for_admin_approve',
                'should_load_all': shouldLoadAll
            },
            url: "ajaxcall.php",
            success: function (data) {
                var d = JSON.parse(data);
                if (d.message_id != 0 && d.message_id != null) {
                    current_chat_id = d.message_id;
                    $('#defaultModal').attr('data-msg-id', d.message_id);
                    $("#category").val(d.c_id).selectpicker('refresh');
                    load_subcategories();
                    // $("#subCategory option").attr('selected', '').selectpicker('refresh');
                    // $("#subCategory option[value="+d.sc_id+"]").attr('selected', 'selected').selectpicker('refresh');
                    $("#subCategory").val(d.sc_id).selectpicker('refresh');
                    // $('#subCategory').selectpicker('refresh');
                    $("#current_message").val(d.message);
                    if (d.images) {
                        var images = d.images.split(",").map(function (a) {
                            return "<a target='_blank' href='/upload/" + a +
                                "'><img src='/upload/" + a + "' width='100'/></a>";
                        });
                        images = images.join("");
                        $("#images").html(images);
                    }
                    $("#defaultModalLabel").text(d.u_name + " | " + d.u_contact + " | " + d.u_pkg_type +
                        " | " + d.u_country);
                    count = 1;
                }

            }
        });
        return count;
    }
    //

    function loadHoldMessages() {
        $("#himages").html("");
        var count = 0;
        $.ajax({
            type: "POST",
            async: false,
            data: {
                'request': 'hold_message_approve'
            },
            url: "ajaxcall.php",
            success: function (data) {
                var d = JSON.parse(data);
                if (d.message_id != 0 && d.message_id != null) {
                    current_chat_id = d.message_id;
                    $('#HoldModal').attr('data-msg-id', d.message_id);
                    $("#hcategory").val(d.c_id).selectpicker('refresh');
                    H_load_subcategories();
                    $("#hsubCategory option[value=" + d.sc_id + "]").attr('selected', 'selected');
                    $('#hsubCategory').selectpicker('refresh');
                    $("#hcurrent_message").val(d.message);

                    if (d.images) {
                        var images = d.images.split(",").map(function (a) {
                            return "<a target='_blank' href='/upload/" + a +
                                "'><img src='/upload/" + a + "' width='100'/></a>";
                        });
                        images = images.join("");
                        $("#himages").html(images);
                    }
                    $("#holdLabel").text(d.u_name + " | " + d.u_contact + " | " + d.u_pkg_type + " | " + d
                        .u_country);
                    count = 1;
                }

            }
        });
        return count;
    }

    function load_subcategories() {
        var id = $("#category").val();
        $.ajax({
            type: "POST",
            async: false,
            data: {
                request: 'load_sub_category_for_send_message',
                catt_id: id
            },
            url: "ajaxcall.php",
            success: function (data) {
                $('#subCategory').html(data);
                $('#subCategory').selectpicker('refresh');
            }
        });
    }

    function H_load_subcategories() {
        var id = $("#hcategory").val();
        $.ajax({
            type: "POST",
            data: {
                request: 'load_sub_category_for_send_message',
                catt_id: id
            },
            url: "ajaxcall.php",
            success: function (data) {
                $('#hsubCategory').html(data);
                $('#hsubCategory').selectpicker('refresh');
            }
        });
    }

    //
    $("#btn_allow").click(function (e) {
        allow_message();
    });

    $("#btn_delete").click(function (e) {
        deleteMessage();
    });

    $("#hbtn_delete").click(function (e) {
        deleteMessage();
        $('#HoldModal').modal('hide');
    });

    $("#btn_hold").click(function (e) {
        sendMsgToHold();
    });

    $("#hbtn_allow").click(function (e) {
        hold_approve_msg();
    });

    function hold_approve_msg() {
        var message_id = current_chat_id;
        var cat = $("#hcategory").val();
        var sub_cat = 0;
        if ($("#hsubCategory").val() != null) {
            sub_cat = $("#hsubCategory").val();
        }
        var message = $("#hcurrent_message").val();
        $('#HoldModal').modal('hide');
        $('.page-loader-wrapper').fadeIn();
        $.ajax({
            type: "POST",
            async: false,
            data: {
                request: 'approve_chat',
                chat_id: message_id,
                chat: message,
                cat_id: cat,
                sub_cat_id: sub_cat,
                approved_by: "<?= $_SESSION['admin_name']?>"
            },
            url: "ajaxcall.php",
            success: function (data) {
                get_hold_msg_count();
                $('.page-loader-wrapper').fadeOut();
                swal(data);
            },
            error: function (error) {
                get_hold_msg_count();
                $('.page-loader-wrapper').fadeOut();
                swal("A Fatal error occurred!");
            }
        });
    }

    function deleteMessage() {
        var message_id = current_chat_id;
        $('#defaultModal').modal('hide');
        $.ajax({
            type: "POST",
            async: false,
            data: {
                request: 'delete_message',
                id: message_id
            },
            url: "ajaxcall.php",
            success: function (data) {
                get_unread_count_msg();
                get_unread_count_of_msg_in_lis_in_home_page();
                swal(data);
            }
        });
    }

    function allow_message() {
        var message_id = current_chat_id;
        var cat = $("#category").val();
        var sub_cat = 0;
        if ($("#subCategory").val() != null) {
            sub_cat = $("#subCategory").val();
        }
        var message = $("#current_message").val();
        $('#defaultModal').modal('hide');
        $('.page-loader-wrapper').fadeIn();
        $.ajax({
            type: "POST",
            async: false,
            data: {
                request: 'approve_chat',
                chat_id: message_id,
                chat: message,
                cat_id: cat,
                sub_cat_id: sub_cat,
                approved_by: '<?php echo $_SESSION['
                admin_name ']  ?>'
            },
            url: "ajaxcall.php",
            success: function (data) {
                // load_unapprove_chat();
                get_unread_count_msg();
                get_unread_count_of_msg_in_lis_in_home_page();
                $('.page-loader-wrapper').fadeOut();
                swal(data);
            },
            error: function (error) {
                get_unread_count_msg();
                get_unread_count_of_msg_in_lis_in_home_page();
                $('.page-loader-wrapper').fadeOut();
                swal("A Fatal error occurred!");
            }
        });
    }

    function sendMsgToHold() {
        var message_id = current_chat_id;
        $.ajax({
            type: "POST",
            async: false,
            data: {
                request: 'send_msg_to_hold',
                m_id: message_id
            },
            url: "ajaxcall.php",
            success: function (data) {
                // load_unapprove_chat();
                $('#defaultModal').modal('hide');
                get_unread_count_msg();
                get_unread_count_of_msg_in_lis_in_home_page();
                $('.page-loader-wrapper').fadeOut();
                swal(data);
            }
        });
    }
</script>
<!--SQ code end-->