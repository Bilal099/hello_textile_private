<?php
// this module is run every midnight and it checks for all the users with expired packages and revert them to silver package
include "config.php";
include_once "/home/hellzpgk/public_html/chat_api/Classes/PushNotification.php"; 
$push = new PushNotification();

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

$sql_silver_pkg = "SELECT p_id, p_name, p_no_of_msg, p_pkg_duration, p_msg_duration, p_size_of_msg
                   FROM packeages WHERE TRIM(LOWER(p_name))='silver' LIMIT 1";
                   
$silver_pkg = mysqli_fetch_assoc(mysqli_query($con, $sql_silver_pkg));

$sql = "SELECT u_id, u_fcm_id, ios, u_expiry_date FROM users
              WHERE DATEDIFF(str_to_date(u_expiry_date, '%m/%d/%Y'), NOW()) < 1";
              // package has expired: DATEDIFF(str_to_date(u_expiry_date, '%m/%d/%Y'), NOW()) < 
            
$result = mysqli_query($con, $sql);
$tokens = [];
$num_users_affected = 0;
$u_ids = [];
while ($row = mysqli_fetch_assoc($result)) {
    $u_id = $row['u_id'];
    $u_ids[] = $u_id;
    $tokens[] = [$row['u_fcm_id'], $row['ios']];
    $num_users_affected += 1;
}

$new_expiry_date = date('m/d/Y', strtotime($silver_pkg['p_pkg_duration']));
$new_renew_messages_date = date('m/d/Y', strtotime($silver_pkg['p_msg_duration']));
$silver_pkg_id = $silver_pkg['p_id'];
$u_total_msg = $silver_pkg['p_no_of_msg'];
$u_pkg_type = $silver_pkg['p_name'];

if (count($u_ids) > 0) {
    $u_ids = implode(",", $u_ids);
    $query = "UPDATE users SET u_is_paid=0, u_expiry_date='$new_expiry_date', pkg_id=$silver_pkg_id, u_total_msg=$u_total_msg, renew_msg_date='$new_renew_messages_date', u_pkg_type='$u_pkg_type' WHERE u_id in ($u_ids)";

// use $tokens to notify all the affected users
    
    if (mysqli_query($con, $query)){
        echo  "[".date('m/d/Y h:i:s a', time())."] Renewed packages for ".$num_users_affected." user(s) \n";
        $push->send_push_to_noti_panel($tokens, 'Package Expired', 'Dear user, your package has expired. Please renew your package to keep using our services.', null);
    } else {
        echo "[".date('m/d/Y h:i:s a', time())."] No users were renewed \n";
        echo $query . "\n";
        echo strval(mysqli_error($con));
    }    
}
 else {
     echo "No new expired users\n";
 }
