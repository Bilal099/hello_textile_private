<?php

class country 
{
    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
    
    public function get_country()
    {
        $msg = "";
        $stmt = $this->conn->prepare("SELECT name, nicename FROM country");
        $stmt->execute();
        $fcm_token = array();
        $stmt->bind_result($fcm_token['name'], $fcm_token['nicename']); //	nicename
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($r = $stmt->fetch())
            {
                $msg .= '<option value="'.$fcm_token['nicename'].'">'.$fcm_token['name'].'</option>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Country Not Found.", "error");</script>';
        }
        return $msg;
        $stmt->close();
    }
}


?>