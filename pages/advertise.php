<!doctype html>
<html>
<head>
<title>HelloTextiles | Advertising</title>
<style>
    tr td:first-child{
        font-weight: bold;
    }
    td {
        padding: 5px 10px;
    }
    body {
        background: url(https://subtlepatterns.com/patterns/sativa.png) repeat fixed;
        font-family: 'Open Sans', sans-serif;
        font-weight: 200;
    }
    .container{
        background-color: white;
        padding: 1em;
        border-radius: 5px;
        box-shadow: 0px 0px 2px 0px #0000008a;
        width: 50%;
        margin: 6em auto;
    }
</style>
</head>
<body>
<div class="container">
<h2 style="text-align:center;">Hello Textiles</h2>
<hr>
<?php 

$logged_in = false;
$error = false;
$id = $_POST['id'];
$pwd = $_POST['pwd'];
$ad = null;
if(isset($id) && isset($pwd)) {
    include "../include/config.php";

    $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
    $result = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM video_ads WHERE advertiser_id='$id' AND advertiser_pwd='$pwd'"));
    if ($result) {
        $ad = $result;
        $logged_in = true;
    } else {
        $error = true;
    }
}
?>

<?php if($logged_in && $ad): ?>
    
     <h3 style="text-align:center;">Ad stats</h3>
    <table>
        <tbody>
            <tr>
                <td>Ad Id</td>
                <td><?=$id?></td>
            </tr>
            <tr>
                <td>Views</td>
                <td><? echo $ad['num_views'] - $ad['views_left'];?></td>
            </tr>
            <tr>
                <td>Clicks</td>
                <td><? echo $ad['clicks'];?></td>
            </tr>
            <tr>
                <td>Is Live:</td>
                <td><? echo $ad['is_live'] ? "Yes" : "No";?></td>
            </tr>
            <tr>
                <td>Expires On:</td>
                <td><? echo date('d-m-Y', strtotime($ad['created_on'] . " + " . $ad['duration_num'] . " " . $ad['duration_type']));?></td>
            </tr>
            <?php if($ad['ad_link']): ?>
            <tr>
                <td>Ad Link</td>
                <td><a href="<?=trim($ad['ad_link'])?>" target="_blank"><?=trim($ad['ad_link'])?></a></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
        

<? else: ?>
  <div class="login">
      <h3 style="text-align:center;">Login to see your Ad stats</h3>
      <form action="advertise.php" method="POST">
          <div>
              <p>Ad Id:</p>
              <input type="text" name="id" />
          </div>
          <div>
              <p>Password:</p>
              <input type="password" name="pwd" />
          </div>
          <div style="margin-top:1em;">
              <input type="submit" value="Submit" />
          </div>
      </form>
      <?php if($error): ?>
        <p style="color:red;">Invalid id or password. Please try again.</p>
      <?php endif; ?>
  </div>
<? endif; ?>

</div>
</body>
</html>