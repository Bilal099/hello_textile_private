<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 1/4/2019
 * Time: 11:50 AM
 */

class packages
{
    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    function add_package($params)
    {
        $result = mysqli_query($this->conn, "SELECT e_rate FROM packeages LIMIT 1");
        $result = mysqli_fetch_assoc($result);
        $e_rate = $result['e_rate'];
        $stmt = $this->conn->prepare("INSERT INTO packeages(p_name, p_no_of_msg, p_size_of_msg, p_pkg_duration, p_msg_duration, p_data_backup_duration, p_access_of_full_profile, p_price_promotion, p_advertisement, p_advertisement_duration, p_advertisement_watch_time, p_advertisement_time_a_day, p_price, p_crncy, p_discount, p_price_after_discount, p_for_user, p_visible, e_rate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, '$e_rate')");
        $stmt->bind_param("ssssssssssssssssss", $params['p_name'],$params['p_no_of_msg'],$params['size_of_msg'],$params['p_pkg_duration'],$params['p_msg_duration'],$params['p_data_backup_duration'],$params['p_access_of_full_profile'],$params['p_price_promotion'],$params['p_advertisement'],$params['p_advertisement_duration'],$params['p_advertisement_watch_time'],$params['p_advertisement_time_a_day'],$params['p_price'],$params['p_crncy'],$params['p_discount'],$params['p_price_after_discount'],$params['p_for_user'],$params['p_visible']);
        if ($stmt->execute())
            $msg = '<script>swal("Good Job!", "Package Create successfully.", "success");</script>';
        else
            $msg = '<script>swal("Oops!","Unable to create package", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function get_all_pakages()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT p_id, p_name, p_no_of_msg, p_pkg_duration,p_price,p_crncy,p_advertisement, is_paid FROM packeages");
        $stmt->execute();
        $stmt->bind_result($param['p_id'],$param['p_name'],$param['p_no_of_msg'],$param['p_pkg_duration'],$param['p_price'],$param['p_crncy'],$param['p_advertisement'], $param['is_paid']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                if($param['p_advertisement']==0){
                    $p_advertisement = '<span class="label label-danger">Not Allowed</span>';
                } else{
                    $p_advertisement = '<span class="label label-success">Allowed</span>';
                }
                if ($param['is_paid']) {
                    $p_is_paid = '<span class="label label-success">Yes</span>';
                } else {
                    $p_is_paid = '<span class="label label-danger">No</span>';
                }
                
                $msg .= '<tr>
                        <td>'.$param['p_name'].'</td>
                        <td>'.$param['p_no_of_msg'].'</td>
                        <td>'.$param['p_pkg_duration'].'</td>
                        <td>'.$param['p_crncy']." ".$param['p_price'].'</td>
                        <td>'.$p_advertisement.'</td>
                        <td>'.$p_is_paid.'</td>
                        <td>
                            <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_package?id='.$param['p_id'].'"><i class="material-icons">edit</i></a>
                            <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple btn_delete" data-package-id ="'.$param['p_id'].'"><i class="material-icons">delete</i></a>
                        </td>
                    </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Packages Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    function get_single_pakage($pakage_id){
        $result = mysqli_query($this->conn, "SELECT * FROM packeages where p_id = $pakage_id");
        $result = mysqli_fetch_assoc($result);
        return $result;
    }

    function update_package($params){
        $stmt = $this->conn->prepare("UPDATE packeages set p_name=?, p_no_of_msg=?, p_size_of_msg=? , p_pkg_duration=?, p_msg_duration=?, p_data_backup_duration=?, p_access_of_full_profile=?, p_price_promotion=?, p_advertisement=?, p_advertisement_duration=?, p_advertisement_watch_time=?, p_advertisement_time_a_day=?, p_price=?, p_crncy=?, p_discount=?, p_price_after_discount=?, p_for_user=?, p_visible=?, is_paid=? WHERE p_id=? ");
        $stmt->bind_param("sssssssssssssssssiii", $params['p_name'],$params['p_no_of_msg'], $params['p_size_of_msg'], $params['p_pkg_duration'],$params['p_msg_duration'],$params['p_data_backup_duration'],$params['p_access_of_full_profile'],$params['p_price_promotion'],$params['p_advertisement'],$params['p_advertisement_duration'],$params['p_advertisement_watch_time'],$params['p_advertisement_time_a_day'],$params['p_price'],$params['p_crncy'],$params['p_discount'],$params['p_price_after_discount'],$params['p_for_user'], $params['p_visible'], $params['is_paid'], $params['p_id']);
        if ($stmt->execute())
            $msg = $params;
        else
            $msg = null;
        $stmt->close();
        return $msg;
    }

    function delete_package($pakage_id){
        $stmt = $this->conn->prepare("DELETE FROM packeages  where p_id=? ");
        $stmt->bind_param("i", $pakage_id);
        if ($stmt->execute())
            $msg = '<script>swal("Good Job!", "Package Removed successfully.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to Remove Package.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function delete_package_request($pr_id){
        $stmt = $this->conn->prepare("DELETE FROM package_request  where pr_id=? ");
        $stmt->bind_param("i", $pr_id);
        if ($stmt->execute())
            $msg = '<script>swal("Good Job!", "Package Request Removed successfully.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to Remove Package.", "error");</script>';
        $stmt->close();
        return $msg;
    }
    function load_num_package_request() {
        $count = 0;
        $stmt = $this->conn->prepare("SELECT COUNT(*) as num_p_request FROM package_request WHERE pr_status=0");
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->store_result();
        $stmt->fetch();
        if ($count > 0) {
            return "<span style='color: white;font-weight: bold;padding: 0 7px;background-color: red;border-radius: 4px;'>$count Requests</span>";
        }
        return "";
    }
    function load_package_request()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT pr_id, pr.pkg_id, pr.p_user_id, u.u_name, u.u_email, u.u_contact, p.p_name, pr.pr_date FROM package_request pr, users u, packeages p WHERE u.u_id = pr.p_user_id AND p.p_id = pr.pkg_id AND pr.pr_status = 0");
        $stmt->execute();
        $stmt->bind_result($param['pr_id'] , $param['pkg_id'] , $param['p_user_id'] , $param['u_name'] , $param['u_email'], $param['u_contact'], $param['p_name'] , $param['pr_date']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<tr>
                        <td>'.$param['u_name'].'</td>
                        <td>'.$param['p_name'].'</td>
                        <td>'.$param['pr_date'].'</td>
                        <td>'.$param['u_email'].'</td>
                        <td>'.$param['u_contact'].'</td>
                        <td>
                            <button value="'.$param['pr_id'].'" data-pkgID="'.$param['pkg_id'].'" data-userID="'.$param['p_user_id'].'" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float pkg_ApproveBtn">
                                <i class="material-icons">done</i>
                            </button>
                        </td>
                        <td>
                            <button value="'.$param['pr_id'].'" data-pkgID="'.$param['pkg_id'].'" data-userID="'.$param['p_user_id'].'" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float pkg_deletBtn">
                                <i class="material-icons">delete</i>
                            </button>
                        </td>
                    </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Packages request Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    private function update_user_package_request_status($pid)
    {
        $stmt = $this->conn->prepare("UPDATE package_request SET pr_status = 1 WHERE pr_id = ?");
        $stmt->bind_param("i", $pid);
        if ($stmt->execute())
            return true;
        else
            return false;
        $stmt->close();
    }

    function select_packages_name()
    {
        $msg = "";
        $stmt = $this->conn->prepare("SELECT p_id, p_name,p_for_user FROM packeages");
        $stmt->execute();
        $pkh_arr = array();
        $stmt->bind_result($pkh_arr['p_id'],$pkh_arr['p_name'],$pkh_arr['p_for_user']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<option value="'.$pkh_arr['p_id'].'">'.$pkh_arr['p_name']. ' (' . ($pkh_arr['p_for_user'] == 0 ? 'Pakistani' : 'International') .')</option>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Packages Not Found.", "error");</script>';
        }
        return $msg;
        $stmt->close();
    }
    
    function update_e_rate($e_rate) {
        if (mysqli_query($this->conn, "UPDATE packeages SET e_rate='$e_rate'")) {
            return json_encode(["success"=>true]);
        }
        return json_encode(["success"=>false]);
    }
    
    function get_e_rate() {
        $result = mysqli_query($this->conn, "SELECT e_rate FROM packeages LIMIT 1");
        $result = mysqli_fetch_assoc($result);
        return json_encode(["success"=> true, "e_rate"=> $result["e_rate"]]);
    } 

    function assign_package_user($param)
    {
        global $push, $user;
        $token = $user->get_user_token_by_id($param['u_id']);

       //get the package detail by id
        $pdetail = $this->get_single_pakage($param['pid']);

        //calculate the user package expiry
        $Pexpiry = strtotime("+".$pdetail['p_pkg_duration']);
        $PexpiryFinal = date("m/d/Y", $Pexpiry);

        //calculate the msg expiry
        $msg_Expiry = strtotime("+".$pdetail['p_msg_duration']);
        $msg_ExpiryFinal = date("m/d/Y", $msg_Expiry);

        $stmt = $this->conn->prepare("UPDATE users SET u_expiry_date = ?, renew_msg_date = ?, u_pkg_type = ?, u_total_msg = ?, size_of_msg = ? , pkg_id = ?, u_is_paid = ? WHERE u_id = '".$param['u_id']."' ");
        $stmt->bind_param("sssiiii", $PexpiryFinal, $msg_ExpiryFinal, $pdetail['p_name'], $pdetail['p_no_of_msg'], $pdetail['p_size_of_msg'],$pdetail['p_id'], $pdetail['is_paid']);
        if ($stmt->execute())
        {
            if($this->update_user_package_request_status($param['prID']))
            {
                $msg = '<script>swal("Good Job!", "Package Assign successfully.", "success");</script>';
                $push->send($token,"Package","Package Request has been approved","");
            }
            else
            {
                $msg = '<script>swal("Oops!", "Unable to approve user request.", "error");</script>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Unable to Updated Assign Package.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }


    function assign_package_user_by_admin($param)
    {
        global $push, $user;

        //get the package detail by id  pkg_id
        $pdetail = $this->get_single_pakage($param['pid']);

        //calculate the user package expiry
        $Pexpiry = strtotime("+".$pdetail['p_pkg_duration']);
        $PexpiryFinal = date("m/d/Y", $Pexpiry);

        //calculate the msg expiry
        $msg_Expiry = strtotime("+".$pdetail['p_msg_duration']);
        $msg_ExpiryFinal = date("m/d/Y", $msg_Expiry);
        
        $error = false;
        foreach($param['u_id'] as $u_id) {
            $token = $user->get_user_token_by_id($u_id);   
            $stmt = $this->conn->prepare("UPDATE users SET u_expiry_date = ?, renew_msg_date = ?, u_pkg_type = ?, u_total_msg = ?, size_of_msg = ? , pkg_id = ?, u_is_paid = ? WHERE u_id = '".$u_id."' ");
            $stmt->bind_param("sssiiii", $PexpiryFinal, $msg_ExpiryFinal, $pdetail['p_name'], $pdetail['p_no_of_msg'], $pdetail['p_size_of_msg'],$pdetail['p_id'], $pdetail['is_paid']);
            if ($stmt->execute())
            {
                $push->send($token,"Package","You have been assigned ".$pdetail['p_name']." By Admin","");
            }
            else {
                $error = true;
            }
            $stmt->close();
        }

        if (!$error)
        {
            $msg = '<script>swal("Good Job!", "Package Assign successfully.", "success");</script>';
        }
        else
        {
            $msg = '<script>swal("Oops!", "There was an error while assigning package to one or more users.", "error");</script>';
        }
        
        return $msg;
    }

    function update_AllUser_PackageDetail_on_timeBase()
    {
        global $user;
        $userDetail_arr = $user->get_user_pkgExpiry_renew();
        foreach ($userDetail_arr as $u)
        {
            $expiry = strtotime($u['u_expiry_date']);
            $renew = strtotime($u['renew_msg_date']);
            $today = strtotime(date('m/d/Y'));

            if($renew<$expiry && $today<$expiry && $renew<=$today)
            {
                $pkg_deatil_arr = $this->get_single_pakage($u['pkg_id']);
                $msg_duaration = $pkg_deatil_arr['p_msg_duration'];
                $new_renew = strtotime($u['renew_msg_date']."+".$msg_duaration);
                $new_renew1 = date("m/d/Y", $new_renew);
                $daysCount = round(($expiry - $new_renew) / (60 * 60 * 24));
                $pkgDays  = round(($new_renew - $renew) / (60 * 60 * 24));
                if($daysCount>$pkgDays)
                {
                    $stmt = $this->conn->prepare("UPDATE users SET renew_msg_date = ?, u_total_msg = ? WHERE u_id = '".$u['u_id']."' ");
                    $stmt->bind_param("si", $new_renew1, $pkg_deatil_arr['p_no_of_msg']);
                    $stmt->execute();
                }
                else
                {
                    $new_renew = strtotime($u['renew_msg_date']."+".$pkgDays." Days");
                    $new_renew1 = date("m/d/Y", $new_renew);

                    $stmt = $this->conn->prepare("UPDATE users SET renew_msg_date = ?, u_total_msg = ? WHERE u_id = '".$u['u_id']."' ");
                    $stmt->bind_param("si", $new_renew1, $pkg_deatil_arr['p_no_of_msg']);
                    $stmt->execute();
                }


                //$d  = $pkgDays;
            }
        }

//      return $d;
    }
    function get_all_orders() {
        $sql = "SELECT 
                    po._id, po.amount, po.type, po.status_desc, CONVERT_TZ(po.created_at, '-04:00', '+05:00') as created_at, po.mobile, po.transaction_status, po.p_assigned,
                    u.u_name, p.p_name
                FROM package_orders po, users u, packeages p
                WHERE po.u_id=u.u_id && po.p_id=p.p_id
                ORDER BY po._id ASC";
        $result = mysqli_query($this->conn, $sql);
        $order = null;
        $msg = "";
        if ($result) {
            
            while($order = mysqli_fetch_assoc($result)) {
                $i = $order['_id'];
                $u = $order['u_name'];
                $pn = $order['p_name'];
                $a = $order['amount'];
                $t = $order['type'];
                $m = $order['mobile'];
                $s = $order['transaction_status'];
                $pa = $order['p_assigned'];
                $c = $order['created_at'];
                $sd = $order['status_desc'];
                $sd = $sd == "SUCCESS" ? "": ucfirst(strtolower($sd));
                
                $type_cell = $t == 'ma' ? "<td><span title='Mobile Account'>MA</span> ($m)</td>" : "<td title='Credit Card'>CC</td>";
                $pa = $pa == 1 ? "Yes" : "No";
                $msg .= "<tr>
                    <td>$i</td>
                    <td>$u</td>
                    <td>$pn</td>
                    <td>$a</td>
                    $type_cell
                    <td>$s</td>
                    <td>$pa</td>
                    <td>$c</td>
                    <td>$sd</td>
                </tr>\n";
            }   
        } else {
            $msg = json_encode(mysqli_error($this->conn));
        }
        return $msg;
    }
}