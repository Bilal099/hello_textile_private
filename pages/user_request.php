<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

include_once "../include/header.php";
?>

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Users Request
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>City</th>
                            <th>Company</th>
                            <th>Status</th>
                            <th>Approve</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>City</th>
                            <th>Company</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <tbody>
<!--                        <tr>-->
<!--                            <td>Tiger Nixon</td>-->
<!--                            <td>System Architect</td>-->
<!--                            <td>Edinburgh</td>-->
<!--                            <td>61</td>-->
<!--                            <td>2011/04/25</td>-->
<!--                            <td><span class="label label-danger">In-Active</span></td>-->
<!--                            <td>-->
<!--                                <button type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float">-->
<!--                                    <i class="material-icons">done</i>-->
<!--                                </button>-->
<!--                            </td>-->
<!--                        </tr>-->
                        <?php echo $user->get_requested_user();?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

<?php
include_once "../include/footer.php"
?>
