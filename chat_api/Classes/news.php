<?php
/**
 * Created by DK_KHAN.
 * User: DK_KHAN
 * Date: 10/24/2018
 * Time: 12:06 PM
 */

class news
{
    function get_news($u_country = null)
    {
        global $DB;
        $country_filter = isset($u_country) ? " WHERE n.n_countries is NULL OR FIND_IN_SET('$u_country', n.n_countries) > 0 " : "";
        $sql = "SELECT n.n_id, n.n_title, n.n_desc, n.n_date FROM news n $country_filter ORDER BY n.n_id DESC";
        $res = $DB->qr($sql); 
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = [
                    'n_id'      => $r['n_id'],
                    'n_title'   => $r['n_title'],
                    'n_desc'    => $r['n_desc'],
                    'n_date'    => $r['n_date']
                ];
            }
        }
        return json_encode($data);
    }

    function get_Notification($country = null)
    {
        global $DB;
        $country_filter = isset($country) ? " WHERE n_country is NULL or FIND_IN_SET(n_country, '$country') > 0 " : "";
        $sql = "SELECT n_id, n_title, n_msg, n_date FROM notification $country_filter ORDER BY n_id DESC";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = [
                    'n_id'      => $r['n_id'],
                    'n_title'   => $r['n_title'],
                    'n_msg'     => $r['n_msg'],
                    'n_date'    => $r['n_date']
                ];
            }
        }
        return json_encode($data);
    }
}