<?php
/**
 * Created by Arsalan.
 * User: Arsalan
 * Date: 11/8/2018
 * Time: 04:24 PM
 */

class advertisment
{
    function get_ads($u_country = null)
    {
        $data = null;
        global $DB;
        $country_filter = isset($u_country) ? " WHERE countries is NULL OR FIND_IN_SET('$u_country', countries) > 0 " : "";
        $sql = "SELECT * FROM Advertisment $country_filter ORDER BY rand()";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = [
                    'ad_id'             =>  $r['ad_id'],
                    'ad_image'          =>  image_view_path.$r['ad_image'],
                    'ad_display_time'   =>  $r['ad_display_time'],
                    'created_at'        =>  $r['created_at']
                ];
            }
        }
        return json_encode($data);
    }

    function update_ads_Watch_time($ads_id)
    {
        global $DB;
        $sql = "UPDATE Advertisment SET a_watchtime_perday = a_watchtime_perday-1 WHERE ad_id = '$ads_id' ";
        if($DB->qr($sql))
        {
            $data = array(
                'success'   =>  true,
                'message'   =>  'Updated'
            );
        }
        else
        {
            $data = array(
                'success'   =>  false,
                'message'   =>  'Unable to update Ads watch time.'
            );
        }
        return json_encode($data);
    }

    function update_per_Day()
    {
        global $DB;
        $sql = "UPDATE Advertisment SET  a_watchtime_perday = a_watchtime_default ";
        if($DB->qr($sql))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function fetch_video_ad($u_id) {
        global $DB;
        $user = $DB->fa($DB->qr("SELECT u_country FROM users WHERE u_id=$u_id"));
        $country_filter = '';
        if ($user) {
            $country_filter = "AND (view_countries='' OR view_countries LIKE '%" . $user['u_country'] . "%')";
        }
        $ad = $DB->fa($DB->qr("SELECT ad_link, advertiser_id, video_length, CONCAT('https://hellotextiles.com/upload/', ad_file) as ad_file, views_left / num_views as dist 
                                FROM video_ads
                                WHERE is_live=true AND views_left > 0 $country_filter
                                ORDER BY dist DESC
                                LIMIT 1"));
        if ($ad) {
            return json_encode([
                "success" => true,
                "ad_id" => $ad['advertiser_id'],
                "ad_link" => $ad['ad_link'],
                "ad_video_link" => $ad['ad_file'],
                "ad_video_length" => intval($ad['video_length']),
            ]);
        }
        return json_encode([
                "success" => false,
                "message" => "No ads available."
        ]);
    }
    
    function update_video_ad_link_click($u_id, $ad_id) {
        global $DB;
        $ad = $DB->fa($DB->qr("SELECT num_views, clicks FROM video_ads WHERE advertiser_id='$ad_id' AND is_live=true"));
        if ($ad) {
            if ($ad['clicks'] < $ad['num_views']) {
                $DB->qr("UPDATE video_ads SET clicks = clicks + 1 WHERE advertiser_id='$ad_id'");   
            }
            return json_encode(["success" => true]);   
        }
        return json_encode(["success" => false]);
    }
    
    function update_ad_view($u_id, $ad_id) {
        global $DB;
        $ad = null;
        if (isset($ad_id)) {
            $ad_id = trim($ad_id);
            $ad = $DB->fa($DB->qr("SELECT num_views, views_left FROM video_ads WHERE advertiser_id='$ad_id' AND is_live=true"));
        }
        $user = $DB->fa($DB->qr("SELECT u_id FROM users WHERE u_id=$u_id"));
        if ($ad && $user) {
            if ($ad['views_left'] > 0) {
                $DB->qr("UPDATE video_ads SET views_left = views_left - 1 WHERE advertiser_id='$ad_id'");
                if ($ad['views_left'] == 1) {
                    $DB->qr("UPDATE video_ads SET is_live=false WHERE advertiser_id='$ad_id'");
                }
            }
        } 
        if ($user) {
            if ($DB->qr("UPDATE users SET u_total_msg = u_total_msg + 2 WHERE u_id=$u_id")) {

                $date = date("Y-m-d H:i:s");
                $query1 = "SELECT * FROM user_ads_view WHERE user_id = '$u_id' AND ads_id='$ad_id'";
                $user1 = $DB->fa($DB->qr($query1));
                if ($user1!=null) 
                {
                    $query2 = "UPDATE user_ads_view SET ads_count=ads_count+1, updated_at='$date' WHERE user_id='$u_id' AND ads_id='$ad_id'";
                } 
                else 
                {
                    $query2 = "INSERT INTO user_ads_view(ads_id, user_id, ads_count, created_at) VALUES ('$ad_id','$u_id',1,'$date')";    
                }
                if($DB->qr($query2))
                {
                    return json_encode(["success" => true, "message" => "You can now send two more private messages."]);
                }
                else
                {
                    return json_encode(["success" => false, "message" => "Something went wrong"]);
                }
                // return json_encode(["success" => true, "message" => "You can now send two more private messages."]);
            }        
        }
        return json_encode(["success" => false, "message" => "Something went wrong"]);
    }
}