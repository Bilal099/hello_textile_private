<?php
include_once "../include/header.php";

if(isset($_POST['submit']))
{
//    $d= strtotime("+"."3 Month");
//    echo date("m/d/Y", $d) . "<br>";
    
    $param = array(
        'p_name'                        =>  $_POST['pgk_name'],
        'p_no_of_msg'                   =>  $_POST['pgk_no_of_msg'],
        'size_of_msg'                   =>  $_POST['size_of_msg'],
        'p_pkg_duration'                =>  $_POST['pgk_duration_count'] . " " . $_POST['pgk_duration'],
        'p_msg_duration'                =>  $_POST['pgk_msg_duration_count'] . " " . $_POST['pgk_msg_duration'],
        'p_data_backup_duration'        =>  $_POST['pgk_data_backup_duration_count'] . " " . $_POST['pgk_data_backup_duration'],
        'p_access_of_full_profile'      =>  $_POST['pgk_profile_access'],
        'p_price_promotion'             =>  $_POST['pgk_price_promotion'],
        'p_advertisement'               =>  $_POST['pgk_advertisement'],
        'p_advertisement_duration'      =>  ($_POST['pgk_advertisement_duration'] == "No" ? "No" : $_POST['pgk_advertisement_duration_count'] . " " . $_POST['pgk_advertisement_duration']),
        'p_advertisement_watch_time'    =>  ($_POST['pgk_advertisement_watch_time_count'] == "" ? "0" : $_POST['pgk_advertisement_watch_time_count']),
        'p_advertisement_time_a_day'    =>  "0",
        'p_price'                       =>  $_POST['pgk_price'],
        'p_crncy'                       =>  $_POST['pgk_currency'],
        'p_discount'                    =>  $_POST['pgk_discount'],
        'p_price_after_discount'        =>  $_POST['pgk_after_discount_price'],
        'p_for_user'                    =>  $_POST['pkg_for'],
        'p_visible'                     =>  $_POST['pgk_visible']
    );
    echo $package->add_package($param);
}

?>

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Create Package</h2>
                </div>
                <div class="body">
                    <form id="form_advanced_validation" method="POST" action="">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="pgk_name" required>
                                <label class="form-label">Package Name</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control" name="pgk_no_of_msg" required>
                                <label class="form-label">No. of Messages</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" class="form-control" name="size_of_msg" required>
                                <label class="form-label">Size of Message</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select Package Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" min="1" value="1" name="pgk_duration_count" required>
                                <select name="pgk_duration">
                                    <option value="Month">Month</option>
                                    <option value="Year">Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select Message Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" min="1" value="3" name="pgk_msg_duration_count" required>
                                <select name="pgk_msg_duration">
                                    <option value="Day">Day</option>
                                    <option value="Week">Week</option>
                                    <option value="Month">Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select Data Backup Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" min="1" value="3" name="pgk_data_backup_duration_count" required>
                                <select name="pgk_data_backup_duration">
                                    <option value="Day">Day</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Visible</b>
                                </p>
                                <select name="pgk_visible" class="form-control show-tick">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Full Profile Access of  all users</b>
                                </p>
                                <select name="pgk_profile_access" class="form-control show-tick">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Price Promotion</b>
                                </p>
                                <select name="pgk_price_promotion" class="form-control show-tick">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Advertisement</b>
                                </p>
                                <select name="pgk_advertisement" class="form-control show-tick">
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Advertisement Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" id="pgk_advertisement_duration_count" name="pgk_advertisement_duration_count">
                                <select name="pgk_advertisement_duration" id="pgk_advertisement_duration">
                                    <option value="No">No</option>
                                    <option value="Day">Day</option>
                                    <option value="Week">Week</option>
                                    <option value="Month">Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Advertisement Watch Time (Seconds)</b>
                                </p>
                                <input type="number" placeholder="Enter a number" class="form-control" name="pgk_advertisement_watch_time_count">
                            </div>
                        </div>
                        <!--<div class="form-group form-float">-->
                        <!--    <p>-->
                        <!--        <b>Advertisement Times a day</b>-->
                        <!--    </p>-->
                        <!--    <select name="pgk_advertisement_time_a_day" class="form-control show-tick">-->
                        <!--        <option value="0">Select</option>-->
                        <!--        <option value="2">2 Times</option>-->
                        <!--        <option value="4">4 Times</option>-->
                        <!--        <option value="8">8 Times</option>-->
                        <!--        <option value="10">10 Times</option>-->
                        <!--    </select>-->
                        <!--</div>-->
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" name="pgk_price" id="pgk_price" required>
                                    <label class="form-label">Package Price</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select Currency</b>
                                </p>
                                <select name="pgk_currency" class="form-control show-tick">
                                    <option value="Rs">Rs</option>
                                    <option value="$">$</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" name="pgk_discount" id="pgk_discount">
                                    <label class="form-label">Package Discount</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Package Price after Discount</b>
                                </p>
                                <input type="number" name="pgk_after_discount_price" value="0" id="pgk_after_discount_price">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <p>
                                <b>Package For</b>
                            </p>
                            <select name="pkg_for" class="form-control show-tick" required>
                                <option value="0">Pakistani Users</option>
                                <option value="1">International Users</option>
                            </select>
                        </div>

                        <button class="btn btn-primary waves-effect" type="submit" name="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
    
    // Toggle pgk_advertisement_duration_count based on pgk_advertisement_duration
        var pgk_ad_duration_count = document.getElementById('pgk_advertisement_duration_count');
        var pgk_ad_duration = document.getElementById('pgk_advertisement_duration');
        pgk_ad_duration_count.style.display="none";
        pgk_ad_duration.onchange = function() {
            if (pgk_ad_duration.value == "No") {
                pgk_ad_duration_count.style.display="none";        
            } else {
                pgk_ad_duration_count.style.display="inline-block";
            }
        };
        
     // Calculate and populate price after discount
        var pgk_price = document.getElementById('pgk_price');
        var pgk_discount = document.getElementById('pgk_discount');
        var pgk_after_discount_price = document.getElementById('pgk_after_discount_price');
        
        function updatePriceAfterDiscount () {
            pgk_after_discount_price.value = Number(pgk_price.value) - Number(pgk_discount.value);
        }
        pgk_price.onkeyup = updatePriceAfterDiscount;
        pgk_discount.onkeyup = updatePriceAfterDiscount;
        
    </script>

<?php
include_once "../include/footer.php";
?>