<?php
/**
 * Created by DK_KHAN.
 * User: DK_KHAN
 * Date: 10/4/2018
 * Time: 1:22 PM
 */

include_once "mysql.php";
$DB = new mysql_functions();


include_once "Classes/api_category.php";
$cat  = new apiCategory();

include_once "Classes/users.php";
$users = new users();

include_once "Classes/PushNotification.php";
$push = new PushNotification();

include_once "Classes/Massenger.php";
$massenger = new Massenger();

include_once "Classes/news.php";
$news = new news();

include_once "Classes/Advertisement.php";
$ads = new advertisment();

include_once "Classes/private_chat_users.php";
$pcu = new pc_users();

include_once "Classes/package.php";
$pkg = new package();

include_once "Classes/blogs.php";
$blogs = new blogs();