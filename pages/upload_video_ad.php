<?php
session_start();

if (!isset($_SESSION['admin_email'])) {
    exit(1);
}

function upload_file($file, $file_name_prefix) 
{
    $file_name_suffix = "_" . strval(time()) . "_" . str_replace(" ", "", basename($file['name']));
    $filename = $file_name_prefix . $file_name_suffix;
    if (!move_uploaded_file($file["tmp_name"], '../upload/' . $filename)) {
        $filename = null;    
    }
    return $filename;
}

function is_file_valid($file)
{
    $valid_extensions = ['video/mp4', 'video/x-m4v'];
    if (!in_array($file['type'], $valid_extensions)) {
        return 2; // invalid image type
    }
    if ($file['size'] > 20000000) {
        return 3; // size greater than 20 MB
    }
    if ($file['error']) {
        return 4; // upload error
    }
    return 1;
}

$error = null;
if (isset($_FILES['ad_file'])) {
    $is_valid = is_file_valid($_FILES['ad_file']);
    if ($is_valid == 1) {
        $filename = upload_file($_FILES['ad_file'], "ad");
        if ($filename) {
            $response = ["success" => true, "ad_filename" => $filename];
            echo json_encode($response);
            exit();
        } else {
            $error = "An error occurred. Please try again.";
        }
    }
    else if ($is_valid == 2) {
        $error = "File must be a mp4 video.";
    }
    else if ($is_valid == 3) {
        $error = "File size more than 20MB is not allowed.";
    }
}

if ($error == null) {
    $error = "An error occurred. Please try again.";
}

http_response_code(500);
echo json_encode(["success" => false, "error" => $error]);

?>