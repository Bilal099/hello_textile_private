<?php
/**
 * Created by DK_KHAN.
 * User: DK_KHAN
 * Date: 10/1/2018
 * Time: 1:50 PM
 */

class category
{
    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    function add_category($name,$sub_cat,$status)
    {
        // $target_dir = image_upload_path;
        // $target_file_name = basename($file["name"]);
        // $target_file = $target_dir . $target_file_name;
        // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // // Allow certain file formats
        // if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg")
        // {
        // $msg = '<script>swal("Oops!", "Invalid Image Format.", "error");</script>';
        // }
        // else
        // {
        // if ($file["size"] > 100000) {
        // $msg = '<script>swal("Oops!", "Image should less then 100 KB.", "error");</script>';
        // }
        // else
        // {
        // if (move_uploaded_file($file["tmp_name"], $target_file))
        // {
        $stmt = $this->conn->prepare("INSERT INTO category(c_name, sub_cat, c_status) VALUES (?,?,?)");
        $stmt->bind_param("ssi",$name,$sub_cat,$status);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Category add successfuly.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to add category.", "error");</script>';
        $stmt->close();
        // }
        // else
        // {
        // $msg = '<script>swal("Oops!", "please try again.", "error");</script>';
        // }
        // }
        // }


        return $msg;
    }


    function update_category_filed($c_id,$name,$sub_cat,$status)
    {
        $stmt = $this->conn->prepare("UPDATE category SET c_name = ?, sub_cat = ?, c_status = ? WHERE  c_id = ?");
        $stmt->bind_param("siii",$name,$sub_cat,$status,$c_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Sub category update successful.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to update sub category.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function update_sub_category_filed($sc_id,$sc_name,$sc_category, $sc_status)
    {
        $stmt = $this->conn->prepare("UPDATE sub_category SET c_id = ?, sc_name = ?, sc_status = ? WHERE sc_id = ? ");
        $stmt->bind_param("isii",$sc_category,$sc_name,$sc_status,$sc_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Sub-Category update successful.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to update category.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function get_all_category()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT c_id, c_name, c_icon, sub_cat ,c_status FROM category order by c_id desc ");
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['name'],$param['icon'],$param['sub_cat'],$param['c_status']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                if($param['c_status']==0)
                    $param['res_status'] = '<span class="label label-danger">In-Active</span>';
                else
                    $param['res_status'] = '<span class="label label-success">Active</span>';
                if($param['sub_cat'] == 0 )
                    $param['res_sub_cat'] = '<span class="label label-danger">No</span>'; else $param['res_sub_cat'] = '<span class="label label-success">Yes</span>';
                $msg .= '<tr>
                            <td>'.$param['name'].'</td>
                           
                            <td>'.$param['res_sub_cat'].'</td>
                            <td>'.$param['res_status'].'</td>
                            <td>
                                <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_category?id='.$param['id'].'"><i class="material-icons">edit</i></a>
                            </td>
                        </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Category Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    function get_category_dropdown($selected_cat)
    {

        $options = "<option value='0'>Select</option>";
        $param = array();
        $stmt = $this->conn->prepare("SELECT c_id, c_name FROM category where c_status = 1 ");
        $stmt->execute();
        $stmt->bind_result($param['c_id'],$param['c_name']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $selected = "";
                if($selected_cat==$param['c_id']){
                    $selected ="selected";
                }
                $options.="<option value='".$param['c_id']."' ".$selected.">".$param['c_name']."</option>";
            }
        }
        $stmt->close();
        return $options;
    }

    function get_category_by_id($c_id)
    {
        $stmt = $this->conn->prepare("SELECT c_name, c_icon, sub_cat, c_status FROM category WHERE c_id = ?");
        $stmt->bind_param("s",$c_id);
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['name'],$cat_arr['icon'],$cat_arr['sub_cat'],$cat_arr['status']);
        $stmt->store_result();
        $stmt->fetch();
        return $cat_arr;
        $stmt->close();
    }

    function get_sub_category_by_id($sc_id)
    {
        $stmt = $this->conn->prepare("SELECT sc.sc_name, c.c_name, sc.sc_status, sc.c_id FROM sub_category sc inner join category c on c.c_id=sc.c_id WHERE  sc.sc_id = ?");
        $stmt->bind_param("s",$sc_id);
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['sc_name'],$cat_arr['c_name'],$cat_arr['sc_status'],$cat_arr['c_id']);
        $stmt->store_result();
        $stmt->fetch();
        return $cat_arr;
        $stmt->close();
    }

    function delete_category($c_id)
    {
        $stmt = $this->conn->prepare("DELETE FROM category WHERE c_id = ?");
        $stmt->bind_param("i",$c_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Category Delete successfuly.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to Delete category.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function delete_sub_category($sc_id)
    {
        $stmt = $this->conn->prepare("DELETE FROM sub_category WHERE sc_id = ?");
        $stmt->bind_param("i",$sc_id);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Sub category Delete successfuly.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to Delete sub category.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function select_category_name()
    {
        $msg = "";
        $stmt = $this->conn->prepare("SELECT c_id, c_name FROM category where sub_cat = 1 and c_status = 1");
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['id'],$cat_arr['name']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<option value="'.$cat_arr['id'].'">'.$cat_arr['name'].'</option>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Category Not Found.", "error");</script>';
        }
        return $msg;
        $stmt->close();
    }

    function select_category()
    {
        $msg = "";
        $stmt = $this->conn->prepare("SELECT c_id, c_name FROM category where c_status = 1");
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['id'],$cat_arr['name']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $msg .= '<option value="'.$cat_arr['id'].'">'.$cat_arr['name'].'</option>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Category Not Found.", "error");</script>';
        }
        return $msg;
        $stmt->close();
    }

    function select_sub_category_name_by_id($cat_id)
    {
        $msg = "";
        $stmt = $this->conn->prepare("SELECT sc.sc_id, sc.sc_name FROM sub_category sc WHERE sc.c_id = '$cat_id' AND sc.sc_status = 1");
        $stmt->execute();
        $cat_arr = array();
        $stmt->bind_result($cat_arr['id'],$cat_arr['name']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                //$msg[] = $cat_arr;
                $msg .= '<option value="'.$cat_arr['id'].'">'.$cat_arr['name'].'</option>';
            }
        }
        else
        {
            //$msg = '<script>swal("Oops!", "Sub Category Not Found.", "error");</script>';
        }
        return $msg;
        $stmt->close();
    }

    function add_sub_category($parent_cat_id,$name,$status)
    {
        $stmt = $this->conn->prepare("INSERT INTO sub_category(c_id, sc_name, sc_status) VALUES (?,?,?)");
        $stmt->bind_param("isi",$parent_cat_id,$name,$status);
        if($stmt->execute())
            $msg = '<script>swal("Good Job!", "Sub Category add successfuly.", "success");</script>';
        else
            $msg = '<script>swal("Oops!", "Unable to add Sub Category.", "error");</script>';
        $stmt->close();
        return $msg;
    }

    function get_all_sub_category()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT sc.sc_id, c.c_name, sc.sc_name, sc.sc_status FROM sub_category sc INNER JOIN category c on c.c_id = sc.c_id");
        $stmt->execute();
        $stmt->bind_result($param['id'],$param['p_cat_name'],$param['name'],$param['c_status']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                if($param['c_status']==0)
                    $param['res_status'] = '<span class="label label-danger">In-Active</span>';
                else
                    $param['res_status'] = '<span class="label label-success">Active</span>';
                $msg .= '<tr>
                            <td>'.$param['name'].'</td>
                            <td>'.$param['p_cat_name'].'</td>
                            <td>'.$param['res_status'].'</td>
                            <td>
                                <a class="btn btn-default btn-circle waves-effect waves-circle waves-purple" href="edit_sub_category?id='.$param['id'].'"><i class="material-icons">edit</i></a>
                            </td>
                        </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Sub Category Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

}