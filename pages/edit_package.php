<?php
include_once "../include/header.php";

if(!empty($_GET['id'])){
    $pkg = $package->get_single_pakage($_GET['id']);

if(isset($_POST['submit']))
{
    $param = array(
        'p_id'                          =>  $_POST['p_id'],
        'p_name'                        =>  $_POST['pgk_name'],
        'p_no_of_msg'                   =>  $_POST['pgk_no_of_msg'],
        'p_size_of_msg'                   =>  $_POST['size_of_msg'],
        'p_pkg_duration'                =>  $_POST['pgk_duration_count'] . " " . $_POST['pgk_duration'],
        'p_msg_duration'                =>  $_POST['pgk_msg_duration_count'] . " " . $_POST['pgk_msg_duration'],
        'p_data_backup_duration'        =>  $_POST['pgk_data_backup_duration_count'] . " " . $_POST['pgk_data_backup_duration'],
        'p_access_of_full_profile'      =>  $_POST['pgk_profile_access'],
        'p_price_promotion'             =>  $_POST['pgk_price_promotion'],
        'p_advertisement'               =>  $_POST['pgk_advertisement'],
        'p_advertisement_duration'      =>  ($_POST['pgk_advertisement_duration'] == "No" ? "No" : $_POST['pgk_advertisement_duration_count'] . " " . $_POST['pgk_advertisement_duration']),
        'p_advertisement_watch_time'    =>  ($_POST['pgk_advertisement_watch_time_count'] == "" ? "0" : $_POST['pgk_advertisement_watch_time_count']),
        'p_advertisement_time_a_day'    =>  "0",
        'p_price'                       =>  $_POST['pgk_price'],
        'p_crncy'                       =>  $_POST['pgk_currency'],
        'p_discount'                    =>  $_POST['pgk_discount'],
        'p_price_after_discount'        =>  $_POST['pgk_after_discount_price'],
        'p_for_user'                    =>  $_POST['pkg_for'],
        'p_visible'                     =>  $_POST['pkg_visible'],
        'is_paid'                       =>  $_POST['is_paid']
    );
    $result = $package->update_package($param);
    if ($result) {
        $pkg = $result;
        echo '<script>swal("Good Job!", "Package Updated successfully.", "success");</script>';
    } else {
        echo '<script>swal("Oops!", "Unable to Updated Package.", "error");</script>';
    }
}
?>

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Update Package</h2>
                </div>
                <div class="body">
                    <form id="form_advanced_validation" method="POST" action="">
                        <input type="text" value="<?php echo $pkg['p_id']; ?>" name="p_id" style="display:none;">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" value="<?php echo $pkg['p_name']; ?>" class="form-control" name="pgk_name" required>
                                <label class="form-label">Package Name</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" value="<?php echo $pkg['p_no_of_msg']; ?>" class="form-control" name="pgk_no_of_msg" required>
                                <label class="form-label">No. of Messages</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" value="<?php echo $pkg['p_size_of_msg']; ?>" class="form-control" name="size_of_msg" required>
                                <label class="form-label">Size of Message</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select pkg Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" min="1" value="<?php echo intval($pkg['p_pkg_duration']); ?>" name="pgk_duration_count" required>
                                <select name="pgk_duration">
                                    <option value="Month" <? echo substr($pkg['p_pkg_duration'], 1 + strlen(strval(intval($pkg['p_pkg_duration'])))) =='Month'? 'selected':'';  ?>>Month</option>
                                    <option value="Year" <? echo substr($pkg['p_pkg_duration'], 1 + strlen(strval(intval($pkg['p_pkg_duration'])))) =='Year'? 'selected':'';  ?>>Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select Message Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" value="<?php echo intval($pkg['p_msg_duration']); ?>" name="pgk_msg_duration_count" required>
                                
                                <select name="pgk_msg_duration">
                                    <option value="Day" <? echo substr($pkg['p_msg_duration'], 1 + strlen(strval(intval($pkg['p_msg_duration'])))) =='Day'? 'selected':'';  ?>>Day</option>
                                    <option value="Week" <? echo substr($pkg['p_msg_duration'], 1 + strlen(strval(intval($pkg['p_msg_duration'])))) =='Week'? 'selected':'';  ?>>Week</option>
                                    <option value="Month" <? echo substr($pkg['p_msg_duration'], 1 + strlen(strval(intval($pkg['p_msg_duration'])))) =='Month'? 'selected':'';  ?>>Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select Data Backup Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" value="<?php echo intval($pkg['p_data_backup_duration']); ?>" name="pgk_data_backup_duration_count" required>
                                <select name="pgk_data_backup_duration">
                                    <option value="Day">Day</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Visible</b>
                                </p>
                                <select name="pkg_visible" class="form-control show-tick">
                                    <option value="0" <? echo $pkg['p_visible'] == 0 ? 'selected': '' ?>>No</option>
                                    <option value="1" <? echo $pkg['p_visible'] == 1 ? 'selected': '' ?>>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Full Profile Access of  all users</b>
                                </p>
                                <select name="pgk_profile_access" value="<?php echo $pkg['p_access_of_full_profile']; ?>" class="form-control show-tick">
                                    <option value="0" <? echo $pkg['p_access_of_full_profile']==0?'selected':'';  ?> >No</option>
                                    <option value="1" <? echo $pkg['p_access_of_full_profile']==1?'selected':'';  ?>  >Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Price Promotion</b>
                                </p>
                                <select name="pgk_price_promotion" value="<?php echo $pkg['p_price_promotion']; ?>" class="form-control show-tick">
                                    <option value="0" <? echo $pkg['p_price_promotion']==0?'selected':'';  ?> >No</option>
                                    <option value="1" <? echo $pkg['p_price_promotion']==1?'selected':'';  ?>  >Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Advertisement</b>
                                </p>
                                <select name="pgk_advertisement" value="<?php echo $pkg['p_advertisement']; ?>" class="form-control show-tick">
                                    <option value="0" <? echo $pkg['p_advertisement']==0?'selected':'';  ?> >No</option>
                                    <option value="1" <? echo $pkg['p_advertisement']==1?'selected':'';  ?> >Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Advertisement Duration</b>
                                </p>
                                <input type="number" placeholder="Enter a number" id="pgk_advertisement_duration_count" value="<?php echo intval($pkg['p_advertisement_duration']); ?>" name="pgk_advertisement_duration_count">
                                <select name="pgk_advertisement_duration" id="pgk_advertisement_duration">
                                    <option value="No">No</option>
                                    <option value="Day" <? echo substr($pkg['p_advertisement_duration'], 1 + strlen(strval(intval($pkg['p_advertisement_duration']))))== 'Day' ? 'selected':'';  ?>>Day</option>
                                    <option value="Week" <? echo substr($pkg['p_advertisement_duration'], 1 + strlen(strval(intval($pkg['p_advertisement_duration']))))== 'Week' ? 'selected':'';  ?>>Week</option>
                                    <option value="Month" <? echo substr($pkg['p_advertisement_duration'], 1 + strlen(strval(intval($pkg['p_advertisement_duration']))))== 'Month' ? 'selected':'';  ?>>Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Advertisement Watch Time</b>
                                </p>
                                <input type="number" placeholder="Enter a number" value="<?php echo $pkg['p_advertisement_watch_time']; ?>" class="form-control" name="pgk_advertisement_watch_time_count">
                            </div>
                        </div>
                        <!--<div class="form-group form-float">-->
                        <!--    <p>-->
                        <!--        <b>Advertisement Times a day</b>-->
                        <!--    </p>-->
                        <!--    <select name="pgk_advertisement_time_a_day" value="<?php echo $pkg['p_advertisement_time_a_day']; ?>" class="form-control show-tick">-->
                        <!--        <option value="0" <? echo $pkg['p_advertisement_time_a_day']==0?'selected':'';  ?> >Select</option>-->
                        <!--        <option value="2" <? echo $pkg['p_advertisement_time_a_day']==2?'selected':'';  ?> >2 Times</option>-->
                        <!--        <option value="4" <? echo $pkg['p_advertisement_time_a_day']==4?'selected':'';  ?> >4 Times</option>-->
                        <!--        <option value="8" <? echo $pkg['p_advertisement_time_a_day']==8?'selected':'';  ?> >8 Times</option>-->
                        <!--        <option value="10" <? echo $pkg['p_advertisement_time_a_day']==10?'selected':'';  ?> >10 Times</option>-->
                        <!--    </select>-->
                        <!--</div>-->
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="pgk_price" value="<?php echo $pkg['p_price']; ?>" class="form-control" name="pgk_price" required>
                                    <label class="form-label">Package Price</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <p>
                                    <b>Select Currency</b>
                                </p>
                                <select name="pgk_currency" value="<?php echo $pkg['p_crncy']; ?>" class="form-control show-tick">
                                    <option value="Rs" <? echo $pkg['p_crncy']=='Rs'?'selected':'';  ?> >Rs</option>
                                    <option value="$" <? echo $pkg['p_crncy']=='$'?'selected':'';  ?> >$</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="pgk_discount" value="<?php echo $pkg['p_discount']; ?>" class="form-control" name="pgk_discount">
                                    <label class="form-label">Package Discount</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" id="pgk_after_discount_price" value="<?php echo $pkg['p_price_after_discount']; ?>" class="form-control" name="pgk_after_discount_price">
                                    <label class="form-label">Package Price After Discount</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <p>
                                <b>Package For</b>
                            </p>
                            <select name="pkg_for" value="<?php echo $pkg['p_for_user']; ?>" class="form-control show-tick" required>
                                <option value="0" <? echo $pkg['p_for_user']==0?'selected':'';  ?> >Pakistani Users</option>
                                <option value="1" <? echo $pkg['p_for_user']==1?'selected':'';  ?> >International Users</option>
                            </select>
                        </div>
                        <div class="form-group form-float">
                            <p>
                                <b>Paid</b>
                            </p>
                            <select name="is_paid" value="<?php echo $pkg['is_paid']; ?>" class="form-control">
                                <option value="0" <? echo $pkg['is_paid']==0?'selected':'';  ?> >No</option>
                                <option value="1" <? echo $pkg['is_paid']==1?'selected':'';  ?> >Yes</option>
                            </select>
                        </div>

                        <button class="btn btn-primary waves-effect" type="submit" name="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
    
    // Toggle pgk_advertisement_duration_count based on pgk_advertisement_duration
        var pgk_ad_duration_count = document.getElementById('pgk_advertisement_duration_count');
        var pgk_ad_duration = document.getElementById('pgk_advertisement_duration');
        
        if (pgk_ad_duration.value == "No") {
                pgk_ad_duration_count.style.display="none";        
        } else {
                pgk_ad_duration_count.style.display="inline-block";
        }
            
        pgk_ad_duration.onchange = function() {
            if (pgk_ad_duration.value == "No") {
                pgk_ad_duration_count.style.display="none";        
            } else {
                pgk_ad_duration_count.style.display="inline-block";
            }
        };
        
     // Calculate and populate price after discount
        var pgk_price = document.getElementById('pgk_price');
        var pgk_discount = document.getElementById('pgk_discount');
        var pgk_after_discount_price = document.getElementById('pgk_after_discount_price');
        
        function updatePriceAfterDiscount () {
            pgk_after_discount_price.value = Number(pgk_price.value) - Number(pgk_discount.value);
        }
        pgk_price.onkeyup = updatePriceAfterDiscount;
        pgk_discount.onkeyup = updatePriceAfterDiscount;
        
    </script>

<?php
}
include_once "../include/footer.php";
?>