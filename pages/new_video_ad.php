<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

include_once "../include/header.php";
?>

<div class="card">
    <div class="header">
        <h2>Create New Ad</h2>
    </div>
    <div class="body">
        <div id="ad_upload">
            <div>Choose your ad (only .mp4 of at most 20 mb in size is allowed):</div>
            <form id="uploadAdForm" enctype="multipart/form-data" method="POST" >
                <div style="display: flex; align-items: center; margin: 5px 0;">
                    <input type="file" name="ad_file" id="adFile" accept="video/mp4,video/x-m4v"/>
                    <button name="uploadBtn" id="uploadBtn" type="submit" style="margin-left: 10px;">Upload Ad</button>
                    <div class="upload_progress progress_text" style="margin-left:5px;">Uploading ..   </div>
                    <div class="upload_progress progress_num" style="margin-left:2px;">2%</div>
                </div>
                <div style="margin: 5px 0; color: red; display:flex;align-items:center;" id="uploadAdError">
                    <i class="material-icons">error</i>
                    <span style="margin-left: 2px;" id="uploadAdErrorText">There was an error</span>
                </div>
            </form>
        </div>
        <div id="ad_preview">
            <div>Ad Preview:</div>
            <video id="ad_preview_video" src="" width="480" height="320" controls>
            Your browser does not support the video tag.
            </video>
            <div style="margin-top:5px;">
                <button class="btn btn-danger" id="delete_ad_btn">Delete & Try Again</button>
            </div>
        </div>
        <div class="ad_form" style="margin-top: 20px;">
            <form id="completeAdForm" method="POST">
                <div>
                    <label>
                        Video Length (seconds):
                        <input type="number" min="0" class="form-control" id="video_length" name="video_length" placeholder="0 seconds" required/>
                    </label>
                </div>
                <div>
                    <label>
                        Number of Views:
                        <input type="number" min="0" class="form-control" id="num_views" name="num_views" placeholder="0" required/>
                    </label>
                </div>
                <div>
                    <label>
                        Duration:
                        <div style="display:flex;">
                            <input type="number" min="0" placeholder="0" class="form-control" id="duration_num" name="duration_num" required/>
                            <select name="duration_type" id="duration_type">
                                <option value="days">Days</option>
                                <option value="weeks">Weeks</option>
                                <option value="months">Months</option>
                            </select>
                        </div>
                    </label>
                </div>
                <div>
                    <label>
                        Ad link:
                        <input type="text" placeholder="Insert url (Optional)" class="form-control" id="ad_link" name="ad_link" />
                    </label>
                </div>
                <div style="margin-top:10px;">
                    <label>
                        Viewer Countries:
                        <div id="multiselect_container" style="min-width:500px;">
                            
                        </div>
                    </label>
                </div>
                <div style="margin-top:10px;">
                    <div>
                        <em>Access for Advertisers:</em>
                    </div>
                    <div style="display:flex;">
                        <label style="flex:1;">
                            ID:
                            <input type="text" placeholder="Only alphabets and numbers are allowed" class="form-control" id="advertiser_id" name="advertiser_id" required/>
                        </label>
                        <label style="flex:1;">
                            Password:
                            <input type="text" placeholder="Use a strong password" class="form-control" name="advertiser_pwd" id="advertiser_pwd"  required/>
                        </label>
                    </div>
                </div>
                <div style="margin-top:10px;">
                    <label style="user-select:none;">
                        <input id="isAdLive" type="checkbox" style="position:unset;margin:unset;opacity:unset;margin-right:3px;" name="is_live" checked/>
                        Live
                    </label>
                </div>
                <div style="margin-top:20px">
                    <button type="submit" class="btn btn-primary" id="createAdBtn">Create</button>
                    <span id="createAdProgressText" style="margin-left:5px;font-style:italic;">Creating ...</span>
                </div>
                <div style="margin-top: 10px; color: red; display:flex;align-items:center;" id="completeFormError">
                    <i class="material-icons">error</i>
                    <span style="margin-left: 2px;" id="completeFormErrorText">There was an error</span>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php"
?>
<script src="../plugins/jquery-multiselect/jquery.dropdown.min.js"></script>
<script>


$(document).ready(function() { 
    let ad_filename = null;
    let selected_countries = {};
    $('.upload_progress').hide();
    $('#ad_preview').hide();
    $('#uploadAdError').hide();
    $("#createAdProgressText").hide();
    
    $('#uploadAdForm').submit(function(e) {	
        e.preventDefault();
        $('#uploadAdError').hide();
        if($('#adFile').val()) {
            var fd = new FormData();    
            var file = $("#adFile")[0].files[0];
            fd.append( 'ad_file', file);
            
            if(file.size > 20 * 1000 * 1000) {
                $('#uploadAdError').show();
                $('#uploadAdErrorText').text("File size more than 20MB is not allowed.");
                return;
            }
            
            if(file.type != "video/mp4") {
                $('#uploadAdError').show();
                $('#uploadAdErrorText').text("File must be a mp4 video.");
                return;
            }

            $('.upload_progress').show();
            $("#uploadBtn").attr('disabled', true);

            $.ajax({
                xhr: function() {
                    
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                      if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $(".upload_progress.progress_num").text(percentComplete + "%");
                      }
                    }, false);
                
                    return xhr;
                  },
                  url: "upload_video_ad.php",
                  data: fd,
                  processData: false,
                  contentType: false,
                  type: 'POST',
                  success: function(result) {
                    ad_filename = JSON.parse(result).ad_filename;
                    $('#ad_preview').show();
                    $("#ad_preview_video").attr("src", "../upload/" + ad_filename);
                    $("#ad_upload").hide();
                    
                    $('.upload_progress').hide();
                    $("#uploadBtn").attr('disabled', false);
                    $(".upload_progress.progress_num").text("");
                  },
                  error: function() {
                    $('.upload_progress').hide();
                    $("#uploadBtn").attr('disabled', false);
                    $('#uploadAdError').show();
                    $('#uploadAdErrorText').text("An error occurred while uploading. Please try again.");
                    $(".upload_progress.progress_num").text("");
                  }
            });
        }
        else {
            $('#uploadAdError').show();
            $('#uploadAdErrorText').text("Please select a video to upload.");
        }
    });
    
    $("#delete_ad_btn").on("click", function(){
        $("#ad_upload").show();
        $("#ad_preview").hide();
        const fd = new FormData();    
        fd.append( 'ad_file', ad_filename);
        ad_filename = null;
        $.ajax({
          url: "delete_video_ad.php",
          data: fd,
          processData: false,
          contentType: false,
          type: 'POST',
        });
    });
    
    
    $("#completeFormError").hide();
    $("#completeAdForm").submit(function(e) {
        e.preventDefault();
        $("#completeFormError").hide();

       const video_length = Number($("#video_length").val());
       const num_views = Number($("#num_views").val());
       const duration_num = Number($("#duration_num").val());
       const duration_type = $("#duration_type").val();
       const ad_link = $("#ad_link").val().trim() || null;
       const advertiser_id = $("#advertiser_id").val().trim();
       const advertiser_pwd = $("#advertiser_pwd").val();
       const view_countries = Object.keys(selected_countries).filter(a => selected_countries[a]).join(",");
       
       const is_live = $("#isAdLive").is(":checked");
       
       let error = null;
       
       if (ad_filename == null) {
           error = "Please choose and upload an ad video first";
       }
       else if (Number.isNaN(video_length) || video_length <= 0) {
           error = "Invalid video length";
       }
       else if (Number.isNaN(num_views) || num_views <= 0) {
           error = "Invalid # of views";
       }
       else if (Number.isNaN(duration_num) || duration_num <= 0) {
           error = "Invalid duration";
       } 
       else if (advertiser_id.length < 4 || /^[a-z0-9]+$/i.test(advertiser_id) === false) {
           error = "Advertiser id should be at least 4 characters. Only alphabets & numbers are allowed";
       }
       else if (advertiser_pwd.length < 6) {
           error = "Advertiser password should be at least 6 characters.";
       }
       
       if (error) {
           $("#completeFormError").show();
           $("#completeFormErrorText").text(error);
           return;
       }
       
        $("#createAdProgressText").show();
        $("#createAdBtn").attr("disabled", true);
       
       const fd = new FormData();    
        fd.append( 'ad_file', ad_filename);
        fd.append('video_length', video_length);
        fd.append('num_views', num_views);
        fd.append('duration_num', duration_num);
        fd.append('duration_type', duration_type);
        fd.append('ad_link', ad_link);
        fd.append('advertiser_id', advertiser_id);
        fd.append('advertiser_pwd', advertiser_pwd);
        fd.append('view_countries', view_countries);
        fd.append('is_live', is_live);
        $.ajax({
          url: "create_video_ad.php",
          data: fd,
          processData: false,
          contentType: false,
          type: 'POST',
          success: function(data){
              $("#createAdBtn").attr("disabled", false);
              $("#createAdProgressText").hide();
              window.location.href="/pages/video_ad_list.php";
          },
          error: function(err) {
              $("#createAdBtn").attr("disabled", false);
              $("#createAdProgressText").hide();
              $("#completeFormError").show();
              $("#completeFormErrorText").text(err.responseText);
          }
        });
        
        
    });
    
    let countries = ['Afghanistan', 'Aland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia, Plurinational State of', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo', 'Costa Rica', 'Croatia', 'Denmark', 'Egypt', 'Ethiopia', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'Georgia', 'Germany', 'Greenland', 'Guyana', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Lebanon', 'Liberia', 'Libyan Arab Jamahiriya', 'Macao', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Malta', 'Marshall Islands', 'Mexico', 'Monaco', 'Morocco', 'Myanmar', 'Namibia', 'Nepal', 'Netherlands', 'New Zealand', 'Niger', 'Nigeria', 'North Korea', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Panama', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn', 'Poland', 'Portugal', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Reunion', 'Saint Barthelemy', 'Saint Lucia', 'Saint Martin', 'Samoa', 'San Marino', 'Saudi Arabia', 'Serbia', 'Sierra Leone', 'Singapore', 'Slovakia', 'Somalia', 'South Africa', 'South Korea', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vietnam', 'Yemen', 'Zambia', 'Zimbabwe'];
    countries = countries.map(function(a){return `<option value="${a}">${a}</option>`}).join("");
    $("#multiselect_container").html(`<select style="display:none;" name="" multiple placeholder="Select countries">${countries}</select>`)
    $("#multiselect_container").dropdown({
        input: '<input id="country_select_input" type="text" maxLength="20" placeholder="Search">',
        searchNoData: '<li style="color:#ddd">No Results</li>',
        multipleMode: 'label',
        readOnly: true,
        choice: function() {
            if (arguments[1]) {
                selected_countries[arguments[1].name] = arguments[1].selected; 
            } else {
                let c = arguments[0].target.getAttribute("data-id");
                selected_countries[c] = false; 
            }
            $("#country_select_input").val("").focus();
        },
    });
});
</script>