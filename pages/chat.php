<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 11/22/2018
 * Time: 3:30 PM
 */
include_once "../include/header.php";

require_once '../include/db_connect.php';
// opening db connection
$db = new DbConnect();
$conn = $db->connect();

// define how many results you want per page
$results_per_page = 10;

// find out the number of results stored in database
$stmt = $conn->prepare("SELECT message_id FROM messages");
$stmt->execute();
$stmt->store_result();
$number_of_results = $stmt->num_rows;
$stmt->close();

// determine number of total pages available
$number_of_pages = ceil($number_of_results/$results_per_page);

// determine which page number visitor is currently on
if (!isset($_GET['page'])) {
    $page = 1;
} else {
    $page = $_GET['page'];
}

// determine the sql LIMIT starting number for the results on the displaying page
$this_page_first_result = ($page-1)*$results_per_page;
$sql = 'SELECT u.u_name,u.u_country, u.u_image, m.message_id , m.message, m.created_at, c.c_name, sc.sc_name, m.approved_by FROM users u, category c, messages m, sub_category sc WHERE m.user_id = u.u_id AND c.c_id = m.category_id AND sc.sc_id = m.sub_cat_id AND m.status = 1 ORDER BY message_id desc LIMIT '. $this_page_first_result . ',' .  $results_per_page;
$stmt = $conn->prepare($sql);
$stmt->execute();
$msg_arr = array();
$stmt->bind_result($msg_arr['u_name'], $msg_arr['u_country'], $msg_arr['u_image'], $msg_arr['message_id'], $msg_arr['message'], $msg_arr['created_at'], $msg_arr['c_name'], $msg_arr['sc_name'], $msg_arr['approved_by']);
$stmt->store_result();
?>

<!-- Default Media -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Approved Chats
                    <small>The messages have been approved.</small>
                </h2>
            </div>
            <div id="load_un_approve_chat_here" class="body">
                <!-- Page Loader -->
                <div id="loading" class="page-loader-wrapper">
                    <div class="loader">
                        <div class="preloader">
                            <div class="spinner-layer pl-red">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                        <p>Please wait...</p>
                    </div>
                </div>
                <!-- #END# Page Loader -->
                <?php while($stmt->fetch()) { ?>
                <div class="media">
                    <div class="media-left">
                        <a href="javascript:void(0);">
                            <img class="media-object" src=" <?= $msg_arr['u_image']; ?> " width="64" height="64">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading approve_msg_heading">
                            <?php echo $msg_arr['u_name'].' -> '.$msg_arr['c_name'].' -> '.$msg_arr['sc_name'].' -> '.$msg_arr['u_country']; ?>
                            <i data-id="<?php echo $msg_arr['message_id']; ?> " class="material-icons edit_button_approve" role="button">delete</i>
                        </h4>
                        <p class="user_message_Approve"><?php echo $msg_arr['message']; ?> </p>
                        <p><?php echo $msg_arr['created_at']; ?></p>
                        <?php if (strlen($msg_arr['approved_by']) > 0) { ?>
                        <p>Approved By: <?= $msg_arr['approved_by']; ?></p>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <nav>
                <ul class="pagination">
                    <li class="disabled">
                        <a href="javascript:void(0);">
                            <i class="material-icons">chevron_left</i>
                        </a>
                    </li>
                    <?php
                    for ($page=1;$page<=$number_of_pages;$page++)
                    {
                        echo '<li><a href="chat?page=' . $page . '" class="waves-effect">' . $page . '</a></li>';
                    }
                    ?>
                    <li>
                        <a href="javascript:void(0);" class="waves-effect">
                            <i class="material-icons">chevron_right</i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- #END# Default Media -->


<?php
$stmt->close();
include_once "../include/footer.php";
?>
<script>
    $('.edit_button_approve').click(function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            async:false,
            data: { request:'delete_message', id: id},
            url: "ajaxcall.php",
            success: function(data)
            {
                swal(data);
            }
        });
    });
</script>
