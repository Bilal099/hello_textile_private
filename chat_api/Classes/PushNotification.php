<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 10/6/2018
 * Time: 4:19 PM
 */

class PushNotification
{
    // sending push message to single user by FCM registration id
    public function send($to, $title, $msg, $image = null) {
        $message = array(
            'for'       =>  'push',
            'title'     =>  $title,
            'message'   =>  $msg,
            'image'     =>  $image,
        );
        $ios = array(
            'title'             =>  $title,
            'body'           =>  $msg,
            'content_available' =>  true,
            'badge'             =>  1,
        );
        $fields = array(
            'to'            => $to[0],
            'data'  => $message
        );
        if ($this->isIos($to)) {
            $fields['notification'] = $ios;
        }
        return $this->sendPushNotification($fields);
    }

    public function deleteFromAdmin($to, $title, $msg, $image) {
        $message = array(
            'for'       =>  'DeleteFromAdmin',
            'title'     =>  $title,
            'message'   =>  $msg,
            'image'     =>  $image,
        );
        $ios = array(
            'title'     =>  $title,
            'body'   =>  $msg,
            'content_available' =>  true,
            'badge'             =>  1,
            
        );
        $fields = array(
            'to'            => $to[0],
            'data'  => $message
        );
        if ($this->isIos($to)) {
            $fields['notification'] = $ios;
        }
        return $this->sendPushNotification($fields);
    }
    private function isIos($token) {
        return intval($token[1]) == 1;
    }
    private function isAndroid($token) {
        return intval($token[1]) == 0;
    }
    private function extractActualToken($token) {
        return $token[0];
    }
    public function for_single_chating($tokens, $from_id, $to_id, $to_u_name ,$msg, $created_at, $images, $msg_id)
    {
        $tokens_android = $this->getAndroidTokens($tokens) ;
        $tokens_ios = $this->getIosTokens($tokens); 
        $message = array(
            'title'         =>  "single_chat",
            'from_id'       =>  $from_id,
            'to_id'         =>  $to_id,
            'to_u_name'     =>  $to_u_name,
            'massege'       =>  $msg,
            'created_at'    =>  $created_at,
            'images'        =>  $images,
            'sc_id'         =>  $msg_id
        );
        $ios = array(
            'title'         =>  $to_u_name . " sent you a message",
            'from_id'       =>  $from_id,
            'to_id'         =>  $to_id,
            'to_u_name'     =>  $to_u_name,
            'sc_id'         =>  $msg_id,
            'body'       =>  $msg,
            'created_at'    =>  $created_at,
            'content_available' =>  true,
            'badge'             =>  1,
            
        );
        $fieldsAndroid = array(
            'registration_ids'  => $tokens_android,
            'notification'      => $ios,
            'data'              => $message,
        );
        
        $fieldsIos = array(
            'registration_ids'  => $tokens_ios,
            'notification'     => $ios,
            'data'              => $message,
        );
        $result = 0;
        if (count($tokens_android) > 0) {
            $result = $this->sendPushNotification($fieldsAndroid);
        }
        if (count($tokens_ios) > 0) {
            $result = $this->sendPushNotification($fieldsIos);
        }
        
        return $result;
    }

    // Sending message to a topic by topic id

    public function sendToAllFromAdmin($tokens, $massege, $u_id, $u_name, $created_at, $name, $c_id, $sc_id) {
        $tokens_android = $this->getAndroidTokens($tokens) ;
        $tokens_ios = $this->getIosTokens($tokens); 
        $message = array(
            'title'       =>  "qwerty",
            'message_from'  => "admin",
            'massege'       =>  $massege,
            'u_id'          =>  $u_id,
            'u_name'        =>  $u_name,
            'created_at'    =>  $created_at,
            'id_name'       =>  $name,
            'c_id'          =>  $c_id,
            'sc_id'         =>  $sc_id,
            'ad_msg'        => '1',
        );
        $ios = array(
            'title'             =>  "admin_message",
            'message_from'      => "admin",
            'body'           =>  $massege,
            'u_id'              =>  $u_id,
            'u_name'            =>  $u_name,
            'created_at'        =>  $created_at,
            'id_name'           =>  $name,
            'c_id'              =>  $c_id,
            'sc_id'             =>  $sc_id,
            'ad_msg'            => '1',
            'content_available' =>  true,
            'badge'             =>  1,
            
        );
        $fieldsAndroid = array(
            'registration_ids'  => $tokens_android,
            'notification'     => $ios,
            'data'              => $message
        );

        $fieldsIos = array(
            'registration_ids'  => $tokens_ios,
            'notification'     => $ios,
            'data'              => $message,
        );
        $result = 0;
        if (count($tokens_android) > 0) {
            $result = $this->sendPushNotification($fieldsAndroid);
        }
        if (count($tokens_ios) > 0) {
            $result = $this->sendPushNotification($fieldsIos);
        }
        
        return $result;
    }

    public function sendToTopic($to, $massege, $u_id, $u_name, $created_at, $name, $c_id, $sc_id) {

        $message = array(
            'title'         =>  $massege,
            'massege'       =>  $massege,
            'u_id'          =>  $u_id,
            'u_name'        =>  $u_name,
            'created_at'    =>  $created_at,
            'id_name'       =>  $name,
            'c_id'          =>  $c_id,
            'sc_id'         =>  $sc_id,
            'ad_msg'        => '0',
        );
        $ios = array(
            'title'             =>  $massege,
            'body'           =>  $massege,
            'u_id'              =>  $u_id,
            'u_name'            =>  $u_name,
            'created_at'        =>  $created_at,
            'id_name'           =>  $name,
            'c_id'              =>  $c_id,
            'sc_id'             =>  $sc_id,
            'ad_msg'            => '0',
            'content_available' =>  true,
            'badge'             =>  1,
            
        );
        $fields = array(
            'to'            => $to[0],
            'data'              => $message,
            'notification'  => $ios
        );

        return $this->sendPushNotification($fields);
    }

    // Sending message to a topic by topic id
    public function sendToApprove($to, $massege, $u_id, $u_name, $created_at, $msg_id, $c_id, $sc_id,$c_name,$sc_name) {
        $tokens_android = $this->getAndroidTokens($to) ;
        $tokens_ios = $this->getIosTokens($to); 
        $message = array(
            'title'             =>  "approve_from_admin",
            'massege'           =>  $massege,
            'u_id'              =>  $u_id,
            'u_name'            =>  $u_name,
            'created_at'        =>  $created_at,
            'msg_id'            =>  $msg_id,
            'c_id'              =>  $c_id,
            'sc_id'             =>  $sc_id,
            'c_name'            =>  $c_name,
            'sc_name'           =>  $sc_name
        );
        $ios = array(
            'title'             =>  "New message in $c_name",
            'body'              =>  $massege,
            'u_id'              =>  $u_id,
            'u_name'            =>  $u_name,
            'created_at'        =>  $created_at,
            'msg_id'            =>  $msg_id,
            'c_id'              =>  $c_id,
            'sc_id'             =>  $sc_id,
            'c_name'            =>  $c_name,
            'sc_name'           =>  $sc_name,
            'content_available' =>  true,
            'badge'             =>  1,
            
        );
        $fieldsAndroid = array(
            'registration_ids'  => $tokens_android,
            'notification'     => $ios,
            'data'              => $message,
        );

        $fieldsIos = array(
            'registration_ids'  => $tokens_ios,
            'notification'     => $ios,
            'data'              => $message,
        );
        $result = 0;
        if (count($tokens_android) > 0) {
            $result = $this->sendPushNotification($fieldsAndroid);
        }
        if (count($tokens_ios) > 0) {
            $result = $this->sendPushNotification($fieldsIos);
        }
        
        return $result;
    }

    // Send Push Notification to Res and Admin panel Using Pusher
    public function notify_web($msg)
    {
        require __DIR__ . '/../vendor/autoload.php';

        $options = array(
            'cluster' => 'us2',
            'useTLS' => true
        );
        $pusher = new Pusher\Pusher(
            'ea28f4594432416f57d9',
            'd791c80be44ec70b5657',
            '627707',
            $options
        );

        $data = $msg;
        $pusher->trigger('my-channel', 'my-event', $data);
    }

    // currently not being used anywhere
    public function send_push_on_Admin_msg($tokens, $title, $msg, $cat_id, $sub_cat_id, $user_id) {
        $message = array(
            'for'   =>  'admin_message',
            'title' =>  $title,
            'message'   =>  $msg,
            'cat_id' =>  $cat_id,
            'sub_cat_id' =>  $sub_cat_id,
            'user_id' =>  $user_id
        );
        $ios = array(
            'title' =>  $title,
            'body'   =>  $msg,
            'cat_id' =>  $cat_id,
            'sub_cat_id' =>  $sub_cat_id,
            'user_id' =>  $user_id,
            'content_available' =>  true,
            'badge' => 1
        );
        $fields = array(
            'registration_ids' => $tokens,
            'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    public function send_push_to_noti_panel($tokens, $title, $msg, $image) {
        $tokens_android = $this->getAndroidTokens($tokens) ;
        $tokens_ios = $this->getIosTokens($tokens); 
        $message = array(
            'for'   =>  'notification',
            'title' =>  $title,
            'message'   =>  $msg,
            'image' =>  $image,
        );
        $ios = array(
            'title' =>  $title,
            'body'   =>  $msg,
            'content_available' =>  true,
            'badge'             =>  1,
        );
        $fieldsAndroid = array(
            'registration_ids'  => $tokens_android,
            'notification'     => $ios,
            'data'              => $message,
        );

        $fieldsIos = array(
            'registration_ids'  => $tokens_ios,
            'notification'     => $ios,
            'data'              => $message,
        );
        $result = 0;
        if (count($tokens_android) > 0) {
            $result = $this->sendPushNotification($fieldsAndroid);
        }
        if (count($tokens_ios) > 0) {
            $result = $this->sendPushNotification($fieldsIos);
        }
        
        return $result;
    }

    public function send_push_to_news($tokens, $title, $msg, $image) {
        $tokens_android = $this->getAndroidTokens($tokens) ;
        $tokens_ios = $this->getIosTokens($tokens); 
        $message = array(
            'for'   =>  'news',
            'title' =>  $title,
            'message'   =>  $msg,
            'image' =>  $image,
        );
        $ios = array(
            'title' =>  $title,
            'body'   =>  $msg,
            'content_available' =>  true,
            'badge'             =>  1,
        );
        $fieldsAndroid = array(
            'registration_ids'  => $tokens_android,
            'notification'     => $ios,
            'data'              => $message,
        );

        $fieldsIos = array(
            'registration_ids'  => $tokens_ios,
            'notification'     => $ios,
            'data'              => $message,
        );
        $result = 0;
        if (count($tokens_android) > 0) {
            $result = $this->sendPushNotification($fieldsAndroid);
        }
        if (count($tokens_ios) > 0) {
            $result = $this->sendPushNotification($fieldsIos);
        }
        
        return $result;
    }
    private function getAndroidTokens($tokens){
        return array_map(function($t){return $t[0];}, array_values(array_filter($tokens, function($t){return intval($t[1]) == 0;})));
    }
    private function getIosTokens($tokens){
        return array_map(function($t){return $t[0];}, array_values(array_filter($tokens, function($t){return intval($t[1]) == 1;})));
    }
    // sending push message to Multiple user by FCM registration id
    public function send_multiple($tokens, $title, $msg, $image) {
        
        // return $msg;
        $tokens_android = $this->getAndroidTokens($tokens) ;
        $tokens_ios = $this->getIosTokens($tokens); 

        // return array_chunk($tokens_android,999);

        $tokens_android_chunk = array_chunk($tokens_android,999);

        $tokens_ios_chunk = array_chunk($tokens_ios,999);


        $message = array(
            'for'   =>  'push',
            'title' =>  $title,
            'message'   =>  $msg,
            'image' =>  $image,
        );
        $ios = array(
            'title' =>  $title,
            'message'   =>  $msg,
            'content_available' =>  true,
            'badge'             =>  1,
            
        );

        $fieldsAndroid = array();
        $fieldsIos = array();

        foreach ($tokens_android_chunk as $key => $value) 
        {
            $fieldsAndroid[$key] = array(
                'registration_ids'  => $value,
                // 'notification'     => $ios,
                'data'              => $message,
            );
        }

        foreach ($tokens_ios_chunk as $key => $value) 
        {
            $fieldsIos[$key] = array(
                'registration_ids'  => $value,
                'notification'     => $ios,
                'data'              => $message,
            );
        }

        // return $fieldsAndroid;
        
        // $fieldsAndroid = array(
        //     'registration_ids'  => $tokens_android,
        //     'notification'     => $ios,
        //     'data'              => $message,
        // );

        // $fieldsIos = array(
        //     'registration_ids'  => $tokens_ios,
        //     'notification'     => $ios,
        //     'data'              => $message,
        // );
        $result = 0;

        // $test['fieldsAndroid'] = $fieldsAndroid;
        // $test['fieldsIos'] = $fieldsIos;
        // $test = "";
        // if (count($tokens_ios) > 1000) {
        //     $test .= "Greater than 1000 total ".count($tokens_ios);
        // }
        // else if (count($tokens_ios) < 1000){
        //     $test .= "Less than 1000 total ".count($tokens_ios);
        // }

        // if (count($tokens_android) > 1000) {
        //     $test .= " Greater than 1000 total ad ".count($tokens_android);
        // }
        // else if (count($tokens_android) < 1000){
        //     $test .= " Less than 1000 total ad ".count($tokens_android);
        // }
        
        
        // if (count($tokens_ios) > 0) {
        
        // }
        // return $fieldsAndroid;
        // return $test;
        // die();


        if (count($tokens_android) > 0) 
        {
            foreach ($fieldsAndroid as $key => $value) 
            {
                $result = $this->sendPushNotification($value);
            }
            // $result = $this->sendPushNotification($fieldsAndroid);
        }
        if (count($tokens_ios) > 0) 
        {
            foreach ($fieldsIos as $key => $value) 
            {
                $result = $this->sendPushNotification($value);
            }
            // $result = $this->sendPushNotification($fieldsIos);
        }
        
        return $result;
        
    }

    // function makes curl request to FCM servers
    private function sendPushNotification($fields) {

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        //var_dump($result);exit;
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }
}