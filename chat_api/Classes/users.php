<?php
/**
 * Created by DK_KHAN.
 * User: DK_KHAN
 * Date: 10/4/2018
 * Time: 2:18 PM
 */

class users
{
    function email_authentication($email)
    {
        global $DB;
        $sql = "SELECT u_email FROM users WHERE u_email LIKE '%$email%' ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
            return true;
        else
            return false;
    }

    function phoneNo_authentication($no)
    {
        global $DB;
        $sql = "SELECT u_contact FROM users WHERE u_contact LIKE '%$no%' ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
            return true;
        else
            return false;
    }

    function update_user_FCM_ID($token, $email, $ios)
    {
        global $DB;
        $ios = isset($ios) ? ", ios=".$ios : "";
        $sql = "UPDATE users SET u_fcm_id = '$token' $ios WHERE u_email = '$email' ";
        if($DB->qr($sql))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function user_registration($user_arr)
    {
        // return 'bilal';
        $creation_date = date('m/d/Y');
        $expiry_date_for_pakistani = date('m/d/Y', strtotime(user_expiry_for_pakistani));
        $expiry_date_for_international = date('m/d/Y', strtotime(user_expiry_for_international));
        global $DB;
        
        $user_arr['email'] = str_replace(' ', '', $user_arr['email']);
        $user_arr['contact'] = str_replace(' ', '', $user_arr['contact']);
        if($this->email_authentication($user_arr['email']))
        {
            //error
            $msg = "Email already exist.";
            $data = ['success'=>false,'message'=>$msg];
            return json_encode($data);
        }
        else
        {
            if($this->phoneNo_authentication($user_arr['contact']))
            {
                //error
                $msg = "Phone No already exist.";
                $data = ['success'=>false,'message'=>$msg];
                return json_encode($data);
            }
            else
            {
                //success
                if($user_arr['user_type'] == 0)
                {
                    //Pakistani User
                    // $sql = "INSERT INTO users(u_name, u_email, u_contact, u_pass, u_city, u_company, u_comp_address, u_nature_bussniess, u_fcm_id, u_created_at, u_expiry_date, u_pkg_type, renew_msg_date, u_total_msg, u_is_paid, u_type, size_of_msg, device_id, u_country, ios, u_status) 
                    //                   VALUES ('".$user_arr['name']."' , '".$user_arr['email']."' , '".$user_arr['contact']."' , '".$user_arr['pass']."' , '".$user_arr['city']."' , '".$user_arr['company']."' , '".$user_arr['company_address']."' , '".$user_arr['bussniess']."' , '".$user_arr['fcm_id']."' , '$creation_date', '$expiry_date_for_pakistani', 'Free', '$expiry_date_for_pakistani' , '5000', '0', '0' , '400' , '".$user_arr['device_id']."' , '".$user_arr['country']."', '".$user_arr['ios']."', '1' ) ";
                    $sql = "INSERT INTO users(
                        u_name,
                        u_email,
                        u_contact,
                        u_pass,
                        u_city,
                        u_company,
                        u_comp_address,
                        u_nature_bussniess,
                        u_fcm_id,
                        u_created_at,
                        u_expiry_date,
                        pkg_id,
                        u_pkg_type,
                        renew_msg_date,
                        u_total_msg,
                        u_is_paid,
                        u_type,
                        size_of_msg,
                        device_id,
                        u_country,
                        ios,
                        u_status,
                        u_seller_company,
                        u_buyer_company,
                        u_seller_city,
                        u_buyer_city,
                        u_seller_country,
                        u_buyer_country,
                        u_seller_business_cat,
                        u_buyer_business_cat,
                        u_seller_business_nat,
                        u_buyer_business_nat,
                        u_seller_phone,
                        u_buyer_phone,
                        u_seller_email,
                        u_buyer_email,
                        u_seller_address,
                        u_buyer_address
                    )
                    VALUES(
                        '".$user_arr['name']."',
                        '".$user_arr['email']."',
                        '".$user_arr['contact']."',
                        '".$user_arr['pass']."',
                        '".$user_arr['city']."',
                        '".$user_arr['company']."',
                        '".$user_arr['company_address']."',
                        '".$user_arr['bussniess']."',
                        '".$user_arr['fcm_id']."',
                        '$creation_date',
                        '$expiry_date_for_pakistani',
                        '31',
                        'Golden 3M',
                        '$creation_date',
                        '5000',
                        '1',
                        '0',
                        '400',
                        '".$user_arr['device_id']."',
                        '".$user_arr['country']."',
                        '".$user_arr['ios']."',
                        '1',
                        '".$user_arr['company']."',
                        '".$user_arr['company']."',
                        '".$user_arr['city']."',
                        '".$user_arr['city']."',
                        '".$user_arr['country']."',
                        '".$user_arr['country']."',
                        'trades',
                        'trades',
                        '".$user_arr['bussniess']."',
                        '".$user_arr['bussniess']."',
                        '".$user_arr['contact']."',
                        '".$user_arr['contact']."',
                        '".$user_arr['email']."',
                        '".$user_arr['email']."',
                        '".$user_arr['company_address']."',
                        '".$user_arr['company_address']."'
                            
                    )";
                }
                else
                {
                    //International User
                    // $sql = "INSERT INTO users(u_name, u_email, u_contact, u_pass, u_city, u_company, u_comp_address, u_nature_bussniess, u_fcm_id, u_created_at, u_expiry_date, u_pkg_type, renew_msg_date, u_total_msg, u_is_paid, u_type, size_of_msg, device_id, u_country, ios, u_status) 
                    //                   VALUES ('".$user_arr['name']."' , '".$user_arr['email']."' , '".$user_arr['contact']."' , '".$user_arr['pass']."' , '".$user_arr['city']."' , '".$user_arr['company']."' , '".$user_arr['company_address']."' , '".$user_arr['bussniess']."' , '".$user_arr['fcm_id']."' , '$creation_date', '$expiry_date_for_international', 'Free', '$expiry_date_for_international' , '100', '0', '1' , '400' , '".$user_arr['device_id']."' , '".$user_arr['country']."', '".$user_arr['ios']."', '1' ) ";
                    $sql = "INSERT INTO users(
                        u_name,
                        u_email,
                        u_contact,
                        u_pass,
                        u_city,
                        u_company,
                        u_comp_address,
                        u_nature_bussniess,
                        u_fcm_id,
                        u_created_at,
                        u_expiry_date,
                        pkg_id,
                        u_pkg_type,
                        renew_msg_date,
                        u_total_msg,
                        u_is_paid,
                        u_type,
                        size_of_msg,
                        device_id,
                        u_country,
                        ios,
                        u_status,
                        u_seller_company,
                        u_buyer_company,
                        u_seller_city,
                        u_buyer_city,
                        u_seller_country,
                        u_buyer_country,
                        u_seller_business_cat,
                        u_buyer_business_cat,
                        u_seller_business_nat,
                        u_buyer_business_nat,
                        u_seller_phone,
                        u_buyer_phone,
                        u_seller_email,
                        u_buyer_email,
                        u_seller_address,
                        u_buyer_address
                    )
                    VALUES(
                        '".$user_arr['name']."',
                        '".$user_arr['email']."',
                        '".$user_arr['contact']."',
                        '".$user_arr['pass']."',
                        '".$user_arr['city']."',
                        '".$user_arr['company']."',
                        '".$user_arr['company_address']."',
                        '".$user_arr['bussniess']."',
                        '".$user_arr['fcm_id']."',
                        '$creation_date',
                        '$expiry_date_for_international',
                        '31',
                        'Golden 3M',
                        '$creation_date',
                        '5000',
                        '1',
                        '1',
                        '400',
                        '".$user_arr['device_id']."',
                        '".$user_arr['country']."',
                        '".$user_arr['ios']."',
                        '1',
                        '".$user_arr['company']."',
                        '".$user_arr['company']."',
                        '".$user_arr['city']."',
                        '".$user_arr['city']."',
                        '".$user_arr['country']."',
                        '".$user_arr['country']."',
                        'trades',
                        'trades',
                        '".$user_arr['bussniess']."',
                        '".$user_arr['bussniess']."',
                        '".$user_arr['contact']."',
                        '".$user_arr['contact']."',
                        '".$user_arr['email']."',
                        '".$user_arr['email']."',
                        '".$user_arr['company_address']."',
                        '".$user_arr['company_address']."'
                            
                    )";
                }
                if($DB->qr($sql))
                {
                    $msg = "Registration Successful. Login to Proceed";
                    $data = ['success' => true,'message'=>$msg];
                    return json_encode($data);
                }
                else
                {
                    $msg = "Server Error Please try again.";
                    $data = ['success'=>false,'message'=>$msg];
                    return json_encode($data);
                }
            }
        }
    }
    
    function save_profile($arr) {
        global $DB;
        $u_id         = $arr['u_id'];
        $profile_type = $arr['profile_type'];
        $business_cat = $arr['business_cat']; 
        $business_nat = $arr['business_nat']; 
        $desc         = $arr['desc'];                 
        $company      = $arr['company'];           
        $phone        = $arr['phone'];
        $email        = $arr['email'];
        $country      = $arr['country'];
        $city         = $arr['city'];
        $address      = $arr['address'];
        $products     = $arr['products'];
        
        $sql = "UPDATE users SET
            u_{$profile_type}_business_cat='$business_cat',
            u_{$profile_type}_business_nat='$business_nat',
            u_{$profile_type}_desc='$desc',
            u_{$profile_type}_company='$company',
            u_{$profile_type}_phone='$phone',
            u_{$profile_type}_email='$email',
            u_{$profile_type}_country='$country',
            u_{$profile_type}_city='$city',
            u_{$profile_type}_address='$address',
            u_{$profile_type}_products='$products'
        WHERE u_id=$u_id";
        
        if($DB->qr($sql)) {
            $profile = ($profile_type=='seller')? 0:1;
            $date = date("Y-m-d H:i:s");
            $query1 = "SELECT * FROM user_profile_update WHERE user_id = '$u_id' AND profile='$profile'";
            $user1 = $DB->fa($DB->qr($query1));
            if ($user1!=null) 
            {
                $query2 = "UPDATE user_profile_update SET updated_at='$date' WHERE user_id = '$u_id' AND profile='$profile'";
            } 
            else 
            {
                $query2 = "INSERT INTO user_profile_update(user_id, profile, is_seen, created_at, updated_at) VALUES ('$u_id','$profile',0,'$date','$date')";
            }
            if($DB->qr($query2))
            {
                return json_encode(["success" => true, "message" => "Profile successfully updated."]);
            }
            else
            {
                return json_encode(["success" => false, "message" => "Something went wrong"]);
            }
        }
        return json_encode(['success' => false, 'message' => 'Something went wrong']);
    }
    
    function search_profiles($arr) {
        global $DB;
        $u_id = $arr['u_id'];
        $profile_type = $arr['profile_type']; // u_profile_type
        $business_cat = $arr['business_cat'];
        $business_nat = $arr['business_nat']; 
        $country = $arr['country'];
        $city = $arr['city'];
        $page = $arr['page'];
        $limit = $arr['limit'] <= 0 ? 10 : $arr['limit'] % 50;
        $keywords = $arr['keywords'];
        $company = $arr['company'];
        
        
        $sql = "SELECT u_is_paid FROM users WHERE u_id=$u_id";
        $user = $DB->qr($sql);
        if ($DB->nr($user) != 1) {
            return json_encode(['success' => false, 'message' => 'invalid user']);       
        } 
        $user  = $DB->fa($user);
        
        $user_search_record = $this->user_search_record($arr);
        
        $filter_business_cat = empty($business_cat) ? "" : "AND u_{$profile_type}_business_cat='$business_cat'";
        $filter_business_nat = empty($business_nat) ? "" : "AND u_{$profile_type}_business_nat='$business_nat'";
        $filter_country = empty($country) ? ""           : "AND u_{$profile_type}_country LIKE '%$country%'";
        $filter_city = empty($city) ? ""                 : "AND u_{$profile_type}_city LIKE '%$city%'";
        $filter_company = empty($company)? ""               : "AND u_{$profile_type}_company LIKE '%$company%'";
        
        $search_text = trim("$business_cat $business_nat $country $city $keywords");
        $search_columns = "u_{$profile_type}_desc, u_{$profile_type}_company, u_{$profile_type}_business_cat, u_{$profile_type}_business_nat, u_{$profile_type}_country, u_{$profile_type}_city, u_{$profile_type}_products, u_name";
        $filter_keywords = empty($keywords) ? ""         : "AND MATCH($search_columns) AGAINST('$search_text' IN BOOLEAN MODE)";
        $start = $page * $limit;
        

        $sql = "SELECT 
                    u_id, 
                    u_name, 
                    u_last_active,
                    u_is_paid,
                    u_{$profile_type}_desc as u_desc,
                    u_{$profile_type}_company as u_company,
                    u_{$profile_type}_city as u_city,
                    u_{$profile_type}_country as u_country,
                    u_{$profile_type}_business_cat as u_business_cat,
                    u_{$profile_type}_business_nat as u_business_nat,
                    u_{$profile_type}_phone as u_phone,
                    u_{$profile_type}_email as u_email,
                    u_{$profile_type}_address as u_address,
                    u_{$profile_type}_products as u_products,
                    MATCH(
                        u_{$profile_type}_company,
                        u_{$profile_type}_products,
                        u_name
                    ) AGAINST('$keywords' IN BOOLEAN MODE) AS colValue
                FROM users 
                WHERE u_id!=$u_id $filter_business_cat $filter_business_nat $filter_country $filter_city $filter_keywords $filter_company
                ORDER BY colValue  DESC
                LIMIT $start, $limit";
        
        $res = $DB->qr($sql);
        // return json_encode([ 'res' => $res]);
        $users = [];
        while($r = $DB->fa($res)) {
            $r['u_products'] = array_filter(explode(" , ", $r['u_products']), "strlen");

            $r['u_name']            = ($r['u_name'] == null)? "":$r['u_name'];
            $r['u_is_paid']         = ($r['u_is_paid'] == null)? "":$r['u_is_paid'];
            $r['u_last_active']     = ($r['u_last_active'] == null)? "":$r['u_last_active'];
            $r['u_desc']            = ($r['u_desc'] == null)? "":$r['u_desc'];
            $r['u_company']         = ($r['u_company'] == null)? "":$r['u_company'];
            $r['u_city']            = ($r['u_city'] == null)? "":$r['u_city'];
            $r['u_country']         = ($r['u_country'] == null)? "":$r['u_country'];
            $r['u_business_cat']    = ($r['u_business_cat'] == null)? "":$r['u_business_cat'];
            $r['u_business_nat']    = ($r['u_business_nat'] == null)? "":$r['u_business_nat'];
            $r['u_phone']           = ($r['u_phone'] == null)? "":$r['u_phone'];
            $r['u_email']           = ($r['u_email'] == null)? "":$r['u_email'];
            $r['u_address']         = ($r['u_address'] == null)? "":$r['u_address'];
            $users[] = $r;
        }
        array_pop($users); // last item is null, so remove it
        return json_encode(['sql' => $sql, 'success' => true, 'data' => $users]);
    }

    function user_login($email,$pass,$token, $device_id, $ios)
    {
        global $DB;
        $sql = "SELECT u_id, u_name, u_email, u_status, device_id, u_country FROM users WHERE u_email = '$email' AND u_pass = '$pass' ";
        $res = $DB->qr($sql);
        if($DB->nr($res) == 1)
        {
            $row  = $DB->fa($res);
            if($row['u_status'] == 0)
            {
                $message = "Your account is In-Active. Wait for admin approval.";
                $data 	= ['success' => false,'message'=>$message];
                return json_encode($data);
            }
            else
            {
                if($row['device_id'] == $device_id)
                {
                    if($this->update_user_FCM_ID($token,$email, $ios))
                    {
                        $this->update_user_last_active($email);
                        $message = "Login Successful.";
                        $data 	= ['success' => true, 'message'=>$message, 'name'=>$row['u_name'], 'email'=>$row['u_email'] ,'id' => $row['u_id'], 'country' => $row['u_country']];
                        return json_encode($data);
                    }
                    else
                    {
                        $this->update_user_last_active($email);
                        $message = "Login Successful. Unable to update FCM";
                        $data 	= ['success' => true, 'message'=>$message, 'name'=>$row['u_name'], 'email'=>$row['u_email'] ,'id' => $row['u_id'], 'country' => $row['u_country']];
                        return json_encode($data);
                    }
                }
                else
                {
                    $message = "Account already logged in.";
                    $data 	= ['success' => false,'message'=>$message];
                    return json_encode($data);
                }
            }
        }
        else
        {
            $message = "Invalid login details.";
            $data 	= ['success' => false,'message'=>$message];
            return json_encode($data);
        }
    }

    function get_user_by_id($u_id)
    {
        global $DB;
        $sql = "SELECT u_id, u_name, u_fcm_id from users WHERE u_id = '$u_id' ";
        $res = $DB->qr($sql);
        $result = $DB->fa($res);
        $data = [
            'u_id'      =>  $result['u_id'],
            'u_name'    =>  $result['u_name'],
            'u_fcm_id'    =>  $result['u_fcm_id']
        ];
        return $data;
    }

    function get_one_user_tokens($u_id)
    {
        global $DB;
        $sql = "SELECT u_fcm_id, ios from users where u_id = '$u_id' ";
        $res = $DB->qr($sql);
        $token = $DB->fa($res);
        return [$token['u_fcm_id'], $token['ios']] ;
    }

    function get_all_users_tokens()
    {
        global $DB;
        $sql = "SELECT u_fcm_id, ios from users";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $tokens[] = [$r['u_fcm_id'], $r['ios']];
            }
        }
        return $tokens;
    }

    function get_user_profile_detail_by_id($user_id,$own_id)
    {
        global $DB;
        $sql = "SELECT 
                    u.u_name, 
                    u.u_email, 
                    u.u_contact, 
                    u.u_country, 
                    u.u_city, 
                    u.u_company, 
                    u.u_image, 
                    u.u_comp_address, 
                    u.u_nature_bussniess, 
                    u.u_is_paid,
                    (SELECT u_is_paid FROM users WHERE u_id='$own_id') as own_user,
                    u_seller_desc,
                    u_seller_company,
                    u_seller_city,
                    u_seller_country,
                    u_seller_business_cat,
                    u_seller_business_nat,
                    u_seller_phone,
                    u_seller_email,
                    u_seller_address,
                    u_seller_products,
                    u_buyer_desc,
                    u_buyer_company,
                    u_buyer_city,
                    u_buyer_country,
                    u_buyer_business_cat,
                    u_buyer_business_nat,
                    u_buyer_phone,
                    u_buyer_email,
                    u_buyer_address,
                    u_buyer_products
                FROM users u 
                WHERE u.u_id='$user_id' ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            $r = $DB->fa($res);
            $data = [
                'success'               => true
            ];
            $r['u_seller_products'] = array_filter(explode(" , ", $r['u_seller_products']), "strlen");
            $r['u_buyer_products'] = array_filter(explode(" , ", $r['u_buyer_products']), "strlen");
            $data = array_merge($data, $r);
        }
        else
        {
            $message = "Profile Detail Not Found";
            $data 	= ['success' => false,'message'=>$message];
        }
        return json_encode($data);
    }

    function update_user_expiry($u_id)
    {
        global $DB;
        $sql = "UPDATE users SET u_pkg_type = 'Expire', u_is_paid = 2 WHERE u_id = '$u_id' ";
        if($DB->qr($sql))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function check_if_user_expire($user_id, $device_id)
    {
        global $DB;
        $sql = "SELECT u_expiry_date, u_is_paid, u_type, size_of_msg, device_id FROM users WHERE u_id = '$user_id' ";
        $res = $DB->qr($sql);
        $package = $DB->fa($res);
        $after_date = $package['u_expiry_date'];
        if(date('Y-m-d') > date('Y-m-d', strtotime($after_date)))
        {
            // User Expired
            if ($this->update_user_expiry($user_id))
            {
                $data = ['success' => false, 'message'=>'Your Package Has Expired'];
            }
        }
        else
        {
            if($package['device_id'] == $device_id)
            {
                $data = ['success' => true, 'message'=>$package['u_is_paid'], 'u_type' => $package['u_type'], 'size_of_msg' => $package['size_of_msg']];
            }
            else
            {
                $data = ['success' => false, 'message'=>'Invalid Device ID'];
            }
        }
        return json_encode($data);
    }
    
    function update_token($u_id, $token) {
        global $DB;
        if (!isset($u_id) || !isset($token)) {
            $data = ['success' => false,'message'=>'u_id and token required'];
            return json_encode($data); 
        } else {
            $sql = "UPDATE users SET u_fcm_id='$token' WHERE u_id=$u_id";
            if ($DB->qr($sql)) {
                $data = ['success' => true,'message'=>'Token updated'];
                return json_encode($data);
            } else {
                $data = ['success' => false,'message'=>'Could not update token'];
                return json_encode($data);
            }
        }
    }

    function update_user_detail($u_id,$u_name,$u_contact,$u_city,$u_company,$u_nature,$u_address,$img,$u_country)
    {
        global $DB;

        if(empty($img)){
            $sql = "UPDATE users SET u_name='$u_name',u_contact='$u_contact',u_city='$u_city',u_company='$u_company',u_comp_address='$u_address',u_nature_bussniess='$u_nature' , u_country = '$u_country' WHERE u_id='$u_id'";
        }else{
            $sql = "UPDATE users SET u_name='$u_name',u_contact='$u_contact',u_city='$u_city',u_company='$u_company',u_comp_address='$u_address',u_nature_bussniess='$u_nature',u_image='$img' , u_country = '$u_country', image_approved=0 WHERE u_id='$u_id'";
        }

        if($DB->qr($sql))
        {
            $msg = "Update Successful.";
            $data = ['success' => true,'message'=>$msg];
            return json_encode($data);
        }
        else
        {
            $msg = "Server Error Please try again.";
            $data = ['success'=>false,'message'=>$msg];
            return json_encode($data);
        }
    }

    function contact_us($param)
    {
        $name = $param['name'];
        $email = $param['email']; 
        $phone = $param['phone'];
        $msg = $param['message'];
        $formcontent="From: $name \nEmail:$email \nPhone: $phone \nMessage: $msg ";
        $recipient = 'info@hellotextile.com, contactus@hellotextile.com, hellotextileapp@gmail.com'; //info@noor-enterprises.com,documents@noor-enterprises.com
        if(mail($recipient, $param['subject'], $formcontent))
        {
            $msg = "Thank you for contacting us.";
            $data = ['success' => true,'message'=>$msg];
        }
        else
        {
            $msg = "Some thing wrong please try again";
            $data = ['success' => false,'message'=>$msg];
        }
        return json_encode($data);
    }

    function check_phone_no_if_we_have($phone)
    {
        global $DB;
        $sql = "SELECT u_contact FROM users WHERE u_contact = '$phone' ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
            return true;
        else
            return false;
    }

    function update_device_id($email,$device_id)
    {
        global $DB;
        if($this->email_authentication($email))
        {
            $sql = "UPDATE users SET device_id='$device_id' WHERE u_email='$email' ";
            if($DB->qr($sql))
            {
                $msg = "Successfully Logged in here.";
                $data = ['success' => true,'message'=>$msg];
                return json_encode($data);
            }
            else
            {
                $msg = "Server Error Please try again.";
                $data = ['success'=>false,'message'=>$msg];
                return json_encode($data);
            }
        }
        else
        {
            //Error
            $msg = "Invalid Email address.";
            $data = ['success' => false,'message'=>$msg];
        }
        return json_encode($data);
    }

    function forgot_password($email)
    {
        global $DB;
        $sql = "SELECT u_pass FROM users WHERE u_email = '$email' ";
        $res = $DB->qr($sql);
        $detail = $DB->fa($res);
        $mailheader = "From: Hello Textile <no-reply@hellotextiles.com>";
        if($DB->nr($res)>0)
        {
            if(mail($email, "Forgot Password", "Your Password is ".$detail['u_pass'], $mailheader))
            {
                $msg = "Your password has been mailed.";
                $data = ['success' => true,'message'=>$msg];
            }
            else
            {
                $msg = "Something went wrong please try again";
                $data = ['success' => false,'message'=> $msg];
            }
        }
        else
        {
            $msg = "No such account with this email.";
            $data = ['success' => false,'message'=>$msg];
        }
        return json_encode($data);
    }
    
    function update_password($u_id, $opwd, $pwd, $cpwd) {
        if ($pwd != $cpwd) {
            return json_encode(["success" => false, "message" => "Passwords don't match."]);
        }
        global $DB;
        $sql = "SELECT u_id FROM users WHERE u_id = '$u_id' AND u_pass='$opwd'";
        $res = $DB->qr($sql);
        
        if ($DB->nr($res) > 0) {
            $DB->qr("UPDATE users SET u_pass='$cpwd' WHERE u_id = '$u_id'");
            return json_encode(["success" => true, "message" => "Successfully updated password."]);
        } else {
            return json_encode(["success" => false, "message" => "$sql"]);
        }
    }

    function select_user_package_detail($user_id)
    {
        global $DB;
        $sql = "SELECT u_id, u_expiry_date, u_pkg_type, u_total_msg, size_of_msg from users WHERE u_id = '$user_id' ";
        $res = $DB->qr($sql);
        $result = $DB->fa($res);
        $data = array(
            'u_id'          =>  $result['u_id'],
            'u_expiry_date' =>  $result['u_expiry_date'],
            'u_pkg_type'    =>  $result['u_pkg_type'],
            'u_total_msg'   =>  $result['u_total_msg'],
            'size_of_msg'   =>  $result['size_of_msg']
        );
        return json_encode($data);
    }
    
    function update_country_filter($u_id, $countries) 
    {
        global $DB;
        $sql = "UPDATE users SET ex_countries='$countries' WHERE u_id=$u_id";
        if ($DB->qr($sql)) {
            return json_encode(["success" => true, "message" => "Updated successfully"]);
        }
        return json_encode(["success" => false, "message" => "An error occurred"]);
    }
    
    function update_group_notif_settings($u_id, $c_id, $sub_cid, $should_block) 
    {
        global $DB;
        
        // Find all sub categories
        $all_sub_cats = null;
        if ($c_id > 0) {
            $sql1 = "SELECT GROUP_CONCAT(sc_id) as sub_cat FROM sub_category WHERE c_id=$c_id GROUP BY c_id";
            if (!$DB->fa($DB->qr("SELECT c_id FROM category WHERE c_id=$c_id"))) {
                return json_encode(['success' => false, 'message'=> 'Invalid category']);
            }
            $all_sub_cats = $DB->qr($sql1);
            $all_sub_cats = $DB->fa($all_sub_cats);
        }
        if ($sub_cid > 0) {
            $sql1 = "SELECT GROUP_CONCAT(sc_id) as sub_cat, c_id FROM sub_category WHERE c_id=(SELECT c_id FROM sub_category WHERE sc_id=$sub_cid) GROUP BY c_id";
            $temp_sub_cats = $DB->qr($sql1);
            $temp_sub_cats = $DB->fa($temp_sub_cats);
            $temp_c_id = intval($temp_sub_cats['c_id']);
            if ($temp_c_id <= 0 || ($c_id > 0 && $temp_c_id != $c_id)) {
                return json_encode(['success' => false, 'message'=> 'Invalid sub-category']);
            }
            $all_sub_cats = $temp_sub_cats;
            $c_id = $temp_c_id;
        }
        
        $all_sub_cats = array_filter(explode(',', $all_sub_cats['sub_cat']), 'strlen');
        $sql = "SELECT blocked_cat, blocked_sub_cat FROM users WHERE u_id=$u_id";
        $result = $DB->qr($sql);
        $result = $DB->fa($result);
        
        if ($result) {
            // split into array using comma
            $blocked_cat = explode(",", $result['blocked_cat']);
            $blocked_sub_cat = explode(",", $result['blocked_sub_cat']);
            
            if ($result['blocked_cat'] == '') {
                $blocked_cat = [];    
            }
            if ($result['blocked_sub_cat'] == '') {
                $blocked_sub_cat = [];    
            }
            
            if ($sub_cid > 0 && in_array($sub_cid, $blocked_sub_cat) && $should_block == 0) {
                // remove subcategory from blocked list of subcategories
                $blocked_sub_cat = array_filter($blocked_sub_cat, function ($el) use ($sub_cid) { return ($el != $sub_cid); });
                
            }
            if ($c_id > 0  && in_array($c_id, $blocked_cat) && $should_block == 0) {
                // remove category from blocked list of categories only if all the subcategories have been unblocked or the subcategory is not provided
                if ($sub_cid <= 0 || count(array_intersect($all_sub_cats, $blocked_sub_cat)) == 0) {
                    $blocked_cat = array_filter($blocked_cat, function ($el) use ($c_id) { return ($el != $c_id); });
                }
                // If no sub category is provided, unblock all subcategories of this category (if they exist)
                if ($sub_cid <= 0) {
                    $blocked_sub_cat = array_diff($blocked_sub_cat, $all_sub_cats);
                }
                    
            }
            if ($c_id > 0 && !in_array($c_id, $blocked_cat) && $should_block == 1) {
                // block category
                $blocked_cat[] = $c_id;
                // If subcategory is not provided, block all subcategories of this category
                if ($sub_cid <= 0) {
                    $blocked_sub_cat = array_unique(array_merge($blocked_sub_cat, $all_sub_cats));
                }
                
            }
            if ($sub_cid > 0 && !in_array($sub_cid, $blocked_sub_cat) && $should_block == 1) {
                // block subcategory
                $blocked_sub_cat[] = $sub_cid;
            }
            // remove any falsy values
            $blocked_cat = array_filter($blocked_cat, 'strlen');
            $blocked_sub_cat = array_filter($blocked_sub_cat, 'strlen');
            
            // turn both arrays into comma-separated string
            $blocked_cat = implode($blocked_cat, ',');
            $blocked_sub_cat = implode($blocked_sub_cat, ',');
            
            $sql = "UPDATE users SET blocked_cat='$blocked_cat', blocked_sub_cat='$blocked_sub_cat' WHERE u_id=$u_id";
            
            if ($DB->qr($sql)) {
                return json_encode(["success" => true, "message" => "Updated successfully"]);
            }
        } 
        return json_encode(["success" => false, 'message' => 'Invalid user']);
    }
    function get_group_notif_settings($u_id)
    {
        global $DB;
        $sql = "SELECT blocked_cat, blocked_sub_cat FROM users WHERE u_id=$u_id";
        $result = $DB->qr($sql);
        $result = $DB->fa($result);
        if ($result) {
            $blocked_cat = explode(",", $result['blocked_cat']);
            $blocked_sub_cat = explode(",", $result['blocked_sub_cat']);
            return json_encode([
                "success" => true,
                "blocked_cat" => $this->map_to_int_array($blocked_cat),
                "blocked_sub_cat" => $this->map_to_int_array($blocked_sub_cat)
            ]);
        }
        return json_encode(["success" => false, "message" => "Something went wrong"]);
    }
    function map_to_int_array($arr) {
        return array_map(function($el){return intval($el);}, array_filter($arr, function($el) {return $el > 0;}));
    }

    public function update_user_last_active($email)
    {
        global $DB;
        // $date = new DateTime(date('Y-m-d H:i:s'));
        $date = date('Y-m-d H:i:s');
        // echo $date;
        // die();
        // $sql = `UPDATE users SET u_last_active = "`.$date.`" WHERE u_email = "`.$email.`"`;
        $sql = "UPDATE users SET u_last_active = '$date'  WHERE u_email = '$email' ";
        if($DB->qr($sql))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function user_search_record($arr)
    {
        global $DB;
        $u_id           = isset($arr['u_id'])? $arr['u_id']:"";
        $profile_type   = isset($arr['profile_type'])? $arr['profile_type']:""; // u_profile_type
        $business_cat   = isset($arr['business_cat'])? $arr['business_cat']:"";
        $business_nat   = isset($arr['business_nat'])? $arr['business_nat']:""; 
        $country        = isset($arr['country'])? $arr['country']:"";
        $city           = isset($arr['city'])? $arr['city']:"";
        $keywords       = isset($arr['keywords'])? $arr['keywords']:"";

        try {
            $sql = "INSERT INTO user_search( user_id, search_looking_for, search_business_nature, search_country, search_city, search_business_category, search_keywords)
                    VALUES($u_id,'$profile_type','$business_nat','$country','$city','$business_cat','$keywords')";    
            $res = $DB->qr($sql);

        } catch (\Throwable $th) {
            return false;
        }

        return true;
    }

    function search_profiles_new($arr) {
        global $DB;
        $u_id           = $arr['u_id'];
        $profile_type   = $arr['profile_type']; // u_profile_type
        $business_cat   = $arr['business_cat'];
        $business_nat   = $arr['business_nat']; 
        $country        = $arr['country'];
        $city           = $arr['city'];
        $page           = $arr['page'];
        $limit          = $arr['limit'] <= 0 ? 10 : $arr['limit'] % 50;
        $keywords       = $arr['keywords'];
        $company        = $arr['company'];
        $search_column  = $arr['search_column']; // new field in search profile 

        
        
        $sql = "SELECT u_is_paid FROM users WHERE u_id=$u_id";
        $user = $DB->qr($sql);
        if ($DB->nr($user) != 1) {
            return json_encode(['success' => false, 'message' => 'invalid user']);       
        } 
        $user  = $DB->fa($user);
        
        $user_search_record = $this->user_search_record($arr);
        
        $filter_business_cat = empty($business_cat) ? "" : "AND u_{$profile_type}_business_cat='$business_cat'";
        $filter_business_nat = empty($business_nat) ? "" : "AND u_{$profile_type}_business_nat='$business_nat'";
        $filter_country = empty($country) ? ""           : "AND u_{$profile_type}_country LIKE '%$country%'";
        $filter_city = empty($city) ? ""                 : "AND u_{$profile_type}_city LIKE '%$city%'";
        $filter_company = empty($company)? ""               : "AND u_{$profile_type}_company LIKE '%$company%'";
        
        $filter_keywords = "";
        $keyword_arr = [];

        if(!empty($search_column))
        {
            if(!empty($keywords))
            {
                $filter_keywords = "AND u_{$profile_type}_{$search_column} LIKE '%$keywords%'";
                $keyword_arr = explode(' ',$keywords);
                if(count($keyword_arr) > 1)
                {
                    foreach ($keyword_arr as $key => $value) {
                        $filter_keywords .= "OR u_{$profile_type}_{$search_column} LIKE '%$value%'";
                    }
                }
            }
        }
        else{

            // $search_text = trim("$business_cat $business_nat $country $city $keywords");
            // $search_columns = "u_{$profile_type}_desc, u_{$profile_type}_company, u_{$profile_type}_business_cat, u_{$profile_type}_business_nat, u_{$profile_type}_country, u_{$profile_type}_city, u_{$profile_type}_products, u_name";
            
            $search_text = trim("$business_cat $business_nat $keywords");
            $search_columns = "u_{$profile_type}_desc, u_{$profile_type}_company, u_{$profile_type}_business_cat, u_{$profile_type}_business_nat, u_{$profile_type}_products, u_name";
            $filter_keywords = empty($keywords) ? ""         : "AND MATCH($search_columns) AGAINST('$search_text' IN BOOLEAN MODE)";

        }

        
        $start = $page * $limit;

        $sql = "SELECT 
                    u_id, 
                    u_name, 
                    u_last_active,
                    u_is_paid,
                    u_{$profile_type}_desc as u_desc,
                    u_{$profile_type}_company as u_company,
                    u_{$profile_type}_city as u_city,
                    u_{$profile_type}_country as u_country,
                    u_{$profile_type}_business_cat as u_business_cat,
                    u_{$profile_type}_business_nat as u_business_nat,
                    u_{$profile_type}_phone as u_phone,
                    u_{$profile_type}_email as u_email,
                    u_{$profile_type}_address as u_address,
                    u_{$profile_type}_products as u_products
                FROM users 
                WHERE u_id!=$u_id $filter_business_cat $filter_business_nat $filter_country $filter_city $filter_keywords $filter_company
                LIMIT $start, $limit";

        $res = $DB->qr($sql);

        $users = [];
        while($r = $DB->fa($res)) {
            $r['u_products'] = array_filter(explode(" , ", $r['u_products']), "strlen");

            $r['u_name']            = ($r['u_name'] == null)? "":$r['u_name'];
            $r['u_is_paid']         = ($r['u_is_paid'] == null)? "":$r['u_is_paid'];
            $r['u_last_active']     = ($r['u_last_active'] == null)? "":$r['u_last_active'];
            $r['u_desc']            = ($r['u_desc'] == null)? "":$r['u_desc'];
            $r['u_company']         = ($r['u_company'] == null)? "":$r['u_company'];
            $r['u_city']            = ($r['u_city'] == null)? "":$r['u_city'];
            $r['u_country']         = ($r['u_country'] == null)? "":$r['u_country'];
            $r['u_business_cat']    = ($r['u_business_cat'] == null)? "":$r['u_business_cat'];
            $r['u_business_nat']    = ($r['u_business_nat'] == null)? "":$r['u_business_nat'];
            $r['u_phone']           = ($r['u_phone'] == null)? "":$r['u_phone'];
            $r['u_email']           = ($r['u_email'] == null)? "":$r['u_email'];
            $r['u_address']         = ($r['u_address'] == null)? "":$r['u_address'];
            $users[] = $r;
        }
        return json_encode(['sql' => $sql, 'success' => true, 'data' => $users]);
    }
}