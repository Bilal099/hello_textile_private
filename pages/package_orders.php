<?php
include_once "../include/header.php";
?>

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Package Orders
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                        <tr>
                            <th id="column_order_id">Order #</th>
                            <th>Name</th>
                            <th>Package</th>
                            <th>Amount (PKR)</th>
                            <th>Payment Method</th>
                            <th>Payment Status</th>
                            <th>Package Assigned?</th>
                            <th>Date</th>
                            <th>Error</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Order #</th>
                            <th>Name</th>
                            <th>Package</th>
                            <th>Amount (PKR)</th>
                            <th>Payment Method</th>
                            <th>Payment Status</th>
                            <th>Package Assigned?</th>
                            <th>Date</th>
                            <th>Error</th>
                        </tr>
                        <tbody>
                            <?php echo $package->get_all_orders();?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

<?php
include_once "../include/footer.php"
?>

<script>
$(document).ready(function() {
  $("#column_order_id").click();
});
    
</script>
