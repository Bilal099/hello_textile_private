<?php
include_once "../include/header.php";

if(isset($_POST['submit']))
{
    $title = $_POST['title'];
    $description = $_POST['description'];
    $include_countries = $_POST['include_countries'];
    echo $noti->add_notifications($title,$description,$include_countries);
}
?>

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Notification</h2>
                    <!-- <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul> -->
                </div>
                <div class="body">
                    <form id="form_validation" enctype="multipart/form-data" method="POST" action="">
                        <input type="hidden" name="include_countries" id="include_countries"/>
                        <div style="margin-top:10px;">
                            <label>
                                Viewer Countries:
                                <div id="multiselect_container" style="min-width:500px;">
                                    
                                </div>
                            </label>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="title" required>
                                <label class="form-label">Title</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea name="description" cols="30" rows="5" class="form-control no-resize" required></textarea>
                                <label class="form-label">Description</label>
                            </div>
                        </div>

                        <button class="btn btn-primary waves-effect" type="submit" name="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php
include_once "../include/footer.php";
?>

<script src="../plugins/jquery-multiselect/jquery.dropdown.min.js"></script>

<script>
    
$(document).ready(function() {
    let selected_countries = {};
    
    let countries = ['Afghanistan', 'Aland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia, Plurinational State of', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo', 'Costa Rica', 'Croatia', 'Denmark', 'Egypt', 'Ethiopia', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'Georgia', 'Germany', 'Greenland', 'Guyana', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Lebanon', 'Liberia', 'Libyan Arab Jamahiriya', 'Macao', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Malta', 'Marshall Islands', 'Mexico', 'Monaco', 'Morocco', 'Myanmar', 'Namibia', 'Nepal', 'Netherlands', 'New Zealand', 'Niger', 'Nigeria', 'North Korea', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Panama', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn', 'Poland', 'Portugal', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Reunion', 'Saint Barthelemy', 'Saint Lucia', 'Saint Martin', 'Samoa', 'San Marino', 'Saudi Arabia', 'Serbia', 'Sierra Leone', 'Singapore', 'Slovakia', 'Somalia', 'South Africa', 'South Korea', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vietnam', 'Yemen', 'Zambia', 'Zimbabwe'];
    countries = countries.map(function(a){return `<option value="${a}">${a}</option>`}).join("");
    $("#multiselect_container").html(`<select style="display:none;" name="" multiple placeholder="Select countries">${countries}</select>`)
    $("#multiselect_container").dropdown({
        input: '<input id="country_select_input" type="text" maxLength="20" placeholder="Search">',
        searchNoData: '<li style="color:#ddd">No Results</li>',
        multipleMode: 'label',
        readOnly: true,
        choice: function() {
            if (arguments[1]) {
                selected_countries[arguments[1].name] = arguments[1].selected; 
            } else {
                let c = arguments[0].target.getAttribute("data-id");
                selected_countries[c] = false; 
            }
                 
            $("#country_select_input").val("").focus();
            $("#include_countries").val(Object.keys(selected_countries).filter(k => selected_countries[k]));
        },
    });
})
</script>