<?php
/**
 * Created by DK_KHAN.
 * User: DK_KHAN
 * Date: 10/4/2018
 * Time: 1:08 PM
 */

class apiCategory
{
    function get_all_cat_and_subcat()
    {
        global $DB;
        $sql = "SELECT c_id, c_name FROM category WHERE c_status = 1 AND sub_cat = 0 ORDER by c_id asc ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = [
                    'c_id'    =>  $r['c_id'],
                    'c_name'  =>  $r['c_name']
                ];
            }
        }
        $sql2 = "SELECT sc_id, sc_name, c_id FROM `sub_category` WHERE sc_status = 1";
        $res2 = $DB->qr($sql2);
        if($DB->nr($res2)>0)
        {
            while($r = $DB->fa($res2))
            {
                $data[] = [
                    'sc_id'    =>  $r['sc_id'],
                    'sc_name'  =>  $r['sc_name'],
                    'c_id'  =>  $r['c_id']
                ];
            }
        }
        return json_encode($data);
    }

    function get_all_category($u_id)
    {
        global $DB;
        $sql = "SELECT c_id, c_name, c_icon, sub_cat FROM category where c_status = 1 ORDER by c_id asc ";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = [
                    'c_id'    =>  $r['c_id'],
                    'c_name'  =>  $r['c_name'],
                    'c_icon'  =>  image_view_path.$r['c_icon'],
                    'sub_cat' =>  $r['sub_cat']
                ];
            }
        }

        if($u_id != 0)
        {
            $date = date('Y-m-d H:i:s');
            $sql_sub = "UPDATE users SET u_last_active = '$date'  WHERE u_id = '$u_id' ";
            $DB->qr($sql_sub);
        }
        return json_encode($data);
    }

    function get_sub_cateory($c_id)
    {
        global $DB;
        $sql = "SELECT sc_id, sc_name, sc_icon FROM `sub_category` WHERE c_id = '$c_id' AND sc_status = 1";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = [
                    'sc_id'    =>  $r['sc_id'],
                    'sc_name'  =>  $r['sc_name'],
                    'sc_icon'  =>  image_view_path.$r['sc_icon'],
                ];
            }
        }
        return json_encode($data);
    }

    public function get_all_category_with_sub_category()
    {
        global $DB;
        $sql            = "SELECT c_id, c_name, c_status FROM category WHERE c_status = 1 ORDER by c_id asc ";
        $res            = $DB->qr($sql);
        $data           = array();
        $category       = array();
        $sub_category   = array();
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $category[] = [
                    'category_id'           => ($r['c_id']      != null)? $r['c_id']:      '', 
                    'category_name'         => ($r['c_name']    != null)? $r['c_name']:    '', 
                    'category_status'       => ($r['c_status']  != null)? $r['c_status']:  ''
                ];
            }
        }
        $query = "SELECT sc_id, sc_name, c_id, sc_status FROM sub_category WHERE  sc_status = 1";
        $res2 = $DB->qr($query);
        if($DB->nr($res2)>0)
        {
            while($r = $DB->fa($res2))
            {
                $sub_category[] = [
                    'sub_category_id'       => ($r['sc_id']     != null)? $r['sc_id']:     '', 
                    'sub_category_name'     => ($r['sc_name']   != null)? $r['sc_name']:   '', 
                    'sub_category_status'   => ($r['sc_status'] != null)? $r['sc_status']: '',
                    'category_id'           => ($r['c_id']      != null)? $r['c_id']:      ''
                ];
            }
        }
        $i = 0;
        foreach ($category as $key => $value) 
        {
            $flag = 0;
            $data[$i]  = [
                'category_id'           => $value['category_id'], 
                'category_name'         => $value['category_name'], 
                'category_status'       => $value['category_status']  
            ];
            foreach ($sub_category as $key1 => $value1) 
            {
                if ($data[$i]['category_id'] == $value1['category_id']) 
                {
                    $data[$i]['sub_category'][] = [
                        'sub_category_id'       => $value1['sub_category_id'], 
                        'sub_category_name'     => $value1['sub_category_name'], 
                        'sub_category_status'   => $value1['sub_category_status'],
                        'category_id'           => $value1['category_id'],
                    ];
                    $flag = 1;
                }
            }
            if ($flag == 0) 
            {
                $data[$i]['sub_category'] = [];
            }
            $i++;
        }
        return json_encode($data);
    }
}