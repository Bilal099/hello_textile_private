<?php
/**
 * Created by Arsalan.
 * User: Arsalan
 * Date: 11/16/2018
 * Time: 02:43- PM
 */

class blogs
{
    function get_blogs()
    {
        global $DB;
        $sql = "SELECT * FROM blogs ORDER BY b_id DESC";
        $res = $DB->qr($sql);
        if($DB->nr($res)>0)
        {
            while($r = $DB->fa($res))
            {
                $data[] = [
                    'b_id'      => $r['b_id'],
                    'b_title'   => $r['b_title'],
                    'b_description'    => $r['b_description'],
                    'b_image'   => image_view_path.$r['b_image'],
                    'b_date'    => $r['b_date'],
                    'b_status'    => $r['b_status']
                ];
            }
        }
        return json_encode($data);
    }
}