
</div>
</section>

<!-- Jquery Core Js -->
<script src="../plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="../plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="../plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="../plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="../plugins/raphael/raphael.min.js"></script>

<!-- ChartJs -->
<script src="../plugins/chartjs/Chart.bundle.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

<!-- Chat JS -->
<script src="../js/chat-application.js" type="text/javascript"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="../plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="../js/pages/tables/jquery-datatable.js"></script>

<!-- Jquery Validation Plugin Css -->
<script src="../plugins/jquery-validation/jquery.validate.js"></script>
<script src="../js/pages/forms/form-validation.js"></script>

<!-- Custom Js -->
<script src="../js/admin.js"></script>
<script src="../js/pages/index.js"></script>

<!-- Demo Js -->
<script src="../js/demo.js"></script>


<script>

    function load_unapprove_chat()
    {
        $.ajax({
            type: "POST",
            data: {'request':'load_un_approve_chat'},
            url: "ajaxcall.php",
            success: function(data)
            {
                $("#load_un_approve_chat_here").html(data);
            }
        });
    }

    function get_unread_count_of_msg_in_lis_in_home_page()
    {
        $.ajax({
            type: "POST",
            data: {'request':'get_unread_count_msg'},
            url: "ajaxcall.php",
            success: function(data)
            {
                $("#span_count_msg").text(data+" New");
            }
        });
    }

    function get_hold_msg_count()
    {
        $.ajax({
            type: "POST",
            data: {'request':'get_count_of_hold_messages'},
            url: "ajaxcall.php",
            success: function(data)
            {
                $("#count_hold_msg").text(data+" New");
            }
        });
    }

    function get_user_seller_profile_count()
    {
        $.ajax({
            type: "POST",
            data: {'request':'get_user_seller_profile_count'},
            url: "ajaxcall.php",
            success: function(data)
            {
                $("#seller_count").val(data);
                $("#count_seller_profile").text(data+" New");
            }
        });
    }

    function get_user_buyer_profile_count()
    {
        $.ajax({
            type: "POST",
            data: {'request':'get_user_buyer_profile_count'},
            url: "ajaxcall.php",
            success: function(data)
            {
                $("#buyer_count").val(data);
                $("#count_buyer_profile").text(data+" New");
            }
        });
    }

</script>

</body>

</html>