<?php
include "../include/config.php";
include_once "../include/header.php";
?>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Video Ads
                    </h2>
                    <p>Ad access for advertisers: <a href="advertise.php" target="_blank">https://hellotextiles.com/pages/advertise.php</a></p>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>Advertiser ID</th>
                                <th>Current # of Views</th>
                                <th>Clicks</th>
                                <th>Created On</th>
                                <th>Expires On</th>
                                <th>Live?</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
                                    $result = mysqli_query($con, "SELECT * FROM video_ads");
                                    while ($ad = mysqli_fetch_assoc($result)) {
                                        
                                        $adv_id = $ad['advertiser_id'];
                                        $views = $ad['num_views'] - $ad['views_left'];
                                        $clicks = $ad['clicks'];
                                        $duration_num = $ad['duration_num'];
                                        $duration_type = $ad['duration_type'];
                                        $is_live = $ad['is_live'] ? "Yes" : "No";
                                        $created_on = date('d-m-Y', strtotime($ad['created_on'] . " + 0 days"));
                                        $expiry = date('d-m-Y', strtotime($ad['created_on'] . " + $duration_num $duration_type"));
                                        $expired = intval(date_diff(date_create(date('d-m-Y')), date_create($expiry))->format('%R%a'));
                                        $red_style = $expired < 0 ? "style='color:red;'" : '';
                                        $expired_word = $expired < 0 ? "(Expired)": "";
                                        $is_live = $expired < 0 ? "No" : $is_live;
                                        
                                        echo "<tr>
                                            <td><a href='/pages/edit_video_ad.php?adv_id=$adv_id'>$adv_id</a></td>
                                            <td>$views</td>
                                            <td>$clicks</td>
                                            <td>$created_on</td>
                                            <td $red_style>$expiry $expired_word</td>
                                            <td>$is_live</td>
                                            <td><button class='delete_btn' data-id='$adv_id'>Delete</button></td>
                                        </tr>";
                                        
                                        if ($ad['is_live'] && $expired < 0) {
                                            mysqli_query($con, "UPDATE video_ads SET is_live=0 WHERE advertiser_id='$adv_id'");
                                        }
                                    }
                                
                                ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Advertiser ID</th>
                                <th>Current # of Views</th>
                                <th>Clicks</th>
                                <th>Created On</th>
                                <th>Expires On</th>
                                <th>Live?</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

<?php
include_once "../include/footer.php"
?>

<script>
    $(document).ready(function(){
        $('.delete_btn').click(function(e) {
          const adv_id = e.target.getAttribute('data-id'); 
          e.target.parentNode.parentNode.style.opacity="0.5";
          $('.delete_btn').attr('disabled', true);
          const fd = new FormData();    
          fd.append( 'adv_id', adv_id);
           $.ajax({
          url: "delete_video_ad.php",
          data: fd,
          processData: false,
          contentType: false,
          type: 'POST',
          success: function(data){
              e.target.parentNode.parentNode.remove();
              $('.delete_btn').attr('disabled', false);
          },
          error: function(err) {
              e.target.parentNode.parentNode.style.opacity="1";
              $('.delete_btn').attr('disabled', false);
          }
        }); 
        });
    });
</script>
