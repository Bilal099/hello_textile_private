<?php
include_once "../include/header.php";

if(isset($_POST['subBtn']))
{
    $parent_cat_id = $_POST['p_cat_id'];
    $name = $_POST['name'];
    $status = $_POST['status'];
    echo $category_obj->add_sub_category($parent_cat_id,$name,$status);
}
?>
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Sub category</h2>
                    <!-- <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul> -->
                </div>
                <div class="body">
                    <form id="form_validation" enctype="multipart/form-data" method="POST">
                        <div class="form-group form-float">
<!--                            <div class="form-line">-->
                                <p>
                                    <b>Select Category</b>
                                </p>
                                <select name="p_cat_id" class="form-control show-tick" data-live-search="true">
                                    <? echo $category_obj->select_category_name(); ?>
                                </select>
<!--                            </div>-->
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" required>
                                <label class="form-label">Title</label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                        <p>
                                            <b>Status</b>
                                        </p>
                                        <select name="status" class="form-control show-tick">
                                            <option value="0">Select Option</option>
                                            <option value="1">Active</option>
                                            <option value="0">In-Active</option>
                                        </select>

                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "../include/footer.php";
?>