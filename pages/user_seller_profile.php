<?php
include_once "../include/header.php";
?> 

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Users Seller Profile List
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Seller Description</th>
                                <th>Seller Company</th>
                                <th>Seller City</th>
                                <th>Seller Country</th>
                                <th>Seller Business Category</th>
                                <th>Seller Business Nature</th>
                                <th>Seller Phone</th>
                                <th>Seller Email</th>
                                <th>Seller Address</th>
                                <th>Seller Product</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                            <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Seller Description</th>
                                <th>Seller Company</th>
                                <th>Seller City</th>
                                <th>Seller Country</th>
                                <th>Seller Business Category</th>
                                <th>Seller Business Nature</th>
                                <th>Seller Phone</th>
                                <th>Seller Email</th>
                                <th>Seller Address</th>
                                <th>Seller Product</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php echo $user->get_user_seller_profile_list();?>
                            
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="view_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>


<script>

    $(".btn_view_record").click(function (e) { 
        e.preventDefault();
        var arr = [
            '.u_name',
            '.u_email',
            '.u_contact',
            '.u_country',
            '.u_city',
            '.u_seller_desc',
            '.u_seller_company',
            '.u_seller_city',
            '.u_seller_country',
            '.u_seller_business_cat',
            '.u_seller_business_nat',
            '.u_seller_phone',
            '.u_seller_email',
            '.u_seller_address',
            '.u_seller_product'
        ];

        var label = [
            'Name',
            'Email',
            'Contact',
            'Country',
            'City',
            'Seller Description',
            'Seller Company',
            'Seller City',
            'Seller Country',
            'Seller Business_cat',
            'Seller Business_nat',
            'Seller Phone',
            'Seller Email',
            'Seller Address',
            'Seller Product'
        ];

        var i;
        var text = '';
        var val = '';
        for (i = 0; i < label.length; i++) 
        {
            if (arr[i] == '.u_seller_business_cat' || arr[i] == '.u_seller_business_nat' || arr[i] == '.u_seller_address' || arr[i] == '.u_seller_product' || arr[i] == '.u_seller_desc' ) 
            {
                val = $(this).closest('tr').find(arr[i]).data(arr[i].replace('.',''));
                let temp = (val != null)? val:"";
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+temp+`</p>
                    </div>
                    </div>`;
            }
            else{
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+$(this).closest('tr').find(arr[i]).text()+`</p>
                    </div>
                    </div>`;
            }
            
        }

        $(".modal-body").html(text);
        $("#view_modal").modal("show");

    });

</script>
