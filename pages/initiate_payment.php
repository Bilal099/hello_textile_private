<?php
echo "Cancelled";
exit;
// >>> To be removed
// echo "We are working on to bring this feature in your region. Sorry for inconvenience.";
// exit;
// <<< To be removed

include "../include/config.php";

$o = $_GET['o']; // get base64 encoded encrypted order id
if (!isset($o)) {
    echo "1.Something went wrong!";
    exit;
}

$o = base64_decode($o);
$orderRefNum = openssl_decrypt($o, "AES-128-ECB","2chMsMGE<j&g=\H");
$order_id = explode("-", $orderRefNum)[3]; // HT-xx-xx-<user_id>
if (!is_numeric($order_id)) {
    echo "2.Something went wrong!";
    exit;
}

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
$order = mysqli_query($con, "SELECT mobile, amount, type, transaction_status FROM package_orders WHERE _id=$order_id");

if (!$order) {
    echo "3.Something went wrong!";
    mysqli_close($con);
    exit;
}

$order = mysqli_fetch_assoc($order);

if (!$order || $order['type'] != 'cc') {
    echo "4.Something went wrong!";
    mysqli_close($con);
    exit;
}

if ($order['transaction_status'] == "PAID") {
    header("Location:/pages/confirm_payment.php?success=true");
}

mysqli_close($con);

$hashRequest = '';
$hashKey = 'C9W9B4592DXO62XK'; // generated from easypay account
$storeId="60479"; // generated from easypay account
$amount = $order['amount']; 
$amount= number_format((float)$amount, 1, '.', '');
$postBackURL="https://hellotextiles.com/pages/confirm_payment.php";
$expiryDate= "" . date("Ymd His", time() + 10 * 60); // 10 minutes expiry
$autoRedirect=1;
$paymentMethod='CC_PAYMENT_METHOD';

///starting encryption///
$paramMap = array();
$paramMap['amount']  = $amount;
$paramMap['autoRedirect']  = $autoRedirect;
$paramMap['expiryDate'] = $expiryDate;
$paramMap['orderRefNum']  = $orderRefNum;
$paramMap['paymentMethod']  = $paymentMethod;
$paramMap['postBackURL'] = $postBackURL;
$paramMap['storeId']  = $storeId;

//Creating string to be encoded
$mapString = '';
foreach ($paramMap as $key => $val) {
      $mapString .=  $key.'='.$val.'&';
}
$mapString  = substr($mapString , 0, -1);

// Encrypting mapString
function pkcs5_pad($text, $blocksize) {
      $pad = $blocksize - (strlen($text) % $blocksize);
      return $text . str_repeat(chr($pad), $pad);
}

// mcrypt module has to be enabled in cpanel
$alg = MCRYPT_RIJNDAEL_128; // AES
$mode = MCRYPT_MODE_ECB; // ECB

$iv_size = mcrypt_get_iv_size($alg, $mode);
$block_size = mcrypt_get_block_size($alg, $mode);
$iv = mcrypt_create_iv($iv_size, MCRYPT_DEV_URANDOM);

$mapString = pkcs5_pad($mapString, $block_size);
$crypttext = mcrypt_encrypt($alg, $hashKey, $mapString, $mode, $iv);
$hashRequest = base64_encode($crypttext);

?>
<html>
<title>Processing your order</title>
<body>
<div style="display: flex; align-items: center;">
    <img src="../images/spinner.gif" width="24" height="24">
    <h2 style="margin-left:10px;">Please wait while we process your order ...</h2>
</div>
<form action=" https://easypay.easypaisa.com.pk/easypay/Index.jsf" method="POST" id="easyPayStartForm">
<input name="storeId" value="<?php echo $storeId; ?>" hidden = "true"/>
<input name="amount" value="<?php echo $amount; ?>" hidden = "true"/>
<input name="postBackURL" value="<?php echo $postBackURL; ?>" hidden = "true"/>
<input name="orderRefNum" value="<?php echo $orderRefNum; ?>" hidden = "true"/>
<input type ="text" hidden="true" name="expiryDate" value="<?php echo $expiryDate; ?>">
<input type="hidden" name="autoRedirect" value="<?php echo $autoRedirect; ?>" >
<input type ="hidden" name="paymentMethod" value="<?php echo $paymentMethod; ?>">
<input type ="hidden" name="merchantHashedReq" value="<?php echo $hashRequest; ?>">
<input type="submit" hidden="true" value="Submit">
</form>
<script>
    (function(){
        document.getElementById("easyPayStartForm").submit();
    })()
</script>
</body>
</html>
