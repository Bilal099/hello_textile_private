<?php
// echo "bilal";
include_once "../include/header.php";

?>

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        User Search Record List
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th>User Name</th>
                                <th>User Country</th>
                                <th>User City</th>
                                <th>Search Profile</th>
                                <th>Search Country</th>
                                <th>Search City</th>
                                <th>Search Nature</th>
                                <th>Search Category</th>
                                <th>Search Keywords</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>User Name</th>
                                <th>User Country</th>
                                <th>User City</th>
                                <th>Search Profile</th>
                                <th>Search Country</th>
                                <th>Search City</th>
                                <th>Search Nature</th>
                                <th>Search Category</th>
                                <th>Search Keywords</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?= $user->user_search_record_list() ?>
                            <!-- <tr>
                                <td>User Name</td>
                                <td>User Country</td>
                                <td>User City</td>
                                <td>Search Profile</td>
                                <td>Search Country</td>
                                <td>Search City</td>
                                <td>Search Nature</td>
                                <td>Search Category</td>
                                <td>Search Keywords</td>
                            </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
<!-- <div id="message"></div> -->
<?php
include_once "../include/footer.php"
?>

<script>

    $(".pkg_ApproveBtn").click(function () {
        var obj = $(this);
        approve_package(obj);
    });

    $(".pkg_deletBtn").click(function () {
        var id = $(".pkg_deletBtn").val();
        var obj = $(this);
        //alert(id);
        $.ajax({
            type: "POST",
            async:false,
            data: { request:'delete_pkg_request', pr_id:id},
            url: "ajaxcall.php",
            success: function(data)
            {
                obj.parent().parent().remove();
                $("#message").html(data);
            }
        });
    });

    function approve_package(obj)
    {
        var id = $(".pkg_ApproveBtn").val();
        var pkg_id = $(".pkg_ApproveBtn").attr('data-pkgID');
        var user_id = $(".pkg_ApproveBtn").attr('data-userID');
        //alert('Request ID: '+id+' Package ID: '+pkg_id+' user iD: '+user_id);

        $.ajax({
            type: "POST",
            async:false,
            data: { request:'assign_user_a_package', pid:pkg_id , prID:id , u_id:user_id},
            url: "ajaxcall.php",
            success: function(data)
            {
                obj.parent().parent().remove();
                $("#message").html(data);
            }
        });
    }
</script>

