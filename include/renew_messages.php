<?php
// this module is run every midnight.
// checks if users' renew_msg_date is expired, in which case the number of messages allowed per <user's package message duration> is reset to <package's allowed number of messages>
include "config.php";

echo "[".date('m/d/Y h:i:s a', time())."] Renewing messages \n";

$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

$sql_users = "SELECT u_id, renew_msg_date, p_no_of_msg, p_msg_duration FROM users, packeages
              WHERE pkg_id=p_id AND DATEDIFF(str_to_date(renew_msg_date, '%m/%d/%Y'), NOW()) < 1 AND DATEDIFF(str_to_date(u_expiry_date, '%m/%d/%Y'), NOW()) >= 1";
              // package hasn't expired yet: DATEDIFF(str_to_date(u_expiry_date, '%m/%d/%Y'), NOW()) >= 1
            
$result = mysqli_query($con, $sql_users);

$queries = "";
$num_users_affected = 0;
$num_total_users = 0;
$_1 =  null;
while ($row = mysqli_fetch_assoc($result)) {
    $new_renew_msg_date = date('m/d/Y', strtotime($row['p_msg_duration']));
    $u_total_msg = $row['p_no_of_msg'];
    $u_id = $row['u_id'];
    if (mysqli_query($con, "UPDATE users SET renew_msg_date='$new_renew_msg_date', u_total_msg=$u_total_msg WHERE u_id=$u_id")) {
        $num_users_affected += 1;
    }
    $num_total_users += 1;
}
echo "[".date('m/d/Y h:i:s a', time())."] Renewed messages for ".$num_users_affected." out of " . $num_total_users . " users\n";