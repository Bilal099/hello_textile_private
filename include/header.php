<?php
include_once "globals.php";
if(!common::logged_in())
{
    header('location:../index?redir=' . $_SERVER['REQUEST_URI']);
}
?>
<?php 

if(isset($_SESSION['admin_email'])) {
    include_once "header_admin.php";
}
else if(isset($_SESSION['sub_admin_email'])) {
    include_once "header_subadmin.php";
}
?>