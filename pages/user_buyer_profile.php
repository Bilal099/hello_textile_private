<?php
include_once "../include/header.php";
?> 

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Users Buyer Profile List
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Buyer Description</th>
                                <th>Buyer Company</th>
                                <th>Buyer City</th>
                                <th>Buyer Country</th>
                                <th>Buyer Business Category</th>
                                <th>Buyer Business Nature</th>
                                <th>Buyer Phone</th>
                                <th>Buyer Email</th>
                                <th>Buyer Address</th>
                                <th>Buyer Product</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Buyer Description</th>
                                <th>Buyer Company</th>
                                <th>Buyer City</th>
                                <th>Buyer Country</th>
                                <th>Buyer Business Category</th>
                                <th>Buyer Business Nature</th>
                                <th>Buyer Phone</th>
                                <th>Buyer Email</th>
                                <th>Buyer Address</th>
                                <th>Buyer Product</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php echo $user->get_user_buyer_profile_list();?>
                            
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="view_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
include_once "../include/footer.php";
?>

<script>

    $(".btn_view_record").click(function (e) { 
        e.preventDefault();
        var arr = [
            '.u_name',
            '.u_email',
            '.u_contact',
            '.u_country',
            '.u_city',
            '.u_buyer_desc',
            '.u_buyer_company',
            '.u_buyer_city',
            '.u_buyer_country',
            '.u_buyer_business_cat',
            '.u_buyer_business_nat',
            '.u_buyer_phone',
            '.u_buyer_email',
            '.u_buyer_address',
            '.u_buyer_product'
        ];

        var label = [
            'Name',
            'Email',
            'Contact',
            'Country',
            'City',
            'Buyer Description',
            'Buyer Company',
            'Buyer City',
            'Buyer Country',
            'Buyer Business_cat',
            'Buyer Business_nat',
            'Buyer Phone',
            'Buyer Email',
            'Buyer Address',
            'Buyer Product'
        ];

        var i;
        var text = '';
        var val = '';
        for (i = 0; i < label.length; i++) 
        {
            if (arr[i] == '.u_buyer_business_cat' || arr[i] == '.u_buyer_business_nat' || arr[i] == '.u_buyer_address' || arr[i] == '.u_buyer_product' || arr[i] == '.u_buyer_desc' ) 
            {
                val = $(this).closest('tr').find(arr[i]).data(arr[i].replace('.',''));
                let temp = (val != null)? val:"";
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+temp+`</p>
                    </div>
                    </div>`;
            }
            else{
                text += `<div class="row">
                    <div class="col-md-2"><label for="">`+ label[i] +`</label></div>
                    <div class="col-md-10">
                        <p>`+$(this).closest('tr').find(arr[i]).text()+`</p>
                    </div>
                    </div>`;
            }
            
        }

        $(".modal-body").html(text);
        $("#view_modal").modal("show");

    });

</script>