<?php
include_once "../include/header.php";
if(isset($_POST['submit']))
{
    // echo "bilal";
    // die();
    $seconds = $_POST['seconds'];
    $watch_time = $_POST['watchtime'];
    $file = $_FILES['file_name'];
    $countries = $_POST['countries'];

    $countries = implode(",",$countries);

    // $result = [
    //     'seconds' => $seconds,
    //     'watch_time' => $watch_time,
    //     // 'file' => $file,
    //     'countries' => $countries,
    // ];
    // $arr2 = json_encode($result);
    // $myfile = fopen("ads_file.txt", "w") or die("Unable to open file!");
    // fwrite($myfile, $arr2);
    // fclose($myfile);

    echo $ads->add_ads($seconds,$file,$watch_time,$countries);
}

?>
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <!-- Page Loader -->
                <div id="loading" class="page-loader-wrapper">
                    <div class="loader">
                        <div class="preloader">
                            <div class="spinner-layer pl-red">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                        <p>Please wait...</p>
                    </div>
                </div>
                <!-- #END# Page Loader -->
                <div class="header">
                    <h2>Add Ads</h2>
                </div>
                <div class="body">
                    <form id="form_validation" enctype="multipart/form-data" method="POST" action="">

                        <div class="form-group form-float">
                            <!--                            <div class="form-line">-->
                            <p>
                                <b>Select Seconds</b>
                            </p>
                            <select name="seconds" class="form-control show-tick" data-live-search="true">
                                <option value="5000">5</option>
                                <option value="10000">10</option>
                                <option value="15000">15</option>
                                <option value="20000">20</option>
                            </select>
                            <!--                            </div>-->
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="watchtime" required>
                                <label class="form-label">Watch Time Per Day</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <label class="">Ads Image</label>
                                <input type="file" class="form-control" name="file_name" required>
                            </div>
                        </div>
                        <div style="form-group form-float">
                            <div class="form-line">
                                <label>
                                    Viewer Countries:
                                    
                                    <!-- <div id="multiselect_container" style="min-width:500px;">
                                        
                                    </div> -->
                                </label>
                                <select name="countries[]" multiple class="form-control show-tick" data-live-search="true" data-actions-box="true">
                                        <!-- <option  disabled>Select Option</option> -->
                                        <?= $country->get_country(); ?>
                                    </select>

                            </div>
                        </div>
                        <br>
                        <!-- <input type="hidden" id="countries_input" name="countries" required> -->
                        <button class="btn btn-primary waves-effect" id="sendbtn" type="submit" name="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div id="message"></div>

<?php
include_once "../include/footer.php";
?>
<script src="../plugins/jquery-multiselect/jquery.dropdown.min.js"></script>

<script>


$(document).ready(function() { 
    let selected_countries = {};
    let countries = ['Afghanistan', 'Aland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia, Plurinational State of', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo', 'Costa Rica', 'Croatia', 'Denmark', 'Egypt', 'Ethiopia', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'Georgia', 'Germany', 'Greenland', 'Guyana', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Lebanon', 'Liberia', 'Libyan Arab Jamahiriya', 'Macao', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Malta', 'Marshall Islands', 'Mexico', 'Monaco', 'Morocco', 'Myanmar', 'Namibia', 'Nepal', 'Netherlands', 'New Zealand', 'Niger', 'Nigeria', 'North Korea', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Panama', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn', 'Poland', 'Portugal', 'Qatar', 'Romania', 'Russia', 'Rwanda', 'Reunion', 'Saint Barthelemy', 'Saint Lucia', 'Saint Martin', 'Samoa', 'San Marino', 'Saudi Arabia', 'Serbia', 'Sierra Leone', 'Singapore', 'Slovakia', 'Somalia', 'South Africa', 'South Korea', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vietnam', 'Yemen', 'Zambia', 'Zimbabwe'];
    $("#countries_input").val(countries.join(","));
    countries = countries.map(function(a){return `<option value="${a}">${a}</option>`}).join("");
    $("#multiselect_container").html(`<select style="display:none;" name="" multiple placeholder="Select countries">${countries}</select>`)
    $("#multiselect_container").dropdown({
        input: '<input id="country_select_input" type="text" maxLength="20" placeholder="Search (By default: all)">',
        searchNoData: '<li style="color:#ddd">No Results</li>',
        multipleMode: 'label',
        readOnly: true,
        choice: function() {
            if (arguments[1]) {
                selected_countries[arguments[1].name] = arguments[1].selected; 
            } else {
                let c = arguments[0].target.getAttribute("data-id");
                selected_countries[c] = false; 
            }
            $("#country_select_input").val("").focus();
            const view_countries = Object.keys(selected_countries).filter(a => selected_countries[a]).join(",");
            $("#countries_input").val(view_countries);
        },
    });
});
</script>