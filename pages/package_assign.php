<?php
/**
 * Created by PhpStorm.
 * User: M.DilawarKhanAzeemi
 * Date: 5/7/2019
 * Time: 2:35 PM
 */
include_once "../include/header.php";

if(isset($_POST['subBtn']))
{
    $param = array(
       'pid'   =>  $_POST['pkg_id'],
       'u_id'   =>  $_POST['user_id'],
    );

    echo $package->assign_package_user_by_admin($param);
}

?>

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Assign Package to User</h2>
                    <!-- <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul> -->
                </div>
                <div class="body">
                    <form id="form_validation" enctype="multipart/form-data" method="POST">

                        <div class="form-group form-float">
                            <!--                            <div class="form-line">-->
                            <p>
                                <b>Select Package</b>
                            </p>
                            <select name="pkg_id" class="form-control show-tick" data-live-search="true">
                                <? echo $package->select_packages_name(); ?>
                            </select>
                            <!--                            </div>-->
                        </div>

                        <div class="form-group form-float">
                            <!--                            <div class="form-line">-->
                            <p>
                                <b>Select User</b>
                            </p>
                            <select name="user_id[]" class="form-control show-tick" multiple data-live-search="true">
                                <? echo $user->seletc_user_name_for_packages(); ?>
                            </select>
                            <!--                            </div>-->
                        </div>


                        <button class="btn btn-primary waves-effect" name="subBtn" type="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
include_once "../include/footer.php"
?>