<?php
/**
 * Created by PhpStorm.
 * User: hp pc
 * Date: 11/20/2018
 * Time: 12:09 PM
 */

class advertisment
{
    private $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    function add_ads($second, $file, $watch_time, $countries)
    {
        $date = date('m/d/Y h:i:s a', time());
        // $target_dir = image_upload_path;
        $target_dir = "D:/xampp/htdocs/www/HelloTextile11Feb2021/upload/";
        $target_file_name = basename($file["name"]);
        $target_file = $target_dir . $target_file_name;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg")
        {
            $msg = '<script>swal("Oops!", "Invalid Image Format.", "error");</script>';
        }
        else
        {
            if ($file["size"] > 500000)
            {
                $msg = '<script>swal("Oops!", "Image should less then 500 KB.", "error");</script>';
            }
            else
            {
                if (move_uploaded_file($file["tmp_name"], $target_file))
                {
                    $stmt = $this->conn->prepare("INSERT INTO Advertisment(ad_image, ad_display_time, created_at, a_watchtime_default, a_watchtime_perday, countries) VALUES (?,?,?,?,?,?)");
                    $stmt->bind_param("sissss", $target_file_name, $second, $date,$watch_time, $watch_time,$countries);
                    if ($stmt->execute())
                        $msg = '<script>swal("Good Job!", "Ads add successfully.", "success");</script>';
                    else
                        $msg = '<script>swal("Oops!", "Unable to add Ads.", "error");</script>';
                    $stmt->close();
                }
                else
                {
                    $msg = '<script>swal("Oops!", "please try again.", "error");</script>';
                }
            }
        }
        return $msg;
    }

    function load_Ad()
    {
        $msg = "";
        $param = array();
        $stmt = $this->conn->prepare("SELECT ad_id, ad_image, ad_display_time, a_watchtime_default, created_at, countries FROM Advertisment");
        $stmt->execute();
        $stmt->bind_result($param['ad_id'],$param['ad_image'],$param['ad_display_time'],$param['a_watchtime_default'],$param['created_at'], $param['countries']);
        $stmt->store_result();
        if($stmt->num_rows > 0)
        {
            while($stmt->fetch())
            {
                $c = $param['countries'];
                if (empty($c)) {
                    $c = "All";    
                }
                $msg .= '<tr>
                                <td><img width="200px" height="100px" src="'.image_view_path.$param['ad_image'].'"></td>
                                <td>'.$param['ad_display_time'].'</td>
                                <td>'.$param['a_watchtime_default'].'</td>
                                <td>'.$param['created_at'].'</td>
                                <td>'.$c.'</td>
                                <td>
                                    <button value="'.$param['ad_id'].'" type="button" class="btn btn-default btn-circle waves-effect waves-circle waves-float ad_delete_btn">
                                        <i class="material-icons">delete</i>
                                    </button>
                                </td>
                            </tr>';
            }
        }
        else
        {
            $msg = '<script>swal("Oops!", "Ads Not Found.", "error");</script>';
        }
        $stmt->close();
        return $msg;
    }

    function delete_ad($id)
    {
        $stmt = $this->conn->prepare("DELETE FROM Advertisment  where ad_id=? ");
        $stmt->bind_param("i", $id);
        if ($stmt->execute())
            $msg = "Ads Removed successfully.";
        else
            $msg = "Unable to Remove Ads.";
        $stmt->close();
        return $msg;
    }
}